﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadTracking
{
	public class DetectedHead
	{
		private HeadModel model;
		private double prob;

		public double Prob
		{
			get { return prob; }
			set { prob = value; }
		}

		public HeadModel Model
		{
			get { return model; }
			set { model = value; }
		}

		public DetectedHead(HeadModel model,double prob){
			this.model = model;
			this.prob = prob;
		
		}
	}
}

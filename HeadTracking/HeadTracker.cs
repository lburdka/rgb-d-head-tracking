﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KinectIO;
using Emgu.CV.Structure;
using System.Windows.Forms;
using Emgu.CV;
using System.Drawing;
using System.Threading;
using System.Diagnostics;

namespace HeadTracking
{
	public class HeadTracker
	{
		public event EventHandler<HeadFrameReadyEventArgs> HeadFrameReady;
		private List<HeadModel> models;
		private HeadIntegrator integrator;
		private byte[] currentColorFrame;
		private byte[] colorFrameToShow;
		private byte[] processedColorFrame;
		private short[] processedDepthFrame;
		private Image<Bgr, Byte> colorImageToShow;
		private Image<Gray, Byte> depthImageToShow;
		private short[] currentDepthFrame;
		private bool firstCycle;


		private byte[] finalImage;

		List<Image<Bgr, Byte>> imgHistory;


		TMMaskedTracker surf;
		TMMaskedMoveTracker mmTracker;
		TMDepthTracker depthTracker;
		TMImprovedDepthTracker idepthTracker;


		//Official fields:
		Image<Bgr, Byte> currentColorEmguImage;
		Image<Gray, Byte> currentDepthEmguImage;

		HeadTask currentTask;

		MovementDetector movementDetector;

		HeadModel trackedHead;

		List<HeadModel> trackedHeads;

		HeadModel[] readyModel;



		public void IntegrateTrackedHeads()
		{
			trackedHeads.Clear();
			trackedHeads.AddRange(surf.GetHeads());
			trackedHeads.AddRange(mmTracker.GetHeads());
			trackedHead = new HeadModel(0,0,0,0);
			double total = 0;

			foreach (HeadModel hm in trackedHeads)
			{
				total += hm.Probability;
			}
			
			foreach (HeadModel hm in trackedHeads)
			{
				trackedHead.headX += hm.headX * (hm.Probability) /total;
				trackedHead.headY += hm.headY * (hm.Probability) / total;
				trackedHead.headZ += hm.headZ * (hm.Probability) / total;
				trackedHead.rad += hm.rad * (hm.Probability) / total;

			}
			trackedHead.Probability = total / trackedHeads.Count;
			//MessageBox.Show(DepthColorMapper.GetRealWidth(trackedHead.rad*2,DepthColorMapper.RealDepth(trackedHead.headX,trackedHead.headY,currentDepthEmguImage)).ToString());
		}

		private void convertColorImage()
		{
			currentColorEmguImage = ImageConverter.BytesToColorImage(currentColorFrame, 640, 480, 4);
			currentColorEmguImage = DepthColorMapper.FitToDepth(currentColorEmguImage);
		}

		private void trackDepth()
		{
			idepthTracker.Track(currentDepthEmguImage);
		}

		private void prepareOutput()
		{
			finalImage=ImageConverter.ColorImageToBytes(currentColorEmguImage);
		}


		public void AddAllFrames(byte[] colorFrame, short[] depthFrame)
		{

//			Stopwatch stopWatch = new Stopwatch();
//			stopWatch.Start();

			currentDepthFrame = depthFrame;

			currentColorFrame = colorFrame;


			Thread colorConversion = new Thread(new ThreadStart(convertColorImage));
			colorConversion.Start();
			

			currentDepthEmguImage = ImageConverter.ShortsToGrayImage(currentDepthFrame, 640, 480, 1);


			Thread outputPreparation = new Thread(new ThreadStart(prepareOutput));

			if (surf.Initialized)
			{
				Thread depthTracking = new Thread(new ThreadStart(trackDepth));
				depthTracking.Start();

				colorConversion.Join();


				//Join Tracked Updates

				surf.SendTargetImage(currentColorEmguImage);
				Thread colorThread = new Thread(new ThreadStart(surf.Track));
				colorThread.Start();

				outputPreparation.Start();

				movementDetector.Add(currentColorEmguImage);

				
				mmTracker.SendTargetImage(movementDetector.GetChangeImage());
				Thread moveThread = new Thread(new ThreadStart(mmTracker.Track));

				depthTracking.Join();

				
				moveThread.Start();
				colorThread.Join();
				moveThread.Join();

				IntegrateTrackedHeads();
			}
			else
			{
				colorConversion.Join();

				outputPreparation.Start();
			}

			
			if (firstCycle)
			{
				if (currentDepthFrame != null)
				{
					firstCycle = false;
					processedColorFrame = currentColorFrame;
					processedDepthFrame = currentDepthFrame;

					HeadTask task = new HeadTask(currentColorFrame, currentDepthFrame);
					currentTask = task;
					task.colorImage = currentColorEmguImage;
					task.depthImage = currentDepthEmguImage;

					integrator.SendTask(task);
					currentDepthFrame = null;
					currentColorFrame = null;
				}
			}
			else
			{

				if (integrator.Ready)
				{
					models.Clear();
					models.AddRange(integrator.Models);
					colorFrameToShow = processedColorFrame;
					colorImageToShow = currentTask.colorImage;
					depthImageToShow = currentTask.depthImage;

					//Test add frames
					bool ok = false;
					foreach (HeadModel hm in integrator.Models)
					{
						if (hm.Probability == 1.0)
						{
							ok = true;
						}
					}

					if (ok && !surf.Initialized)
					{
						idepthTracker.PassInitialFrame(depthImageToShow, integrator.Models);

						movementDetector.Add(colorImageToShow);
						surf.PassInitialFrame(colorImageToShow, integrator.Models);
						mmTracker.PassInitialFrame(movementDetector.GetChangeImage(), integrator.Models);
						
					}
					else
					{
						if (surf.Initialized)
						{

							idepthTracker.Update(depthImageToShow, integrator.Models);

							Image<Bgr, Byte> depthMask = idepthTracker.GetDepthMask().Convert<Bgr, Byte>();

							surf.Update(colorImageToShow, depthMask, integrator.Models);
							mmTracker.Update(movementDetector.GetChangeImage(), depthMask, integrator.Models);
							
							
						}
					}

					if (currentDepthFrame != null)
					{
						processedColorFrame = currentColorFrame;
						processedDepthFrame = currentDepthFrame;

						HeadTask task = new HeadTask(currentColorFrame, currentDepthFrame);
						currentTask = task;
						task.colorImage = currentColorEmguImage;
						task.depthImage = currentDepthEmguImage;
						integrator.SendTask(task);
						currentDepthFrame = null;
						currentColorFrame = null;
					}

				}
			}

			if (HeadFrameReady != null)
			{
				readyModel[0] = trackedHead;
				HeadFrameReadyEventArgs readyEvent = new HeadFrameReadyEventArgs(readyModel);

				outputPreparation.Join();

				readyEvent.rawColorData = finalImage;
				
				HeadFrameReady(this, readyEvent);
			}


		/*	stopWatch.Stop();
        // Get the elapsed time as a TimeSpan value.
        TimeSpan ts = stopWatch.Elapsed;

        // Format and display the TimeSpan value. 
		string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:0000}",
			ts.Hours, ts.Minutes, ts.Seconds,
			ts.Milliseconds);
		MessageBox.Show(elapsedTime);*/
		}


		public void AddColorFrame(byte[] frame)
		{
			//Deprecated
		}

		public void SURFTrackerCallback(List<SURFTrackedHead> trackedHeads)
		{
			foreach (SURFTrackedHead tracked in trackedHeads)
			{
				if (tracked.homography != null)
				{  //draw a rectangle along the projected model
					Rectangle rect = tracked.ROI;
					PointF[] pts = new PointF[] { 
               new PointF(rect.Left, rect.Bottom),
               new PointF(rect.Right, rect.Bottom),
               new PointF(rect.Right, rect.Top),
               new PointF(rect.Left, rect.Top)};
					tracked.homography.ProjectPoints(pts);
					
					if(colorImageToShow!=null)
						colorImageToShow.DrawPolyline(Array.ConvertAll<PointF, Point>(pts, Point.Round), true, new Bgr(Color.Red), 5);
				}
			}
		}

		public void AddDepthFrame(short[] frame)
		{
			
			//Deprecated
		}

		public void ResetSession()
		{
			integrator.ResetDetectors();
		}

		public HeadTracker()
		{
			models = new List<HeadModel>();
			integrator = new HeadIntegrator();
			firstCycle = true;
			idepthTracker = new TMImprovedDepthTracker(this);
			surf = new TMMaskedTracker(this);
			surf.IdTracker = idepthTracker;
			depthTracker = new TMDepthTracker(this);
			imgHistory = new List<Image<Bgr,Byte>>();
			movementDetector = new MovementDetector(3);
			mmTracker = new TMMaskedMoveTracker(this);
			mmTracker.IdTracker = idepthTracker;
			trackedHeads = new List<HeadModel>();
			trackedHead = new HeadModel(0, 0, 0, 0);
			readyModel = new HeadModel[1];

			
		}

		public static byte[] TestDepthConversion(short[] image){
			return null;
		}
		
	}
}

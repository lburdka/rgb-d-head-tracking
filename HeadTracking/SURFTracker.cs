﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Features2D;
using Emgu.CV.GPU;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.VideoSurveillance;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeadTracking
{
	class SURFTracker
	{
		SURFDetector surfCPU;
		GpuSURFDetector surfGPU;

		VectorOfKeyPoint observedKeyPoints;
		Matrix<int> indices;

		Matrix<byte> mask;
        int k = 2;
		double uniquenessThreshold = 0.8;

		GpuBruteForceMatcher<float> matcher;

		HeadTracker tracker;

		List<SURFTrackedHead> trackedHeads;

		public bool Initialized;

		private bool firstDebug = true;



		public SURFTracker(HeadTracker ht)
		{
			Initialized = false;
			surfCPU = new SURFDetector(500, true);
			surfGPU = new GpuSURFDetector(surfCPU.SURFParams, 0.1f);
			matcher = new GpuBruteForceMatcher<float>(DistanceType.L2);
			tracker = ht;
			trackedHeads = new List<SURFTrackedHead>();

			

		}

		public void PassInitialFrame(Image<Bgr, Byte> modelImage,List<HeadModel> models)
		{
			Initialized = true;
			//Recognize features for ROIs
		//	modelImage = modelImage.Canny(20, 100).Convert<Bgr, Byte>();

			/*String win1 = "Debug Window";
			CvInvoke.cvNamedWindow(win1);
			CvInvoke.cvShowImage(win1, modelImage.Ptr);
			*/
			Image<Gray, Byte> grayModelImage = modelImage.Convert<Gray, Byte>();

			

	//		MessageBox.Show(models.Count.ToString());


			GpuImage<Gray, Byte> gpuModelImage = new GpuImage<Gray, byte>(grayModelImage);


			foreach (HeadModel model in models)
			{
				if (model.Probability == 1.0)
				{
					



					Image<Gray, Byte> mMask = new Image<Gray, Byte>(new Size(grayModelImage.Width, grayModelImage.Height));
					Point[] total = { new Point(0, 0), new Point(grayModelImage.Width, 0), new Point(0, grayModelImage.Height), new Point(grayModelImage.Width, grayModelImage.Height) };
					mMask.FillConvexPoly(total, new Gray(0));
					Point[] sub = { new Point(model.ColorBoundingRect.X, model.ColorBoundingRect.Y), new Point(model.ColorBoundingRect.X + model.ColorBoundingRect.Width, model.ColorBoundingRect.Y), new Point(model.ColorBoundingRect.X, model.ColorBoundingRect.Y + model.ColorBoundingRect.Height), new Point(model.ColorBoundingRect.X + model.ColorBoundingRect.Width, model.ColorBoundingRect.Y + model.ColorBoundingRect.Height) };
					mMask.FillConvexPoly(sub, new Gray(255));
					GpuImage<Gray, Byte> gpuMask = new GpuImage<Gray, byte>(mMask);

					GpuMat<float> gpuModelKeyPoints = surfGPU.DetectKeyPointsRaw(gpuModelImage, gpuMask);
				
					GpuMat<float> gpuModelDescriptors = surfGPU.ComputeDescriptorsRaw(gpuModelImage, gpuMask, gpuModelKeyPoints);

					VectorOfKeyPoint modelKeyPoints = new VectorOfKeyPoint();
					surfGPU.DownloadKeypoints(gpuModelKeyPoints, modelKeyPoints);

					SURFTrackedHead tracked = new SURFTrackedHead(model);
					tracked.ROI = model.ColorBoundingRect;
					tracked.gpuModelDescriptors = gpuModelDescriptors;
					tracked.modelKeyPoints = new VectorOfKeyPoint();
					tracked.modelKeyPoints = modelKeyPoints;
					tracked.grayModelImage = grayModelImage;
					trackedHeads.Add(tracked);
					
					/*
					String win1 = "Debug Window";
					CvInvoke.cvNamedWindow(win1);
					Image<Bgr, Byte> colorRoi = grayModelImage.Convert<Bgr, Byte>();
					colorRoi=Features2DToolbox.DrawKeypoints(colorRoi, tracked.modelKeyPoints, new Bgr(Color.Red), Features2DToolbox.KeypointDrawType.DRAW_RICH_KEYPOINTS);
					CvInvoke.cvShowImage(win1, colorRoi.Ptr);
					trackedHeads.Add(tracked);*/
 
 
				}
			}

			
		}

		public void UpdateModels(Image<Bgr, Byte> modelImage, List<HeadModel> models)
		{
			//Check if those models are tracked already if no, add to tracking
		}

		public void Track(Image<Bgr, Byte> targetImage)
		{
		//	targetImage = targetImage.Canny(20, 100).Convert<Bgr, Byte>();
			Image<Gray, Byte> grayTargetImage = targetImage.Convert<Gray, Byte>();
			GpuImage<Gray, Byte> gpuObservedImage = new GpuImage<Gray, byte>(grayTargetImage);
			

			/*
			 Image<Gray, Byte> mMask = new Image<Gray, Byte>(new Size(grayModelImage.Width, grayModelImage.Height));
					Point[] total = {new Point(0, 0),new Point(grayModelImage.Width, 0),new Point(0, grayModelImage.Height),new Point(grayModelImage.Width, grayModelImage.Height)};
					mMask.FillConvexPoly(total, new Gray(0));
					Point[] sub = { new Point(model.ColorBoundingRect.X, model.ColorBoundingRect.Y), new Point(model.ColorBoundingRect.X + model.ColorBoundingRect.Width, model.ColorBoundingRect.Y), new Point(model.ColorBoundingRect.X, model.ColorBoundingRect.Y + model.ColorBoundingRect.Height), new Point(model.ColorBoundingRect.X + model.ColorBoundingRect.Width, model.ColorBoundingRect.Y + model.ColorBoundingRect.Height) };
					mMask.FillConvexPoly(sub, new Gray(255));
			 */

			/*
			
			String win1 = "Debug Window2";
			CvInvoke.cvNamedWindow(win1);
			Image<Bgr, Byte> colorRoi2 = grayTargetImage.Convert<Bgr, Byte>();
			colorRoi2 = Features2DToolbox.DrawKeypoints(colorRoi2, observedKeyPoints, new Bgr(Color.Red), Features2DToolbox.KeypointDrawType.DRAW_RICH_KEYPOINTS);
			CvInvoke.cvShowImage(win1, colorRoi2.Ptr);
			*/


			foreach (SURFTrackedHead tracked in trackedHeads)
			{

				Image<Gray, Byte> mMask = new Image<Gray, Byte>(new Size(tracked.grayModelImage.Width, tracked.grayModelImage.Height));
				Point[] total = { new Point(0, 0), new Point(tracked.grayModelImage.Width, 0), new Point(0, tracked.grayModelImage.Height), new Point(tracked.grayModelImage.Width, tracked.grayModelImage.Height) };
				mMask.FillConvexPoly(total, new Gray(0));
				Point[] sub = { new Point(tracked.model.ColorBoundingRect.X - tracked.model.ColorBoundingRect.Width, tracked.model.ColorBoundingRect.Y - tracked.model.ColorBoundingRect.Height), new Point(tracked.model.ColorBoundingRect.X + tracked.model.ColorBoundingRect.Width * 2, tracked.model.ColorBoundingRect.Y - tracked.model.ColorBoundingRect.Height), new Point(tracked.model.ColorBoundingRect.X - tracked.model.ColorBoundingRect.Width, tracked.model.ColorBoundingRect.Y + tracked.model.ColorBoundingRect.Height*2), new Point(tracked.model.ColorBoundingRect.X + tracked.model.ColorBoundingRect.Width*2, tracked.model.ColorBoundingRect.Y + tracked.model.ColorBoundingRect.Height*2) };
				mMask.FillConvexPoly(sub, new Gray(255));
				GpuImage<Gray, Byte> gpuMaskX = new GpuImage<Gray, byte>(mMask);

				GpuMat<float> gpuObservedKeyPoints = surfGPU.DetectKeyPointsRaw(gpuObservedImage, gpuMaskX);
				GpuMat<float> gpuObservedDescriptors = surfGPU.ComputeDescriptorsRaw(gpuObservedImage, gpuMaskX, gpuObservedKeyPoints);
				GpuMat<int> gpuMatchIndices = new GpuMat<int>(gpuObservedDescriptors.Size.Height, k, 1, true);
				GpuMat<float> gpuMatchDist = new GpuMat<float>(gpuObservedDescriptors.Size.Height, k, 1, true);
				GpuMat<Byte> gpuMask = new GpuMat<byte>(gpuMatchIndices.Size.Height, 1, 1);
				Stream stream = new Stream();


				observedKeyPoints = new VectorOfKeyPoint();
				surfGPU.DownloadKeypoints(gpuObservedKeyPoints, observedKeyPoints);

				matcher.KnnMatchSingle(gpuObservedDescriptors, tracked.gpuModelDescriptors, gpuMatchIndices, gpuMatchDist, k, null, stream);
				stream.WaitForCompletion();
				
				if (gpuMatchIndices.Size.Width > 1 && gpuMatchIndices.Size.Height > 1)
				{
					indices = new Matrix<int>(gpuMatchIndices.Size);
					mask = new Matrix<byte>(gpuMask.Size);
					GpuMat<float> col0 = gpuMatchDist.Col(0);
					GpuMat<float> col1 = gpuMatchDist.Col(1);
					GpuInvoke.Multiply(col1, new MCvScalar(uniquenessThreshold), col1, stream);
					GpuInvoke.Compare(col0, col1, gpuMask, CMP_TYPE.CV_CMP_LT, stream);
					stream.WaitForCompletion();
					gpuMask.Download(mask);
					gpuMatchIndices.Download(indices);
					if (GpuInvoke.CountNonZero(gpuMask) >= 2)
					{
						int nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(tracked.modelKeyPoints, observedKeyPoints, indices, mask, 1.5, 20);
						if (nonZeroCount >= 4)
						{
							//	MessageBox.Show("Yeah");
							tracked.homography = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(tracked.modelKeyPoints, observedKeyPoints, indices, mask, 2);

							if (firstDebug)
							{
								Image<Bgr, Byte> result = Features2DToolbox.DrawMatches(tracked.grayModelImage, tracked.modelKeyPoints, grayTargetImage, observedKeyPoints,
			   indices, new Bgr(255, 255, 255), new Bgr(255, 255, 255), mask, Features2DToolbox.KeypointDrawType.DEFAULT);
								String win1 = "Debug Window2";
								CvInvoke.cvNamedWindow(win1);
								CvInvoke.cvShowImage(win1, result.Ptr);
								firstDebug = false;
							}

							//perform model update here?
							/*tracked.modelKeyPoints = observedKeyPoints;
							tracked.gpuModelDescriptors = gpuObservedDescriptors;
							tracked.ROI=*/
						}
						else
						{
							tracked.homography = null;
						}
					}
				}
			}


			
			tracker.SURFTrackerCallback(trackedHeads);
			//The callback method of HeadTracker will be called here
		}

	}
}

﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.VideoSurveillance;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeadTracking
{
	public class MotionDetector : HeadDetector
	{
		
		private HeadTask task;
		private DetectedHead[] heads;
		private  MotionHistory motionHistory;
		private  FGDetector<Bgr> forgroundDetector;
		private MovementDetector movementDetector;

		public static Image<Gray, Byte> motionMaskS = null;
		Image<Gray, Byte> fg;
		Image<Gray, Byte> mh;

		public MotionDetector()
		{
				motionHistory = new MotionHistory(
			   3, //in second, the duration of motion history you wants to keep
			   0.001, //in second, maxDelta for cvCalcMotionGradient
			   1.0); //in second, minDelta for cvCalcMotionGradient	
			forgroundDetector = new FGDetector<Bgr>(Emgu.CV.CvEnum.FORGROUND_DETECTOR_TYPE.FGD);
			movementDetector = new MovementDetector(3);
			
		}

		public void SendTask(HeadTask task)
		{
			this.task = task;
		}

		public static byte[] GetSample(byte[] data)
		{

			return null;
			
		}

		public DetectedHead[] GetResponse()
		{
			return heads;
		}

		public void Reset()
		{
		
		}

		public static void ResetS()
		{

		}

		public void ExtractForeground()
		{

			try
			{

				Emgu.CV.Image<Bgr, byte> img = task.colorImage;

				img =img.SmoothGaussian(3);

				movementDetector.Add(img);

				fg = movementDetector.GetMask();

				//forgroundDetector.Update(img);

				//fg = forgroundDetector.ForgroundMask.Mul(1.0);

				//motionHistory.Update(forgroundDetector.ForgroundMask);

			/*	double[] minValues, maxValues;
				Point[] minLoc, maxLoc;
				motionHistory.Mask.MinMax(out minValues, out maxValues, out minLoc, out maxLoc);
				Image<Gray, Byte> motionMask = motionHistory.Mask.Mul(255.0/maxValues[0]);

				mh = motionMask.Dilate(12);

				mh = img.And(mh.Convert<Bgr, Byte>()).Convert<Gray,Byte>();

			*/	

				img.Dispose();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
				MessageBox.Show(e.StackTrace);
			}
		}

		public double isForeground(Rectangle r)
		{
			

			Image<Gray, Byte> toCheck = fg.GetSubRect(r);
			
			return (double)(toCheck.CountNonzero()[0])/(toCheck.Width*toCheck.Height);
		}

		public void Process()
		{
			try
			{
				heads = new DetectedHead[HeadIntegrator.MAX_HEADS];


				Emgu.CV.Image<Bgr, byte> img = task.colorImage;
				Emgu.CV.Image<Bgr, byte> orig = img.Copy();
				img=img.SmoothGaussian(3);
				//	Emgu.CV.Image<Gray, Byte> imgCpy = img.Convert<Gray, Byte>();

					
					forgroundDetector.Update(img);

					Image<Gray, Byte> silh = forgroundDetector.ForgroundMask.Mul(1.0).Dilate(10);

					using (MemStorage storage2 = new MemStorage())
					{
						int indx = 0;
						for (
							  Contour<Point> contours = silh.FindContours(
								 Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_NONE,
								 Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL,
								 storage2);
							  contours != null;
							  contours = contours.HNext)
						{
							//	Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.05, storage);
							Contour<Point> currentContour = contours;
							//	ccc = (int)(r.NextDouble() * 255);

							if (contours.BoundingRectangle.Height > 150)
							{
								Rectangle rrr = contours.BoundingRectangle;
							//	silh.Draw(rrr, new Gray(50), 2);
								//Do the whole detection here


								//Determine the center of head suspected region
								int avgCenter = 0;
								int avgWidth = 0;
								for (int i = 0; i < rrr.Height / 5; i++)
								{
									int silC = 0;
									int silCount = 0;
									for (int j = 0; j < rrr.Width; j++)
									{
										if (contours.InContour(new PointF(rrr.X + j, rrr.Y + i)) > 0)
										{
											silC += j;
											silCount++;
										}
									}
									if (silCount > 0)
										silC /= silCount;
									avgCenter += silC;
									avgWidth += silCount;
								}
								avgCenter /= rrr.Height / 5;
								avgWidth /= rrr.Height / 5;

								if (avgWidth < rrr.Height / 5 && avgWidth > rrr.Height / 10)
								{
									Rectangle hRec = new Rectangle(rrr.X + avgCenter - avgWidth / 2, rrr.Y, avgWidth, avgWidth);
									HeadModel head = new HeadModel(hRec.X + hRec.Width / 2, hRec.Y + hRec.Height / 2, 0, hRec.Width / 2);
									
									if (SkinDetector.IsRegionSkin(orig, hRec, 0, 0, 0.1))
									{
										head.Probability = 1;
										head.ColorBoundingRect = hRec;
										//Point dc=DepthColorMapper.SimpleColorToDepthCoords(hRec.X,hRec.Y);
										Point dc = new Point(hRec.X, hRec.Y);
										head.DepthBoundingRect = new Rectangle(dc.X,dc.Y, hRec.Width, hRec.Height);
										heads[indx] = new DetectedHead(head, 1);
										//							imgCpy.Draw(new Rectangle(rrr.X + avgCenter - avgWidth / 2, rrr.Y, avgWidth, avgWidth), new Gray(255), 3);
									}
									else
									{
										head.Probability = 0.5;
										heads[indx] = new DetectedHead(head, 0.5);
										//						imgCpy.Draw(new Rectangle(rrr.X + avgCenter - avgWidth / 2, rrr.Y, avgWidth, avgWidth), new Gray(30), 3);
									}

									indx++;

								}
								//	Emgu.CV.CvInvoke.cvDrawContours(silh, currentContour, new MCvScalar(30, 30, 30), new MCvScalar(0, 0, 0), 1, 5, Emgu.CV.CvEnum.LINE_TYPE.CV_AA, new System.Drawing.Point(1));
							}


						}

						while (indx < HeadIntegrator.MAX_HEADS)
						{
							heads[indx] = new DetectedHead(new HeadModel(0, 0, 0, 10), 0);
							indx++;
						}
					}
					img.Dispose();
					orig.Dispose();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
				MessageBox.Show(e.StackTrace);
			}
		}

		public double Weight()
		{
			return 0.8;
		}
	}
}

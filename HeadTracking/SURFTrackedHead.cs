﻿using Emgu.CV;
using Emgu.CV.GPU;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadTracking
{
	public class SURFTrackedHead
	{
		public HeadModel model;
		public GpuMat<float> gpuModelDescriptors;
		public VectorOfKeyPoint modelKeyPoints;
		public HomographyMatrix homography;
		public Rectangle ROI;
		public Image<Gray, Byte> grayModelImage;
		public SURFTrackedHead(HeadModel head)
		{
			model = head;
		}
	}
}

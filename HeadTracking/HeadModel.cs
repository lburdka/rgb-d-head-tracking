﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace HeadTracking
{
	public class HeadModel
	{
		//to delete
		public double headX;
		public double headY;
		public double headZ;
		public double rad;
		/////////////////////
		private Vector3 position;

		public System.Drawing.Rectangle ColorBoundingRect;
		public System.Drawing.Rectangle DepthBoundingRect;

		public Vector3 Position
		{
			get { return position; }
			set { position = value; }
		}
		private double radius;
		public double Radius
		{
			get { return radius; }
			set { radius = value; }
		}
		private double probability;
		public double Probability
		{
			get { return probability; }
			set { probability = value; }
		}

		public HeadModel(float X, float Y, float Z, double R)
		{
			radius = R;
			position = new Vector3(X, Y, Z);

			//to del
			headX = X;
			headY = Y;
			headZ = Z;
			rad = R;
		}
	}
}

﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeadTracking
{
	public class DepthColorMapper
	{

		//Based on http://burrus.name/index.php/Research/KinectCalibration
		//Color
		private static double fx_rgb= 5.2921508098293293e+02;
		private static double fy_rgb= 5.2556393630057437e+02;
//		private static double cx_rgb= 3.2894272028759258e+02;
//		private static double cy_rgb= 2.6748068171871557e+02;
		private static double cx_rgb = 339.30780975300314;
		private static double cy_rgb = 242.73913761751615;
		private static double k1_rgb= 2.6451622333009589e-01;
		private static double k2_rgb= -8.3990749424620825e-01;
		private static double p1_rgb= -1.9922302173693159e-03;
		private static double p2_rgb= 1.4371995932897616e-03;
		private static double k3_rgb= 9.1192465078713847e-01;

		//Depth
		private static double fx_d= 5.9421434211923247e+02;
		private static double fy_d= 5.9104053696870778e+02;
		private static double cx_d= 339.30780975300314;
		private static double cy_d= 242.73913761751615;
		private static double k1_d= -2.6386489753128833e-01;
		private static double k2_d= 9.9966832163729757e-01;
		private static double p1_d= -7.6275862143610667e-04;
		private static double p2_d= 5.0350940090814270e-03;
		private static double k3_d = -1.3053628089976321e+00;

		private static double NEAR = 0.8;
		private static double FAR = 4.0;

	/*	public static Point SimpleDepthToColorCoords(int x, int y)
		{
			double sideShift = (((640-x)/640.0)) * 30-10;
			return new Point((int)(x + sideShift), y + 20);
		}

		public static Point SimpleColorToDepthCoords(int x, int y)
		{
			return new Point(x + 10, y - 20);
		}*/

		/*public static Point DepthToColorCoords(double x, double y, Emgu.CV.Image<Gray, Byte> img)
		{
			Microsoft.Xna.Framework.Vector3 vex = DepthToSpaceCoords(x, y, img);
			return SpaceToColorCoords(vex.X, vex.Y, vex.Z);
		}

		public static Point SpaceToColorCoords(float x, float y, float z)
		{
			Microsoft.Xna.Framework.Vector3 vex = new Microsoft.Xna.Framework.Vector3(x, y, z);
			vex.X += 0.05f;
			Point ret = new Point();
			ret.X = (int)((vex.X * fx_rgb / vex.Z) + cx_rgb);
			ret.Y = (int)(((480-vex.Y) * fy_rgb / vex.Z) + cy_rgb);
			return ret;
		}*/

		/*public static Microsoft.Xna.Framework.Vector3 DepthToSpaceCoords(double x, double y, Emgu.CV.Image<Gray, Byte> img)
		{

			Microsoft.Xna.Framework.Vector3 vex = new Microsoft.Xna.Framework.Vector3();
			vex.X = (float)((x - cx_d) * Depth(x,y,img) / fx_d);
			vex.Y = 480 - (float)((y - cy_d) * Depth(x, y, img) / fy_d);
			vex.Z = (float)(Depth(x, y, img));
	//		MessageBox.Show(x.ToString()+" "+y.ToString()+" "+(depth(x, y, img)).ToString()+" - "+vex.X.ToString()+" "+vex.Y.ToString()+" "+vex.Z.ToString());
			return vex;
		}*/

		public static double Depth(double x, double y, Emgu.CV.Image<Gray, Byte> img)
		{
			return NEAR+(img.Data[(int)(y),(int)(x), 0]/255.0)*(FAR-NEAR);
			
		}

		public static double RealDepth(double x, double y, Emgu.CV.Image<Gray, Byte> img)
		{
			if (x < 0 || x >= 640 || y < 0 || y >= 480)
				return 0;
			byte dat = img.Data[(int)(y), (int)(x), 0];
			if (dat == 255)
				dat = 0;
			return dat;

		}

		public static Image<Bgr, Byte> FitToDepth(Image<Bgr, Byte> colorImg)
		{
			return colorImg.Resize(1.1, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR).GetSubRect(new Rectangle((int)(0.06 * 640), (int)(0.05 * 480), 640, 480)).Copy();
		}

		public static double GetRealHeight(double pixelHeight, double depth)
		{
			double dist = depth * 32;
			return 2 * Math.Tan(21.5 * Math.PI / 180.0) * dist * (pixelHeight / 480);
		}

		public static double GetRealWidth(double pixelWidth, double depth)
		{
			double dist = depth * 32;
			return 2 * Math.Tan(28.5 * Math.PI / 180.0) * dist * (pixelWidth / 640);
		}

		/*public static void Allign(Image<Gray, Byte> dpth, Image<Bgr, Byte> clr)
		{

			clr = clr.Resize(1.1, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
			clr=clr.GetSubRect(new Rectangle((int)(0.06*640),(int)(0.05*480),640,480));
			Image<Bgr, Byte> res = clr.Copy();
			dpth = dpth.Not().Mul(10);
			res=res.And(dpth.Convert<Bgr, Byte>());
			String win1 = "Debug Window Kotik! :[";
			CvInvoke.cvNamedWindow(win1);

			CvInvoke.cvShowImage(win1, res.Ptr);
		}*/
	}
}

﻿using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadTracking
{
	class BasicDetector : HeadDetector
	{
		private FaceDetector faceDetector;
		private HeadTask task;
		private DetectedHead[] heads;

		public BasicDetector()
		{
			faceDetector = new FaceDetector();
		}

		public void SendTask(HeadTask task)
		{
			this.task = task;
		}

		public DetectedHead[] GetResponse()
		{
			return heads;
		}

		public void Reset()
		{
			
		}

		public void Process()
		{
			heads = new DetectedHead[HeadIntegrator.MAX_HEADS];
			Emgu.CV.Image<Bgr, Byte> img = task.colorImage;

			Emgu.CV.Image<Gray, Byte> dpth = ImageConverter.ShortsToGrayImage(task.DepthFrame, 640, 480, 1);

			int xMarg = 0;
			int yMarg = 0;

			Rectangle r = SkinDetector.GetSkinRegion(img,5);
			xMarg = r.X;
			yMarg = r.Y;
			img=img.GetSubRect(r);

			List<Rectangle> faces = new List<Rectangle>();
			faceDetector.Detect(img, faces);
			int indx = 0;
			foreach (Rectangle rect in faces)
			{
				if(SkinDetector.IsRegionSkin(img,rect,xMarg,yMarg)){
					HeadModel hm = new HeadModel(rect.X + xMarg + rect.Width / 2, rect.Y + yMarg + rect.Height / 2, 0, rect.Width / 2);
					hm.ColorBoundingRect = rect;
					DetectedHead h = new DetectedHead(hm, 1);
					heads[indx] = h;
					indx++;
				}
			}
			while (indx < HeadIntegrator.MAX_HEADS)
			{
				heads[indx] = new DetectedHead(new HeadModel(0,0, 0, 10), 0);
				indx++;
			}
		}

		public double Weight()
		{
			return 0.8;
		}
	}
}

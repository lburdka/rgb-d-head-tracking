﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadTracking
{
	class MovementDetector
	{
		private List<Image<Bgr, Byte>> history;
		private List<Image<Gray, Byte>> depthHistory;
		private Image<Bgr, Byte> changeImage;
		private Image<Gray, Byte> mask;
		private int hSize;

		public MovementDetector(int historySize)
		{
			hSize = historySize;

			history = new List<Image<Bgr, Byte>>();
			depthHistory = new List<Image<Gray, Byte>>();
		}

		public Image<Bgr, Byte> GetChangeImage()
		{
			return changeImage;
		}

		public Image<Gray, Byte> GetMask()
		{
			return mask;
		}

		public bool IsMoving(Rectangle r)
		{
			Image<Gray, Byte> toCheck = mask.GetSubRect(r);
			
			return (double)(toCheck.CountNonzero()[0]) / (toCheck.Width * toCheck.Height)>0.5;
		}

		public void Add(Image<Bgr, Byte> img)
		{
			
			history.Add(img.SmoothGaussian(1));
			if (history.Count > hSize)
			{
				Image<Bgr, Byte> i = history[0];
				history.RemoveAt(0);
				
			}

			if (history.Count > 1)
			{

				
				if (history.Count > 2)
				{
					Image<Bgr, Byte> dif3C = history[history.Count - 2].AbsDiff(history[history.Count - 3]);
					Image<Bgr, Byte> dif2C = history[history.Count - 1].AbsDiff(history[history.Count - 2]);

					Image<Gray, Byte> dif3 = dif3C.Convert<Gray, Byte>();
					Image<Gray, Byte> dif2 = dif2C.Convert<Gray, Byte>();
					Image<Gray, Byte> dif4 = dif2.Add(dif3).Add(dif2).ThresholdToZero(new Gray(40)).Mul(20.0).Erode(1).Dilate(10);
					
					mask= dif4;

					Image<Bgr, Byte> dif4C = dif2C.Add(dif3C).Add(dif2C);
					changeImage = dif4C;


				}
				Image<Bgr, Byte> difC= history[history.Count - 1].AbsDiff(history[history.Count - 2]);

				Image<Gray, Byte> dif = difC.Convert<Gray, Byte>().ThresholdToZero(new Gray(10)).Mul(255.0).Erode(1).Dilate(6);
				mask= dif;
				changeImage = difC;
			}
			else
			{
				changeImage = history[0].AbsDiff(history[0]);
				mask = changeImage.Convert<Gray, Byte>();
			}

		}


		public Image<Gray, Byte> GetLastDepthChange()
		{
			if (depthHistory.Count > 1)
			{
				Image<Gray, Byte> dif = depthHistory[depthHistory.Count - 1].AbsDiff(depthHistory[depthHistory.Count - 2]);
				return dif;
			}
			else
			{
				return null;
			}
		}

		public void AddDepth(Image<Gray, Byte> img)
		{
			
			depthHistory.Add(img);
			if (depthHistory.Count > hSize)
			{
				Image<Gray, Byte> i = depthHistory[0];
				depthHistory.RemoveAt(0);
			//	i.Dispose();

			}
		}

	}
}

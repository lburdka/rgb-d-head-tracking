﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeadTracking
{

	//Zarzadza detektorami. Wysyla do nich nowe klatki i scala wyniki otrzymane od nich
	class OldHeadIntegrator
	{
		private HeadTask currentTask;
		private bool ready;
		private List<HeadDetector> detectors;
		private List<HeadModel> models;
		public static int MAX_HEADS=10;
		public static int NEARBY_RANGE = 50;
		private FaceDetector faceValidator;

		public List<HeadModel> Models
		{
			get { return models; }
		}

		//Sprawdzanie czy jest gotowe rozpoznanie modeli
		public bool Ready
		{
			get { return ready; }
		}

		public OldHeadIntegrator()
		{
			ready=false;
			faceValidator = new FaceDetector();
			models = new List<HeadModel>();
			detectors = new List<HeadDetector>();
			//detectors.Add(new TestDetector());

			//Disabled for sake of DSD testing
	//		detectors.Add(new BasicDetector());
	//		detectors.Add(new DepthSilhouetteDetector(this));
			detectors.Add(new DepthSilhouetteDetector2(new HeadIntegrator()));
			detectors.Add(new MotionDetector());
			//Tu dodac detektory
		}

		public void ResetDetectors()
		{
			for (int i = 0; i < detectors.Count; i++)
			{
				detectors[i].Reset();
			}
		}

		//Przyjmij obrazek, zrob nowy watek
		public void SendTask(HeadTask task)
		{
			models.Clear();
			ready = false;
			currentTask = task;

			Thread thread = new Thread(new ThreadStart(process));
			thread.Start();
		}

		//Przekaz zadanie do detektorow i scal wyniki ich pracy
		private void process()
		{
			try
			{
				List<Thread> tasksToDo = new List<Thread>();
				foreach (HeadDetector detector in detectors)
				{
					detector.SendTask(currentTask);
					Thread thread = new Thread(new ThreadStart(detector.Process));
					thread.Start();
					tasksToDo.Add(thread);
				}

				//Poczekaj az wszystkie zadania beda zrobione
				foreach (Thread t in tasksToDo)
				{
					t.Join();
				}

				List<HeadModel> tModels=new List<HeadModel>();

				//Scal
				foreach (HeadDetector detector in detectors)
				{
					foreach(DetectedHead head in detector.GetResponse()){
						if (head.Prob > 0)
						{
							
							tModels.Add(head.Model);
						}
						else
							break;
					}

				}
				//MessageBox.Show("Tasks done");

					for (int i = 0; i < tModels.Count; i++)
					{
						HeadModel head = tModels[i];
						bool merged = false;
							for (int j = i + 1; j < tModels.Count; j++)
							{
								HeadModel head2 = tModels[j];
								double distS = (head2.headX - head.headX) * (head2.headX - head.headX) + (head2.headY - head.headY) * (head2.headY - head.headY) + (head2.headZ - head.headZ) * (head2.headZ - head.headZ);
								if (distS < 2500)
								{
									HeadModel hm = new HeadModel((float)(head.headX + head2.headX) / 2, (float)(head.headY + head2.headY) / 2, (float)(head.headZ + head2.headZ) / 2, Math.Max(head.Radius , head2.Radius));
									hm.ColorBoundingRect = new Rectangle((head.ColorBoundingRect.X + head2.ColorBoundingRect.X) / 2, (head.ColorBoundingRect.Y + head2.ColorBoundingRect.Y) / 2, (head.ColorBoundingRect.Width + head2.ColorBoundingRect.Width) / 2, (head.ColorBoundingRect.Height + head2.ColorBoundingRect.Height) / 2);
									hm.DepthBoundingRect = new Rectangle((head.DepthBoundingRect.X + head2.DepthBoundingRect.X) / 2, (head.DepthBoundingRect.Y + head2.DepthBoundingRect.Y) / 2, (head.DepthBoundingRect.Width + head2.DepthBoundingRect.Width) / 2, (head.DepthBoundingRect.Height + head2.DepthBoundingRect.Height) / 2);
									hm.Probability = head.Probability * head2.Probability;
									//		HeadModel hm = new HeadModel((float)(head.Model.headX), (float)(head.Model.headY), (float)(head.Model.headZ), head.Model.Probability * head2.Model.Probability);
									models.Add(hm);
									tModels.Remove(head2);
									j--;
									merged = true;
									break;
								}

							}
							if (!merged)
							{
								head.Probability /= detectors.Count;
								models.Add(head);
						}
								
					}

					Image<Bgr, Byte> imgToValidate = currentTask.colorImage;
					foreach (HeadModel hm in models)
					{
						if (hm.Probability < 1)
						{
							if (hm.ColorBoundingRect.X > 0 && hm.ColorBoundingRect.X < 640 && hm.ColorBoundingRect.Y > 0 && hm.ColorBoundingRect.Height < 480 && hm.ColorBoundingRect.Width > 10 && hm.ColorBoundingRect.Height > 10)
							{
								if (faceValidator.Validate(imgToValidate.GetSubRect(hm.ColorBoundingRect).Resize(2.0,INTER.CV_INTER_LINEAR)))
								{
									if(SkinDetector.IsRegionSkin(imgToValidate, hm.ColorBoundingRect,0,0,0.2))
									{
										hm.Probability = 1.0;
									}
								}
							}
						}
					}


				

				ready = true;
			}
			catch(Exception e)
			{
				MessageBox.Show(e.StackTrace);
			}
		}




	}
}

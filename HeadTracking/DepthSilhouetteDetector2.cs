﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeadTracking
{
	class DepthSilhouetteDetector2: HeadDetector
	{

		private HeadTask task;
		private DetectedHead[] heads;
		private int AcceptableSpacing = 500;
		private HeadIntegrator integrator;
		private List<Rectangle> lastDetected;


		public DepthSilhouetteDetector2(HeadIntegrator hi)
		{
			integrator = hi;
		}

		public void Reset()
		{
			
		}

		public void SendTask(HeadTask task)
		{
			this.task = task;
		}

		public void Process()
		{
			


			heads = new DetectedHead[HeadIntegrator.MAX_HEADS];
			Emgu.CV.Image<Gray, Byte> img = ImageConverter.ShortsToGrayImage(task.DepthFrame, 640, 480, 1);
			//Fixing white area seems too expensive
			//fixWhiteAreas(img);
			
			//Noise reduction from depth image
			img = img.Erode(7);

			Emgu.CV.Image<Gray, Byte> origin = img.Copy();

			//Detect edges
			img = img.Canny(50, 70);


			//Rectangles detected as potential silhouetes
			List<Rectangle> silRects = new List<Rectangle>();

			using (MemStorage storage = new MemStorage())
			{
				for (
					//Link edge points into contours and iterate through them to select those that match criterions
					  Contour<Point> contours = img.FindContours(
						 Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_NONE,
						 Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL,
						 storage);
					  contours != null;
					  contours = contours.HNext)
				{

					int headX=getSuspectedHeadX(img, contours.BoundingRectangle);

					if (isHeightAcceptable(contours.BoundingRectangle, origin,headX))
					{
						//img.Draw(contours.BoundingRectangle, new Gray(80), 2);
						silRects.Add(contours.BoundingRectangle);
					}

				}
			}

			List<Int32> silHeights = new List<Int32>();
			for (int i = 0; i < silRects.Count; i++)
			{
				silHeights.Add(silRects[i].Height);
			}

			preprocessRectangles(silRects,origin,img);

			int indx = 0;

			//Filter and join output rectangles
			for (int i = 0; i < silRects.Count; i++)
			{
				System.Drawing.Rectangle rect = silRects[i];

				//Check if not invalidated
				if (rect.X != -999)
				{
					


					//For now we assume that the test was passed

					HeadModel head=findHead(rect, img,origin);
					head.Probability = 0.8;

					validateHead(head,silHeights[i],origin);

					//Check if it wasn't discarded
					if (head.rad != -1)
					{

						DetectedHead detHead = new DetectedHead(head, 1);
						if (indx < HeadIntegrator.MAX_HEADS)
						{
							heads[indx] = detHead;
							indx++;
						}

					}
					
				}
				
			}
			while (indx < HeadIntegrator.MAX_HEADS)
			{
				heads[indx] = new DetectedHead(new HeadModel(0, 0, 0, 10), 0);
				indx++;
			}
			if (silRects.Count > 0)
			{
				lastDetected = new List<Rectangle>();
				lastDetected.AddRange(silRects);
			}
		}

		private void validateHead(HeadModel head, int silHeight,Image<Gray,Byte> origin)
		{
			/*if (head.rad > silHeight / 5 || head.rad < silHeight / 15)
			{
				head.rad = -1;
			}*/
			double dpthVal = DepthColorMapper.RealDepth(head.headX,head.headY, origin);
			double realWidth = DepthColorMapper.GetRealWidth(head.rad, dpthVal)*2;
			if (realWidth<160 || realWidth>320)
			{
				head.rad = -1;
			}
			
		
			
		}

		private bool isHeightAcceptable(Rectangle rect, Emgu.CV.Image<Gray, Byte> img,int headX)
		{

			double cD=0;
			double realDepth = 0;

			int badPixels=0;

			for (int i = 0; i < rect.Height/5; i++)
			{
				cD += DepthColorMapper.Depth(rect.X + headX, rect.Y + i, img);
				double addition= DepthColorMapper.RealDepth(rect.X + headX, rect.Y + i, img);
				realDepth += addition;
				if (addition == 0)
					badPixels++;
			}

			
			cD /= (rect.Height/5);

			

			realDepth /= ((rect.Height / 5)-badPixels);

			double centralDpth = cD;// Math.Min(Math.Min(lD, rD), cD);
		//	MessageBox.Show(centralDpth.ToString());

			if (DepthColorMapper.GetRealHeight(rect.Height, realDepth) > 1000)
			{
				return true;
			}
			else
			{
				return false;
			}
			
			/*
			if (rect.Height >= 300 - 150 * (Math.Max(0, (centralDpth - 2) / 2)))
			{
				img.Draw(rect, new Gray(255), 2);
				String win1 = "Debug Window22";
				CvInvoke.cvNamedWindow(win1);

				CvInvoke.cvShowImage(win1, img.Ptr);
				MessageBox.Show(((rect.Height/5)-badPixels).ToString());
				MessageBox.Show(realDepth.ToString());
				MessageBox.Show(DepthColorMapper.GetRealHeight(rect.Height,realDepth).ToString());
				return true;
			}
			else
			{
				return false;
			}

			*/


		}

		

		private int getSuspectedHeadX(Emgu.CV.Image<Gray, Byte> silh, Rectangle rect)
		{
			int suspect = 0;
			int positions = 0;
			int positionCount = 0;
			byte[, ,] data = silh.Data;
			for (int k = 0; k < rect.Height / 5; k++)
			{

				for (int j = 0; j < rect.Width; j++)
				{
					if (data[rect.Y + k, rect.X + j, 0] > 0)
					{
						positionCount++;
						positions += j;
					}
				}
			}

			if (positionCount > 0)
			{
				positions /= positionCount;
			}
			else
			{
				positions = 0;
			}

			suspect = positions;

			return suspect;
			
		}

		private HeadModel findHead(Rectangle rect, Emgu.CV.Image<Gray, Byte> silh,Emgu.CV.Image<Gray, Byte> origin)
		{
			byte[, ,] data = silh.Data;
			if (rect.X>0&&rect.Y>0&&rect.X + rect.Width < silh.Width - 1 && rect.Y + rect.Height < silh.Height - 1)
			{

				//Skan all lines, searching for the silhouette Then return it's center
				double centers = 0;
				double width = 0;
				List<double> wdths = new List<double>();
				
				for (int i = 0; i < rect.Height - 1; i++)
				{
					bool silStarted = false;
					int silStart = 0;
					int silEnd = rect.Width;
					for (int j = 0; j < rect.Width - 1; j++)
					{
						if (data[rect.Y + i, rect.X + j, 0] > 0)
						{
							if (!silStarted)
							{
								silStart = j;
								silStarted = true;
							}
							else
							{
								silEnd = j;
								break;
							}
						}

					}
					centers += (silStart + silEnd) / 2;
					width += silEnd - silStart;
					wdths.Add((silEnd - silStart)*0.8);
				}

				width *= 0.8;

				
				wdths.Sort();

				/*for (int i = 0; i < wdths.Count; i++)
				{
					MessageBox.Show(i.ToString()+" "+wdths[i].ToString());
				}*/

			//	double medianWidth = wdths[wdths.Count / 2];

				Point dpthPos = new Point((int)(rect.X + centers / rect.Height) + 10, (int)(rect.Y + rect.Height / 2));

				

				Point colPos = dpthPos;

				//Mediana zamiast średniej

				//MessageBox.Show("AVG "+(width / rect.Height).ToString());
				//MessageBox.Show("median " + medianWidth.ToString());
				//MessageBox.Show("Real "+dpthPos.X +" "+dpthPos.Y+" "+ DepthColorMapper.GetRealWidth(medianWidth, dpthVal).ToString());

				HeadModel head = new HeadModel(colPos.X, colPos.Y, 0, width / rect.Height);
				//HeadModel head = new HeadModel(colPos.X, colPos.Y, 0,medianWidth/2);
				head.ColorBoundingRect = new Rectangle(colPos.X - (int)head.rad, colPos.Y - (int)head.rad, (int)head.rad * 2, (int)head.rad*2);
				head.DepthBoundingRect = new Rectangle(dpthPos.X - (int)head.rad, dpthPos.Y - (int)head.rad, (int)head.rad * 2, (int)head.rad * 2);
				return head;
			}
			else
			{
				return new HeadModel(0, 0, 0, 1);
			}
		}

		private void preprocessRectangles(List<Rectangle> silRects, Emgu.CV.Image<Gray, Byte> img, Emgu.CV.Image<Gray, Byte> silh)
		{
			for (int i = 0; i < silRects.Count; i++)
			{
				System.Drawing.Rectangle rect = silRects[i];

				//Check if not invalidated
				if (rect.X != -999)
				{


						//Narrow the rectangle to the area of interest
						rect.Height /= 5;

						//The silhouette was divided
						if (rect.Y < 5)
						{

							bool matchFound = false;

							//Check if there is another part of the silhouette detected
							for (int j = 0; j < silRects.Count; j++)
							{
								if (j != i)
								{
									Rectangle rect2 = silRects[j];
									//Also touches the top of screen
									if (rect2.Y < 5)
									{

										bool left = rect2.X < rect.X;

										//Check if it's close enough
										if (left && rect.X - rect2.X + rect2.Width < AcceptableSpacing)
										{
											rect.Width = rect.X + rect.Width - rect2.X;
											rect.X = rect2.X;

											//Invalidate
											rect2.X = -999;
											silRects[j] = rect2;
											matchFound = true;
											break;
										}
										else
										{
											if (!left && rect2.X - rect.X + rect.Width < AcceptableSpacing)
											{

												rect.Width = rect2.X + rect2.Width - rect.X;
												//Invalidate
												rect2.X = -999;
												silRects[j] = rect2;
												matchFound = true;
												break;
											}
										}
									}
								}
							}

							//The other part of silhouette wasn't detected as a rectangle.
							if (!matchFound)
							{
								//We check if top of the silhouette is more on left or right
								int pos = 0;
								int posC = 0;
								for (int j = 0; j < rect.Width; j++)
								{
									if (silh.Data[rect.Y, rect.X + j, 0] > 0)
									{
										pos += j;
										posC++;
									}
								}
								pos /= posC;

								//Head on left
								if (pos < rect.Width / 3)
								{
									rect.X -= rect.Width;
									rect.Width *= 2;
								}
								else
								{
									//Head on right
									if (pos > 2 * rect.Width / 3)
									{
										rect.Width *= 2;
									}
									else
									{
										//Head on center. We do nothing
									}
								}


							}
						}


				}
				silRects[i] = rect;
			}
			
		}

		private bool detectedNearby(Rectangle rect)
		{
			if (lastDetected != null)
			{
				for (int i = 0; i < lastDetected.Count; i++)
				{
					double dist = Math.Sqrt((rect.X - lastDetected[i].X) * (rect.X - lastDetected[i].X) + (rect.Y - lastDetected[i].Y) * (rect.Y - lastDetected[i].Y));
					if (dist < HeadIntegrator.NEARBY_RANGE)
					{
						return true;
					}
				}
			}
			return false;
		}

		public DetectedHead[] GetResponse()
		{
			return heads;
		}

		public double Weight()
		{
			return 0.6;
		}
	}
}

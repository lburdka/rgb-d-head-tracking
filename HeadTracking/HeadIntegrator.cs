﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeadTracking
{

	//Zarzadza detektorami. Wysyla do nich nowe klatki i scala wyniki otrzymane od nich
	class HeadIntegrator
	{
		private HeadTask currentTask;
		private bool ready;
		private List<HeadDetector> detectors;
		private List<HeadModel> models;
		public static int MAX_HEADS=10;
		public static int NEARBY_RANGE = 50;
		private FaceDetector faceValidator;
		DepthSilhouetteDetector2 depthDetector;
		MotionDetector motionDetector;
		MovementDetector movementDet;

		public List<HeadModel> Models
		{
			get { return models; }
		}

		//Sprawdzanie czy jest gotowe rozpoznanie modeli
		public bool Ready
		{
			get { return ready; }
		}

		public HeadIntegrator()
		{
			ready=false;
			faceValidator = new FaceDetector();
			depthDetector = new DepthSilhouetteDetector2(this);
			motionDetector = new MotionDetector();
			models = new List<HeadModel>();
			detectors = new List<HeadDetector>();
			movementDet = new MovementDetector(1);
			//detectors.Add(new TestDetector());

			//Disabled for sake of DSD testing
	//		detectors.Add(new BasicDetector());
	//		detectors.Add(new DepthSilhouetteDetector(this));
		//	detectors.Add(new DepthSilhouetteDetector2(this));
		//	detectors.Add(new MotionDetector());
			//Tu dodac detektory
		}

		public void ResetDetectors()
		{
			for (int i = 0; i < detectors.Count; i++)
			{
				detectors[i].Reset();
			}
		}

		//Przyjmij obrazek, zrob nowy watek
		public void SendTask(HeadTask task)
		{
			models.Clear();
			ready = false;
			currentTask = task;

		//	process();

			Thread thread = new Thread(new ThreadStart(process));
			thread.Start();
		}

		//Przekaz zadanie do detektorow i scal wyniki ich pracy
		private void process()
		{
			try
			{
				List<Thread> tasksToDo = new List<Thread>();


				//Dodac tu zadania
				depthDetector.SendTask(currentTask);
			//	depthDetector.Process();
				Thread dThread = new Thread(new ThreadStart(depthDetector.Process));
				dThread.Start();
				tasksToDo.Add(dThread);

				motionDetector.SendTask(currentTask);
				Thread mThread = new Thread(new ThreadStart(motionDetector.ExtractForeground));
				mThread.Start();
				tasksToDo.Add(mThread);

				

				//Poczekaj az wszystkie zadania beda zrobione
				foreach (Thread t in tasksToDo)
				{
					t.Join();
				}

				//Scal
				foreach(DetectedHead head in depthDetector.GetResponse()){
					if (head.Prob > 0)
					{
							
						models.Add(head.Model);
					}
					else
						break;
				}

				

				Image<Bgr, Byte> imgToValidate = currentTask.colorImage;
			//	movementDet.Add(imgToValidate);
			//	movementDet.AddDepth(ImageConverter.ShortsToGrayImage(currentTask.DepthFrame,640,480,1).Dilate(1));
				foreach (HeadModel hm in models)
				{
					if (hm.Probability < 1)
					{
						if (hm.ColorBoundingRect.X > 0 && hm.ColorBoundingRect.X < 640 && hm.ColorBoundingRect.Y > 0 && hm.ColorBoundingRect.Height < 480 && hm.ColorBoundingRect.Width > 10 && hm.ColorBoundingRect.Height > 10)
						{
							//Check if face
							if (faceValidator.Validate(imgToValidate.GetSubRect(hm.ColorBoundingRect).Resize(2.0,INTER.CV_INTER_LINEAR)))
							{
								if(SkinDetector.IsRegionSkin(imgToValidate, hm.ColorBoundingRect,0,0,0.2))
								{
									hm.Probability = 1.0;
								}
							}

							//Check if moving
							if (hm.Probability < 1)
							{
								if (motionDetector.isForeground(hm.ColorBoundingRect) > 0.05)
								{
									
									hm.Probability = 1.0;
								}
							}
						}
					}
				}

				motionDetector.isForeground(new Rectangle(0, 0, 1, 1));
				

				ready = true;
			}
			catch(Exception e)
			{
				MessageBox.Show(e.StackTrace);
			}
		}




	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;

namespace HeadTracking
{
	public class HeadFrameReadyEventArgs : EventArgs
	{
		public HeadModel[] heads;
		public Image<Bgr, Byte> colorFrame;
		public Image<Gray, Byte> depthFrame;
		public byte[] rawColorData;

		public HeadFrameReadyEventArgs(HeadModel[] models)
		{
			heads = models;
		}
	}
}

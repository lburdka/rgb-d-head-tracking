﻿using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeadTracking
{
	public class SkinDetector
	{
		public SkinDetector()
		{
			
		}

		public static byte[] ShowSkinRegion(byte[] image)
		{
			Emgu.CV.Image<Bgr, Byte> img = ImageConverter.BytesToColorImage(image, 640, 480, 4);
			Rectangle r = SkinDetector.GetSkinRegion(img, 5);

			for (int x = 0; x < r.Width; x++)
			{
				img[r.Y, x] = new Bgr(255, 0, 0);
				img[r.Y+r.Height, x] = new Bgr(255, 0, 0);
				
			}
			for (int y = 0; y < r.Height; y++)
			{
				img[y, r.X] = new Bgr(255, 0, 0);
				img[y, r.X+r.Width] = new Bgr(255, 0, 0);
			}
			return ImageConverter.ColorImageToBytes(img);
		}

		public static byte[] ShowSkin(byte[] image)
		{
			Emgu.CV.Image<Bgr, Byte> img = ImageConverter.BytesToColorImage(image, 640, 480,4);
			for (int h = 0; h < 480; h++)
			{
				for (int w = 0; w < 640; w++)
				{
					if (IsSkin(img, w, h))
					{
						img[h, w] = new Bgr(255,0,0);

					}
					
				}
			}

			return ImageConverter.ColorImageToBytes(img);
			
		}

		public static bool IsRegionSkin(Emgu.CV.Image<Bgr, Byte> img, Rectangle reg,int margX,int margY,double threshold=0.2)
		{
			int count = 0;
			int passed=0;
			double r=0;
			double b=0;
			double g = 0;
			for (int w = 0; w < reg.Width; w++)
			{
				for (int h = 0; h < reg.Height; h++)
				{


					b = img.Data[margY + reg.Y + h, margX + reg.X + w, 0];
					g = img.Data[margY + reg.Y + h, margX + reg.X + w, 1];
					r = img.Data[margY + reg.Y + h, margX + reg.X + w, 2];
					count++;
					if (isColorSkin(b, g, r))
					{
						passed++;
					}
				}
			}

			return (passed > count *threshold);
		}

		private static bool isColorSkin(double b, double g, double r)
		{
			if (r > 95 && g > 40 && b > 20)
			{
				double max = Math.Max(b, Math.Max(g, r));
				double min = Math.Min(b, Math.Min(g, r));
				if (max - min > 15 && Math.Abs(r - g) > 15 && r > g && r > b)
				{
					return true;
				}
			}
			return false;
		}

		public static Rectangle GetSkinRegion(Emgu.CV.Image<Bgr, Byte> img, int step = 5)
		{
			int minX = img.Width;
			int maxX = 0;
			int minY = img.Height;
			int maxY = 0;

			bool[] previousColumn = new bool[img.Height];

			for (int h = 0; h < img.Height; h++)
			{
				previousColumn[h] = false;
			}
			for (int w = 0; w < img.Width; w += step)
			{
				bool previous = false;
				for (int h = 0; h < img.Height; h += step)
				{
					if (IsSkin(img, w, h))
					{
						if (previousColumn[h]&&previous)
						{
							if (w < minX)
								minX = w;
							if (w > maxX)
								maxX = w;
							if (h < minY)
								minY = h;
							if (h > maxY)
								maxY = h;
						}
						previousColumn[h] = true;
						previous = true;
					}
					else
					{
						previousColumn[h] = false;
						previous = false;
					}
				}
			}

			if (minX > 20)
				minX -= 20;
			if (maxX < img.Width - 20)
				maxX += 20;
			if (minY > 20)
				minY -= 20;
			if (maxY < img.Height - 20)
				maxY += 20;

			Rectangle r = new Rectangle(minX, minY, maxX - minX, maxY - minY);
			return r;
		}

		//Metoda zaadoptowana z publikacji A Survey on Pixel-Based Skin Color Detection Techniques
		public static bool IsSkin(Emgu.CV.Image<Bgr, Byte> img, int x, int y)
		{
			
			double b= img.Data[y, x,0];
			double g = img.Data[y, x, 1];
			double r = img.Data[y, x, 2];
			
			return isColorSkin(b,g,r);
		}

	}
}

﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeadTracking
{
	class TMTracker
	{
		Point Location;
		Point smallLocation;
		Point bigLocation;
		public Image<Gray, double> Results { get; set; }
		public Image<Gray, double> smallResults { get; set; }
		public Image<Gray, double> bigResults { get; set; }

		Image<Gray, Byte> smallTrackedObject;
		Image<Gray, Byte> trackedObject;
		Image<Gray, Byte> bigTrackedObject;
		public bool Initialized;
		//int marg = 50;
		bool busy = false;
		double result;
		double smallResult;
		double bigResult;

		Point smallMove;
		Point move;
		Point bigMove;

		MovementDetector moveDetector;
		TMMoveTracker tmMoveTracker;

		internal TMImprovedDepthTracker IdTracker
		{
			get;
			set;
		}

		public TMTracker(HeadTracker ht)
		{
			Initialized = false;
			moveDetector = new MovementDetector(3);
			tmMoveTracker = new TMMoveTracker(ht, moveDetector);
		}

		public bool IsSeriousShitMrProgram()
		{
			bool yeeees = false;

			double grade = moveDetector.GetMask().GetAverage().Intensity;
			if (grade > 40)
			{
			//	MessageBox.Show("o kuuurwa");
				yeeees = true;
			}

			return yeeees;
		}

		public bool ImCoolAsAShaolinMonk()
		{
			bool yeeees = false;

			double grade = moveDetector.GetMask().GetAverage().Intensity;
			if (grade < 0)
			{
				//	MessageBox.Show("I'm soo cool");
				yeeees = true;
			}

			return yeeees;
		}

		public void PassInitialFrame(Image<Bgr, Byte> modelImage, List<HeadModel> models)
		{
			Initialized = true;
			smallMove = new Point(0,0);
			move = new Point(0,0);
			bigMove = new Point(0,0);
			moveDetector.Add(modelImage);
			tmMoveTracker.PassInitialFrame(modelImage, models);
			//modelImage=modelImage.And(moveDetector.GetMask().Convert<Bgr,Byte>());
			foreach (HeadModel model in models)
			{
				if (model.Probability == 1.0)
				{
					
					Rectangle roi = model.ColorBoundingRect;
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					trackedObject = modelImage.GetSubRect(roi).Convert<Gray, Byte>();
					Location = roi.Location;

					roi = model.ColorBoundingRect;
					roi = new Rectangle(roi.X + roi.Width / 4, roi.Y + roi.Height / 4, roi.Width / 2, roi.Height / 2);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					smallTrackedObject = modelImage.GetSubRect(roi).Convert<Gray, Byte>();
					smallLocation = roi.Location;

					roi = model.ColorBoundingRect;
					//roi = new Rectangle(roi.X - roi.Width / 4, roi.Y - roi.Height / 4,(int)( roi.Width*1.5),(int)( roi.Height*1.5));
					roi = new Rectangle(roi.X + (int)(roi.Width * (3.0 / 8.0)), roi.Y + (int)(roi.Height * (3.0 / 8.0)), roi.Width / 4, roi.Height / 4);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					bigTrackedObject = modelImage.GetSubRect(roi).Convert<Gray, Byte>();
					bigLocation = roi.Location;

					break;
				}
			}
		}

		public void Update(Image<Bgr, Byte> modelImage,List<HeadModel> models)
		{
			//return;

			moveDetector.Add(modelImage);
			tmMoveTracker.Update(modelImage, models);
			//modelImage = moveDetector.GetLastChange();
			//modelImage = modelImage.And(moveDetector.GetMask().Convert<Bgr,Byte>());

			bool sure = false;
			foreach (HeadModel model in models)
			{
				if (model.Probability == 1.0)
				{
					Rectangle roi = model.ColorBoundingRect;
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					trackedObject = modelImage.GetSubRect(roi).Convert<Gray, Byte>();
					Location = roi.Location;

					roi = model.ColorBoundingRect;
					roi = new Rectangle(roi.X + roi.Width / 4, roi.Y + roi.Height / 4, roi.Width / 2, roi.Height / 2);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					smallTrackedObject = modelImage.GetSubRect(roi).Convert<Gray, Byte>();
					smallLocation = roi.Location;

					roi = model.ColorBoundingRect;
					//roi = new Rectangle(roi.X - roi.Width / 4, roi.Y - roi.Height / 4, (int)(roi.Width * 1.5), (int)(roi.Height * 1.5));
					roi = new Rectangle(roi.X + (int)(roi.Width * (3.0 / 8.0)), roi.Y + (int)(roi.Height * (3.0 / 8.0)), roi.Width / 4, roi.Height / 4);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					bigTrackedObject = modelImage.GetSubRect(roi).Convert<Gray, Byte>();
					bigLocation = roi.Location;

					sure = true;
					break;
				}
			}

			//Compare if tracked head matches one of detected heads
			if (!sure)
			{
				//Find the best model to track
				HeadModel model = null;
				double bestProb=0;
				foreach (HeadModel hm in models)
				{
					Point colorCenter = new Point(hm.ColorBoundingRect.X + hm.ColorBoundingRect.Width / 2, hm.ColorBoundingRect.Y + hm.ColorBoundingRect.Height / 2);
					if (colorCenter.X > smallLocation.X && colorCenter.X < smallLocation.X + smallTrackedObject.Width && colorCenter.Y > smallLocation.Y && colorCenter.Y < smallLocation.Y + smallTrackedObject.Height)
					{
						if (hm.Probability > bestProb)
						{
							model = hm;
							bestProb = hm.Probability;
						}
					}
					if (colorCenter.X > Location.X && colorCenter.X < Location.X + trackedObject.Width && colorCenter.Y > Location.Y && colorCenter.Y < Location.Y+trackedObject.Height)
					{
						model = hm;
						bestProb = hm.Probability;
					}
				/*	if (colorCenter.X > bigLocation.X && colorCenter.X < bigLocation.X + bigTrackedObject.Width && colorCenter.Y > bigLocation.Y && colorCenter.Y < bigLocation.Y+bigTrackedObject.Height)
					{
						model = hm;
						bestProb = hm.Probability;
					}*/
				}
				if (model != null)
				{
					Rectangle roi = model.ColorBoundingRect;
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					trackedObject = modelImage.GetSubRect(roi).Convert<Gray, Byte>();
					Location = roi.Location;

					roi = model.ColorBoundingRect;
					roi = new Rectangle(roi.X + roi.Width / 4, roi.Y + roi.Height / 4, roi.Width / 2, roi.Height / 2);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					smallTrackedObject = modelImage.GetSubRect(roi).Convert<Gray, Byte>();
					smallLocation = roi.Location;

					roi = model.ColorBoundingRect;
					roi = new Rectangle(roi.X - roi.Width / 4, roi.Y - roi.Height / 4, (int)(roi.Width * 1.5), (int)(roi.Height * 1.5));
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					bigTrackedObject = modelImage.GetSubRect(roi).Convert<Gray, Byte>();
					bigLocation = roi.Location;
				}
			}
		}

		public void Track(Image<Bgr, Byte> targetImage)
		{
		//	double good = 0.1;
			double sooooBad = 0.95;
			Image<Bgr, Byte> origin = targetImage.Copy();
			moveDetector.Add(targetImage);
			//targetImage = moveDetector.GetLastChange();
			//targetImage = targetImage.And(moveDetector.GetMask().Convert<Bgr,Byte>());
			tmMoveTracker.Track(targetImage);
			int margL = 50;
			int margT = 50;
			int margB = 50;
			int margR = 50;
			//marg = 50;
			if (!busy)
			{
				busy = true;
				Image<Bgr, Byte> targetImageCopy = targetImage.Copy();
				
				Point oldLocation = Location;
				margL = Math.Min(margL, Location.X);
				margT = Math.Min(margT, Location.Y);
				margR = Math.Min(margR, 639 - (Location.X + trackedObject.Width));
				margB = Math.Min(margB, 479 - (Location.Y + trackedObject.Height));

				int minX = Location.X - margL;
				int maxX = Location.X + trackedObject.Width + margR;
				int minY = Location.Y - margT;
				int maxY = Location.Y + trackedObject.Height + margB;

				Image<Gray, Byte> area = targetImage.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY)).Convert<Gray, Byte>();
				
				if (DetectObject(area, trackedObject, 0))
				{

				/*	if (result > 0.8)
					{*/
						Location.X += oldLocation.X - margL;
						Location.Y += oldLocation.Y - margT;

					if (!Location.Equals(oldLocation))
					{
						/*	if(!moveDetector.IsMoving(new Rectangle(oldLocation.X,oldLocation.Y,trackedObject.Width,trackedObject.Height))){
								Location=oldLocation;
							}
							else
							{*/
						if (IsSeriousShitMrProgram() && result < sooooBad)
							//	if(result<sooooBad)
							//	if (!moveDetector.IsMoving(new Rectangle(Location.X, Location.Y, trackedObject.Width, trackedObject.Height)))
								{
							//		MessageBox.Show("case " + tmMoveTracker.Location.X + " " + tmMoveTracker.Location.Y + " " + Location.X+" "+Location.Y);
								/*	Location.X = tmMoveTracker.Location.X;
									Location.Y = tmMoveTracker.Location.Y;*/
								//	Point ide = IdTracker.bigLocation;
								//	Point iLoc = DepthColorMapper.SimpleDepthToColorCoords(ide.X, ide.Y);
								//	Location.X = iLoc.X;
								//	Location.Y = iLoc.Y;
								//	Location = ide;
									
								}
						if (ImCoolAsAShaolinMonk())
						{
						//	IdTracker.bigLocation = Location;
						}
						/*	}

							*/
						
					}
				/*	if (result > good)
					{
						tmMoveTracker.Location.X = Location.X;
						tmMoveTracker.Location.Y = Location.Y;
					}*/

						move.X = Location.X - oldLocation.X;
						move.Y = Location.Y - oldLocation.Y;
						Point[] pts = { new Point(Location.X, Location.Y), new Point(Location.X + trackedObject.Width, Location.Y), new Point(Location.X + trackedObject.Width, Location.Y + trackedObject.Height), new Point(Location.X, Location.Y + trackedObject.Height) };
						targetImageCopy.DrawPolyline(pts, true, new Bgr(0, (int)(255 * (result - 0.5) * 2), 0), 3);

						minX = Math.Min(Math.Max(Location.X, 1), 638);
						minY = Math.Min(Math.Max(Location.Y, 1), 478);
						maxX = Math.Max(2, Math.Min(Location.X + trackedObject.Width, 639));
						maxY = Math.Max(2, Math.Min(Location.Y + trackedObject.Height, 479));


						trackedObject = origin.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY)).Convert<Gray, Byte>();
				/*		if (result > good)
						{
					//		tmMoveTracker.trackedObject = trackedObject;
						}*/
					/*}
					else
					{
						Location.X = oldLocation.X + move.X;
						Location.Y = oldLocation.Y + move.Y;
						Point[] pts = { new Point(Location.X, Location.Y), new Point(Location.X + trackedObject.Width, Location.Y), new Point(Location.X + trackedObject.Width, Location.Y + trackedObject.Height), new Point(Location.X, Location.Y + trackedObject.Height) };
						targetImageCopy.DrawPolyline(pts, true, new Bgr(0, (int)(255 * (result - 0.5) * 2), 0), 3);
					}*/
					
				//	trackedObject = targetImage.GetSubRect(new Rectangle(Location.X, Location.Y, trackedObject.Width, trackedObject.Height)).Convert<Gray,Byte>();
				}

				oldLocation = smallLocation;
				margL = Math.Min(margL, smallLocation.X);
				margT = Math.Min(margT, smallLocation.Y);
				margR = Math.Min(margR, 639 - (smallLocation.X + smallTrackedObject.Width));
				margB = Math.Min(margB, 479 - (smallLocation.Y + smallTrackedObject.Height));

				minX = smallLocation.X - margL;
				maxX = smallLocation.X + smallTrackedObject.Width + margR;
				minY = smallLocation.Y - margT;
				maxY = smallLocation.Y + smallTrackedObject.Height + margB;

				area = targetImage.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY)).Convert<Gray, Byte>();
				if (DetectObject(area, smallTrackedObject, 1))
				{
					/*if (smallResult > 0.8)
					{*/
						smallLocation.X += oldLocation.X - margL;
						smallLocation.Y += oldLocation.Y - margT;

						if (!smallLocation.Equals(oldLocation))
						{
						/*	if (!moveDetector.IsMoving(new Rectangle(oldLocation.X, oldLocation.Y, smallTrackedObject.Width, smallTrackedObject.Height)))
							{
								smallLocation = oldLocation;
							}
							else
							{*/
						//	if (smallResult < sooooBad)
							if (IsSeriousShitMrProgram() && smallResult < sooooBad)
							//	if (!moveDetector.IsMoving(new Rectangle(smallLocation.X, smallLocation.Y, smallTrackedObject.Width, smallTrackedObject.Height)))
								{
							//		smallLocation.X = tmMoveTracker.smallLocation.X;
							//		smallLocation.Y = tmMoveTracker.smallLocation.Y;
									Point ide = IdTracker.Location;
								//	Point iLoc = DepthColorMapper.SimpleDepthToColorCoords(ide.X, ide.Y);
									smallLocation = ide;
								}
							if (ImCoolAsAShaolinMonk())
							{
								IdTracker.Location = smallLocation;
							}
						/*	}*/

							
						}
			/*			if (smallResult > good)
						{
							tmMoveTracker.smallLocation.X = smallLocation.X;
							tmMoveTracker.smallLocation.Y = smallLocation.Y;
						}*/
						smallMove.X = smallLocation.X - oldLocation.X;
						smallMove.Y = smallLocation.Y - oldLocation.Y;
						Point[] pts = { new Point(smallLocation.X, smallLocation.Y), new Point(smallLocation.X + smallTrackedObject.Width, smallLocation.Y), new Point(smallLocation.X + smallTrackedObject.Width, smallLocation.Y + smallTrackedObject.Height), new Point(smallLocation.X, smallLocation.Y + smallTrackedObject.Height) };
						targetImageCopy.DrawPolyline(pts, true, new Bgr(0, 0, (int)(255 * (smallResult - 0.5) * 2)), 3);

						minX = Math.Min(Math.Max(smallLocation.X, 1), 638);
						minY = Math.Min(Math.Max(smallLocation.Y, 1), 478);
						maxX = Math.Max(2, Math.Min(smallLocation.X + smallTrackedObject.Width, 639));
						maxY = Math.Max(2, Math.Min(smallLocation.Y + smallTrackedObject.Height, 479));

						
						smallTrackedObject = origin.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY)).Convert<Gray, Byte>();
					/*	if (smallResult > good)
						{
					//		tmMoveTracker.smallTrackedObject = smallTrackedObject;
						}*/
				/*	}
					else
					{
						smallLocation.X = oldLocation.X + smallMove.X;
						smallLocation.Y = oldLocation.Y + smallMove.Y;
						Point[] pts = { new Point(smallLocation.X, smallLocation.Y), new Point(smallLocation.X + smallTrackedObject.Width, smallLocation.Y), new Point(smallLocation.X + smallTrackedObject.Width, smallLocation.Y + smallTrackedObject.Height), new Point(smallLocation.X, smallLocation.Y + smallTrackedObject.Height) };
						targetImageCopy.DrawPolyline(pts, true, new Bgr(0, 0, (int)(255 * (smallResult - 0.5) * 2)), 3);
					}*/
				//	smallTrackedObject = targetImage.GetSubRect(new Rectangle(smallLocation.X, smallLocation.Y, smallTrackedObject.Width, smallTrackedObject.Height)).Convert<Gray, Byte>();
				}

				oldLocation = bigLocation;
				margL = Math.Min(margL, bigLocation.X);
				margT = Math.Min(margT, bigLocation.Y);
				margR = Math.Min(margR, 639 - (bigLocation.X + bigTrackedObject.Width));
				margB = Math.Min(margB, 479 - (bigLocation.Y + bigTrackedObject.Height));

				minX = bigLocation.X - margL;
				maxX = bigLocation.X + bigTrackedObject.Width + margR;
				minY = bigLocation.Y - margT;
				maxY = bigLocation.Y + bigTrackedObject.Height + margB;

				area = targetImage.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY)).Convert<Gray, Byte>();
				if (DetectObject(area, bigTrackedObject, 2))
				{
					/*if (bigResult > 0.8)
					{*/
						bigLocation.X += oldLocation.X - margL;
						bigLocation.Y += oldLocation.Y - margT;

						if (!bigLocation.Equals(oldLocation))
						{
						/*	if (!moveDetector.IsMoving(new Rectangle(oldLocation.X, oldLocation.Y, bigTrackedObject.Width, bigTrackedObject.Height)))
							{
								bigLocation = oldLocation;
							}
						/*	else
							{*/
						//	if (bigResult < sooooBad)
							if (IsSeriousShitMrProgram() && bigResult < sooooBad)
							//	if (!moveDetector.IsMoving(new Rectangle(bigLocation.X, bigLocation.Y, bigTrackedObject.Width, bigTrackedObject.Height)))
								{
								//	bigLocation.X = tmMoveTracker.bigLocation.X;
								//	bigLocation.Y = tmMoveTracker.bigLocation.Y; Point ide = IdTracker.Location;
								//	Point ide = IdTracker.smallLocation;
								//    Point iLoc = DepthColorMapper.SimpleDepthToColorCoords(ide.X, ide.Y);
								//	bigLocation = ide;

								}
							if (ImCoolAsAShaolinMonk())
							{
							//	IdTracker.smallLocation = bigLocation;
							}
							/*}
							*/
							
						}
				/*		if (bigResult > good)
						{
							tmMoveTracker.bigLocation.X = bigLocation.X;
							tmMoveTracker.bigLocation.Y = bigLocation.Y;
						}*/

						bigMove.X = bigLocation.X - oldLocation.X;
						bigMove.Y = bigLocation.Y - oldLocation.Y;
						Point[] pts = { new Point(bigLocation.X, bigLocation.Y), new Point(bigLocation.X + bigTrackedObject.Width, bigLocation.Y), new Point(bigLocation.X + bigTrackedObject.Width, bigLocation.Y + bigTrackedObject.Height), new Point(bigLocation.X, bigLocation.Y + bigTrackedObject.Height) };
						targetImageCopy.DrawPolyline(pts, true, new Bgr((int)(255 * (bigResult - 0.5) * 2), 0, 0), 3);

						minX = Math.Min(Math.Max(bigLocation.X, 1), 638);
						minY = Math.Min(Math.Max(bigLocation.Y, 1), 478);
						maxX = Math.Max(2, Math.Min(bigLocation.X + bigTrackedObject.Width, 639));
						maxY = Math.Max(2, Math.Min(bigLocation.Y + bigTrackedObject.Height, 479));

						
						bigTrackedObject = origin.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY)).Convert<Gray, Byte>();
					if(bigResult>0.8)	{
					
					//	tmMoveTracker.bigTrackedObject = bigTrackedObject;
					}
					/*}
					else
					{
						bigLocation.X = oldLocation.X + bigMove.X;
						bigLocation.Y = oldLocation.Y + bigMove.Y;
						Point[] pts = { new Point(bigLocation.X, bigLocation.Y), new Point(bigLocation.X + bigTrackedObject.Width, bigLocation.Y), new Point(bigLocation.X + bigTrackedObject.Width, bigLocation.Y + bigTrackedObject.Height), new Point(bigLocation.X, bigLocation.Y + bigTrackedObject.Height) };
						targetImageCopy.DrawPolyline(pts, true, new Bgr((int)(255 * (bigResult - 0.5) * 2), 0, 0), 3);
					}*/

					//bigTrackedObject = targetImage.GetSubRect(new Rectangle(bigLocation.X, bigLocation.Y, bigTrackedObject.Width, bigTrackedObject.Height)).Convert<Gray, Byte>();
				}


		//		CenterByBest(targetImage);


				String win1 = "Debug Window2";
				CvInvoke.cvNamedWindow(win1);

				CvInvoke.cvShowImage(win1, targetImageCopy.Ptr);
				busy = false;
			}
		}

		private void CenterByBest(Image<Bgr, Byte> target)
		{
			//Small is the best
			if (smallResult >= result)
			{
				bigLocation = new Point(smallLocation.X + smallTrackedObject.Width / 2 - bigTrackedObject.Width/2, smallLocation.Y + smallTrackedObject.Height / 2 - bigTrackedObject.Height/2);
				Location = new Point(smallLocation.X + smallTrackedObject.Width / 2 - trackedObject.Width/2, smallLocation.Y + smallTrackedObject.Height / 2 - trackedObject.Height/2);
				bigTrackedObject = target.GetSubRect(new Rectangle(bigLocation.X, bigLocation.Y, bigTrackedObject.Width, bigTrackedObject.Height)).Convert<Gray, Byte>();
				trackedObject = target.GetSubRect(new Rectangle(Location.X, Location.Y, trackedObject.Width, trackedObject.Height)).Convert<Gray, Byte>();
			}
			else
			{
				//Result is the best
				if (result >= smallResult)
				{
					bigLocation = new Point(Location.X + trackedObject.Width / 2 - bigTrackedObject.Width/2, Location.Y + trackedObject.Height / 2 - bigTrackedObject.Height/2);
					smallLocation = new Point(Location.X + trackedObject.Width / 2 - smallTrackedObject.Width/2, Location.Y + trackedObject.Height / 2 - smallTrackedObject.Height/2);
					bigTrackedObject = target.GetSubRect(new Rectangle(bigLocation.X, bigLocation.Y, bigTrackedObject.Width, bigTrackedObject.Height)).Convert<Gray, Byte>();
					smallTrackedObject = target.GetSubRect(new Rectangle(smallLocation.X, smallLocation.Y, smallTrackedObject.Width, smallTrackedObject.Height)).Convert<Gray, Byte>();
				}

/*				else
				{
					//Big is the best
					smallLocation = new Point(bigLocation.X + bigTrackedObject.Width / 2 - smallTrackedObject.Width/2, bigLocation.Y + bigTrackedObject.Height / 2 - smallTrackedObject.Height/2);
					Location = new Point(bigLocation.X + bigTrackedObject.Width / 2 - trackedObject.Width/2, bigLocation.Y + bigTrackedObject.Height / 2 - trackedObject.Height/2);
					smallTrackedObject = target.GetSubRect(new Rectangle(smallLocation.X, smallLocation.Y, smallTrackedObject.Width, smallTrackedObject.Height)).Convert<Gray, Byte>();
					trackedObject = target.GetSubRect(new Rectangle(Location.X, Location.Y, trackedObject.Width, trackedObject.Height)).Convert<Gray, Byte>();
				}*/
			}
		}
		

		private bool DetectObject(Image<Gray, Byte> Area_Image, Image<Gray, Byte> image_object,int variant)
		{
			bool success = false;

			//Work out padding array size
			Point dftSize = new Point(Area_Image.Width + (image_object.Width * 2), Area_Image.Height + (image_object.Height * 2));
			//Pad the Array with zeros
			using (Image<Gray, Byte> pad_array = new Image<Gray, Byte>(dftSize.X, dftSize.Y))
			{
				//copy centre
				pad_array.ROI = new Rectangle(image_object.Width, image_object.Height, Area_Image.Width, Area_Image.Height);
				CvInvoke.cvCopy(Area_Image, pad_array, IntPtr.Zero);

				pad_array.ROI = (new Rectangle(0, 0, dftSize.X, dftSize.Y));

				//Match Template
				using (Image<Gray, float> result_Matrix = pad_array.MatchTemplate(image_object, TM_TYPE.CV_TM_CCOEFF_NORMED))
				{
					/*String win1 = "Debug Window3";
					CvInvoke.cvNamedWindow(win1);

					CvInvoke.cvShowImage(win1, result_Matrix.Ptr);
					*/
					Point[] MAX_Loc, Min_Loc;
					double[] min, max;
					//Limit ROI to look for Match

					if (Area_Image.Width - image_object.Width <= 0 || Area_Image.Height - image_object.Height<=0)
					{
						return false;
					}

					result_Matrix.ROI = new Rectangle(image_object.Width, image_object.Height, Area_Image.Width - image_object.Width, Area_Image.Height - image_object.Height);

					result_Matrix.MinMax(out min, out max, out Min_Loc, out MAX_Loc);

					//if (max[0] > 0.5)
					//{
						success = true;
						if (variant == 0)
						{
							Location = new Point((MAX_Loc[0].X), (MAX_Loc[0].Y));
							Results = result_Matrix.Convert<Gray, Double>();
							result = max[0];
						}
						if (variant == 1)
						{
							smallLocation = new Point((MAX_Loc[0].X), (MAX_Loc[0].Y));
							smallResults = result_Matrix.Convert<Gray, Double>();
							smallResult = max[0];
						}
						if (variant == 2)
						{
							bigLocation = new Point((MAX_Loc[0].X), (MAX_Loc[0].Y));
							bigResults = result_Matrix.Convert<Gray, Double>();
							bigResult = max[0];
						}

					//}
				}
			}
			return success;
		}

		
	}
}

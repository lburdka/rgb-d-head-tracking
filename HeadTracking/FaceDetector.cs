﻿using Emgu.CV.GPU;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeadTracking
{
	class FaceDetector
	{
		private GpuCascadeClassifier faceClassifier;
		public FaceDetector()
		{

			try
			{
				faceClassifier = new GpuCascadeClassifier("haarcascade_frontalface_default.xml");
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		public void Detect(Emgu.CV.Image<Bgr, Byte> image, List<Rectangle> faces)
		{
			using (GpuImage<Bgr, Byte> gpuImage = new GpuImage<Bgr, byte>(image))
			using (GpuImage<Gray, Byte> gpuGray = gpuImage.Convert<Gray, Byte>())
			{
				Rectangle[] faceRegion = faceClassifier.DetectMultiScale(gpuGray, 2, 3, new Size(20, 20));
				faces.AddRange(faceRegion);
			}
		}

		public bool Validate(Emgu.CV.Image<Bgr, Byte> image)
		{
			List<Rectangle> faces = new List<Rectangle>();
			using (GpuImage<Bgr, Byte> gpuImage = new GpuImage<Bgr, byte>(image))
			using (GpuImage<Gray, Byte> gpuGray = gpuImage.Convert<Gray, Byte>())
			{
				Rectangle[] faceRegion = faceClassifier.DetectMultiScale(gpuGray,1.5, 3, new Size(20, 20));
				faces.AddRange(faceRegion);
			}
			if (faces.Count > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

	}
}

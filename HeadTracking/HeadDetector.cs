﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadTracking
{

	//Wykrywa glowe na obrazie przy uzyciu roznych metod
	interface HeadDetector
	{
		//Send the new detection task
		void SendTask(HeadTask task);


		//Przetworz zadanie w nowym watku
		void Process();

		//Get the response after succesfull processing
		DetectedHead[] GetResponse();

		void Reset();

		double Weight();
	}
}

﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadTracking
{
	public class HeadTask
	{
		private byte[] colorFrame;
		private short[] depthFrame;

		public Image<Bgr, Byte> colorImage;
		public Image<Gray, Byte> depthImage;


		public byte[] ColorFrame
		{
			get { return colorFrame; }
			set { colorFrame = value; }
		}

		public short[] DepthFrame
		{
			get { return depthFrame; }
			set { depthFrame = value; }
		}

		public HeadTask(byte[] colorFrame, short[] depthFrame)
		{
			this.colorFrame = colorFrame;
			this.depthFrame = depthFrame;
		}
	}
}

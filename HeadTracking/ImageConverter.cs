﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeadTracking
{
	public static class ImageConverter
	{

		

		public static Image<Bgr, Byte> Compose(Image<Bgr, Byte> color, Image<Gray, Byte> depth)
		{
			
			Image<Bgr, Byte> ret = color.Mul(0.5).Add(depth.Convert<Bgr, Byte>().Mul(0.5));
			String win1 = "Debug WindowX";
			CvInvoke.cvNamedWindow(win1);
			CvInvoke.cvShowImage(win1, ret.Ptr);
			return null; 
		}

		public static byte[] ColorImageToBytes(Emgu.CV.Image<Bgr, Byte> img)
		{

			

			if (img != null)
			{
				byte[, ,] oData = img.Data;
				int hgt = img.Height;
				int wdt = img.Width;
				byte[] data = new byte[wdt * hgt * 4];
				int ptr = 0;

				

				for (int h = 0; h <hgt; h++)
				{
					for (int w = 0; w < wdt; w++)
					{
						data[ptr] = (byte)(oData[h, w, 0]);
						data[ptr + 1] = (byte)(oData[h, w, 1]);
						data[ptr + 2] = (byte)(oData[h, w, 2]);
						data[ptr + 3] = 0;
						ptr += 4;
					}
				}
				return data;
			}
			else
			{
				byte[] data = new byte[640 * 480 * 4];
				int ptr = 0;
				for (int h = 0; h < 480; h++)
				{
					for (int w = 0; w < 640; w++)
					{
						data[ptr] = (byte)(0);
						data[ptr + 1] = (byte)(0);
						data[ptr + 2] = (byte)(0);
						data[ptr + 3] = 0;
						ptr += 4;
					}
				}
				return data;
			}
		}

		//May need optimization
		public static Emgu.CV.Image<Bgr, byte> BytesToColorImage(byte[] bts, int width, int height, int channels)
		{
			if(bts.Length!=width*height*channels){
				MessageBox.Show("Invalid img parameters");
				MessageBox.Show((width * height * channels).ToString());
				MessageBox.Show(bts.Length.ToString());
				return null;
			}else{
				byte[,,] data=new byte[height,width,3];

				int ptr = 0;
				for(int h=0;h<height;h++){
					for(int w=0;w<width;w++){
						for(int c=0;c<channels;c++){
							if(c<=2)
								data[h,w,c]=(byte)(bts[ptr]);
							ptr++;
						}
					}
				}

				Emgu.CV.Image<Bgr, byte> image = new Emgu.CV.Image<Bgr, byte>(data);
				return image;
			}
			
		}


		public static byte[] DepthImageToBytes(Emgu.CV.Image<Gray, Byte> img)
		{
			if (img != null&&img.Data!=null)
			{
				byte[] data = new byte[img.Width * img.Height * 1];
				int ptr = 0;
				for (int h = 0; h < img.Height; h++)
				{
					for (int w = 0; w < img.Width; w++)
					{
						data[ptr] = (byte)(img.Data[h, w, 0]);
						ptr += 1;
					}
				}
				return data;
			}
			else
			{
				byte[] data = new byte[640 * 480 * 1];
				int ptr = 0;
				for (int h = 0; h < 480; h++)
				{
					for (int w = 0; w < 640; w++)
					{
						data[ptr] = (byte)(0);
						ptr += 1;
					}
				}
				return data;
			}
		}

		public static Emgu.CV.Image<Gray, Byte> ShortsToGrayImage(short[] bts, int width, int height, int channels)
		{
			if (bts.Length != width * height * channels)
			{
				MessageBox.Show("Invalid img parameters");
				MessageBox.Show((width * height * channels).ToString());
				MessageBox.Show(bts.Length.ToString());
				return null;
			}
			else
			{
				byte[, ,] data = new byte[height, width, 1];

				int ptr = 0;
				for (int h = 0; h < height; h++)
				{
					for (int w = 0; w < width; w++)
					{
						for (int c = 0; c < channels; c++)
						{
							if (c <= 2)
								data[h, w, c] = (byte)(bts[ptr]>>8);
							ptr++;
						}
					}
				}

				Emgu.CV.Image<Gray, Byte> image = new Emgu.CV.Image<Gray, byte>(data);
				return image;
			}

		}

	}
}

﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeadTracking
{
	class TMDepthTracker
	{
		Point Location;
		Point smallLocation;
		Point bigLocation;
		public Image<Gray, double> Results { get; set; }
		public Image<Gray, double> smallResults { get; set; }
		public Image<Gray, double> bigResults { get; set; }

		Image<Gray, Byte> smallTrackedObject;
		Image<Gray, Byte> trackedObject;
		Image<Gray, Byte> bigTrackedObject;
		public bool Initialized;
		//int marg = 50;
		bool busy = false;
		double result;
		double smallResult;
		double bigResult;

		public TMDepthTracker(HeadTracker ht)
		{
			Initialized = false;
		}

		public void PassInitialFrame(Image<Gray, Byte> modelImage, List<HeadModel> models)
		{

			modelImage = modelImage.Erode(3);
			Initialized = true;
			foreach (HeadModel model in models)
			{
				if (model.Probability == 1.0)
				{
					
					Rectangle roi = model.DepthBoundingRect;
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					trackedObject = modelImage.GetSubRect(roi);
					Location = roi.Location;

					roi = model.DepthBoundingRect;
					roi = new Rectangle(roi.X + roi.Width / 4, roi.Y + roi.Height / 4, roi.Width / 2, roi.Height / 2);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					smallTrackedObject = modelImage.GetSubRect(roi);
					smallLocation = roi.Location;

					roi = model.DepthBoundingRect;
					roi = new Rectangle(roi.X - roi.Width / 4, roi.Y - roi.Height / 4,(int)( roi.Width*1.5),(int)( roi.Height*1.5));
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					bigTrackedObject = modelImage.GetSubRect(roi);
					bigLocation = roi.Location;

					break;
				}
			}
		}

		public void Update(Image<Gray, Byte> modelImage,List<HeadModel> models)
		{
			return;
			bool sure = false;
			foreach (HeadModel model in models)
			{
				if (model.Probability == 1.0)
				{
					Rectangle roi = model.DepthBoundingRect;
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					trackedObject = modelImage.GetSubRect(roi);
					Location = roi.Location;

					roi = model.DepthBoundingRect;
					roi = new Rectangle(roi.X + roi.Width / 4, roi.Y + roi.Height / 4, roi.Width / 2, roi.Height / 2);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					smallTrackedObject = modelImage.GetSubRect(roi);
					smallLocation = roi.Location;

					roi = model.DepthBoundingRect;
					roi = new Rectangle(roi.X - roi.Width / 4, roi.Y - roi.Height / 4, (int)(roi.Width * 1.5), (int)(roi.Height * 1.5));
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					bigTrackedObject = modelImage.GetSubRect(roi);
					bigLocation = roi.Location;

					sure = true;
					break;
				}
			}

			//Compare if tracked head matches one of detected heads
			if (!sure)
			{
				//Find the best model to track
				HeadModel model = null;
				double bestProb=0;
				foreach (HeadModel hm in models)
				{
					Point colorCenter = new Point(hm.DepthBoundingRect.X + hm.DepthBoundingRect.Width / 2, hm.DepthBoundingRect.Y + hm.DepthBoundingRect.Height / 2);
					if (colorCenter.X > smallLocation.X && colorCenter.X < smallLocation.X + smallTrackedObject.Width && colorCenter.Y > smallLocation.Y && colorCenter.Y < smallLocation.Y + smallTrackedObject.Height)
					{
						if (hm.Probability > bestProb)
						{
							model = hm;
							bestProb = hm.Probability;
						}
					}
					if (colorCenter.X > Location.X && colorCenter.X < Location.X + trackedObject.Width && colorCenter.Y > Location.Y && colorCenter.Y < Location.Y+trackedObject.Height)
					{
						model = hm;
						bestProb = hm.Probability;
					}
				/*	if (colorCenter.X > bigLocation.X && colorCenter.X < bigLocation.X + bigTrackedObject.Width && colorCenter.Y > bigLocation.Y && colorCenter.Y < bigLocation.Y+bigTrackedObject.Height)
					{
						model = hm;
						bestProb = hm.Probability;
					}*/
				}
				if (model != null)
				{
					Rectangle roi = model.DepthBoundingRect;
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					trackedObject = modelImage.GetSubRect(roi);
					Location = roi.Location;

					roi = model.DepthBoundingRect;
					roi = new Rectangle(roi.X + roi.Width / 4, roi.Y + roi.Height / 4, roi.Width / 2, roi.Height / 2);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					smallTrackedObject = modelImage.GetSubRect(roi);
					smallLocation = roi.Location;

					roi = model.DepthBoundingRect;
					roi = new Rectangle(roi.X - roi.Width / 4, roi.Y - roi.Height / 4, (int)(roi.Width * 1.5), (int)(roi.Height * 1.5));
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					bigTrackedObject = modelImage.GetSubRect(roi);
					bigLocation = roi.Location;
				}
			}
		}

		public void Track(Image<Gray, Byte> targetImage)
		{
			targetImage = targetImage.Erode(3);
			int margL = 100;
			int margT = 100;
			int margB = 100;
			int margR = 100;
			//marg = 50;
			if (!busy)
			{
				busy = true;
				Image<Gray, Byte> targetImageCopy = targetImage.Copy();
				Point oldLocation = Location;
				margL = Math.Min(margL, Location.X);
				margT = Math.Min(margT, Location.Y);
				margR = Math.Min(margR, 639 - (Location.X + trackedObject.Width));
				margB = Math.Min(margB, 479 - (Location.Y + trackedObject.Height));
				
				int minX = Location.X-margL;
				int maxX = Location.X + trackedObject.Width + margR;
				int minY = Location.Y - margT;
				int maxY = Location.Y + trackedObject.Height + margB;

				Image<Gray, Byte> area = targetImage.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));
				
				if (DetectObject(area, trackedObject, 0))
				{
					Location.X += oldLocation.X - margL;
					Location.Y += oldLocation.Y - margT;
					Point[] pts = { new Point(Location.X, Location.Y), new Point(Location.X + trackedObject.Width, Location.Y), new Point(Location.X + trackedObject.Width, Location.Y + trackedObject.Height), new Point(Location.X, Location.Y + trackedObject.Height) };
					targetImageCopy.DrawPolyline(pts, true, new Gray(255 * (result - 0.5) * 2), 3);

					minX = Math.Min(Math.Max(Location.X, 1), 638);
					minY = Math.Min(Math.Max(Location.Y, 1), 478);
					maxX = Math.Max(2, Math.Min(Location.X + trackedObject.Width, 639));
					maxY = Math.Max(2, Math.Min(Location.Y + trackedObject.Height, 479));

					trackedObject = targetImage.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));
					
				//	trackedObject = targetImage.GetSubRect(new Rectangle(Location.X, Location.Y, trackedObject.Width, trackedObject.Height)).Convert<Gray,Byte>();
				}

				oldLocation = smallLocation;
				margL = Math.Min(margL, smallLocation.X);
				margT = Math.Min(margT, smallLocation.Y);
				margR = Math.Min(margR, 639 - (smallLocation.X + smallTrackedObject.Width));
				margB = Math.Min(margB, 479 - (smallLocation.Y + smallTrackedObject.Height));

				minX = smallLocation.X - margL;
				maxX = smallLocation.X + smallTrackedObject.Width + margR;
				minY = smallLocation.Y - margT;
				maxY = smallLocation.Y + smallTrackedObject.Height + margB;

				area = targetImage.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));
				if (DetectObject(area, smallTrackedObject, 1))
				{
					smallLocation.X += oldLocation.X - margL;
					smallLocation.Y += oldLocation.Y - margT;
					Point[] pts = { new Point(smallLocation.X, smallLocation.Y), new Point(smallLocation.X + smallTrackedObject.Width, smallLocation.Y), new Point(smallLocation.X + smallTrackedObject.Width, smallLocation.Y + smallTrackedObject.Height), new Point(smallLocation.X, smallLocation.Y + smallTrackedObject.Height) };
					targetImageCopy.DrawPolyline(pts, true, new Gray(255 * (smallResult - 0.5) * 2), 3);

					minX = Math.Min(Math.Max(smallLocation.X, 1), 638);
					minY = Math.Min(Math.Max(smallLocation.Y, 1), 478);
					maxX = Math.Max(2, Math.Min(smallLocation.X + smallTrackedObject.Width, 639));
					maxY = Math.Max(2, Math.Min(smallLocation.Y + smallTrackedObject.Height, 479));

					smallTrackedObject = targetImage.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));
					
				//	smallTrackedObject = targetImage.GetSubRect(new Rectangle(smallLocation.X, smallLocation.Y, smallTrackedObject.Width, smallTrackedObject.Height)).Convert<Gray, Byte>();
				}

				oldLocation = bigLocation;
				margL = Math.Min(margL, bigLocation.X);
				margT = Math.Min(margT, bigLocation.Y);
				margR = Math.Min(margR, 639 - (bigLocation.X + bigTrackedObject.Width));
				margB = Math.Min(margB, 479 - (bigLocation.Y + bigTrackedObject.Height));

				minX = bigLocation.X - margL;
				maxX = bigLocation.X + bigTrackedObject.Width + margR;
				minY = bigLocation.Y - margT;
				maxY = bigLocation.Y + bigTrackedObject.Height + margB;

				area = targetImage.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));
				if (DetectObject(area, bigTrackedObject, 2))
				{
					bigLocation.X += oldLocation.X - margL;
					bigLocation.Y += oldLocation.Y - margT;
					Point[] pts = { new Point(bigLocation.X, bigLocation.Y), new Point(bigLocation.X + bigTrackedObject.Width, bigLocation.Y), new Point(bigLocation.X + bigTrackedObject.Width, bigLocation.Y + bigTrackedObject.Height), new Point(bigLocation.X, bigLocation.Y + bigTrackedObject.Height) };
					targetImageCopy.DrawPolyline(pts, true, new Gray(255 * (bigResult - 0.5) * 2), 3);

					minX = Math.Min(Math.Max(bigLocation.X, 1), 638);
					minY = Math.Min(Math.Max(bigLocation.Y, 1), 478);
					maxX = Math.Max(2, Math.Min(bigLocation.X + bigTrackedObject.Width, 639));
					maxY = Math.Max(2, Math.Min(bigLocation.Y + bigTrackedObject.Height, 479));

					bigTrackedObject = targetImage.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));


					//bigTrackedObject = targetImage.GetSubRect(new Rectangle(bigLocation.X, bigLocation.Y, bigTrackedObject.Width, bigTrackedObject.Height)).Convert<Gray, Byte>();
				}


		//		CenterByBest(targetImage);


				String win1 = "Debug WindowG";
				CvInvoke.cvNamedWindow(win1);

				CvInvoke.cvShowImage(win1, targetImageCopy.Ptr);
				busy = false;
			}
		}

		private void CenterByBest(Image<Gray, Byte> target)
		{
			//Small is the best
			if (smallResult >= result)
			{
				bigLocation = new Point(smallLocation.X + smallTrackedObject.Width / 2 - bigTrackedObject.Width/2, smallLocation.Y + smallTrackedObject.Height / 2 - bigTrackedObject.Height/2);
				Location = new Point(smallLocation.X + smallTrackedObject.Width / 2 - trackedObject.Width/2, smallLocation.Y + smallTrackedObject.Height / 2 - trackedObject.Height/2);
				bigTrackedObject = target.GetSubRect(new Rectangle(bigLocation.X, bigLocation.Y, bigTrackedObject.Width, bigTrackedObject.Height));
				trackedObject = target.GetSubRect(new Rectangle(Location.X, Location.Y, trackedObject.Width, trackedObject.Height));
			}
			else
			{
				//Result is the best
				if (result >= smallResult)
				{
					bigLocation = new Point(Location.X + trackedObject.Width / 2 - bigTrackedObject.Width/2, Location.Y + trackedObject.Height / 2 - bigTrackedObject.Height/2);
					smallLocation = new Point(Location.X + trackedObject.Width / 2 - smallTrackedObject.Width/2, Location.Y + trackedObject.Height / 2 - smallTrackedObject.Height/2);
					bigTrackedObject = target.GetSubRect(new Rectangle(bigLocation.X, bigLocation.Y, bigTrackedObject.Width, bigTrackedObject.Height));
					smallTrackedObject = target.GetSubRect(new Rectangle(smallLocation.X, smallLocation.Y, smallTrackedObject.Width, smallTrackedObject.Height));
				}

/*				else
				{
					//Big is the best
					smallLocation = new Point(bigLocation.X + bigTrackedObject.Width / 2 - smallTrackedObject.Width/2, bigLocation.Y + bigTrackedObject.Height / 2 - smallTrackedObject.Height/2);
					Location = new Point(bigLocation.X + bigTrackedObject.Width / 2 - trackedObject.Width/2, bigLocation.Y + bigTrackedObject.Height / 2 - trackedObject.Height/2);
					smallTrackedObject = target.GetSubRect(new Rectangle(smallLocation.X, smallLocation.Y, smallTrackedObject.Width, smallTrackedObject.Height)).Convert<Gray, Byte>();
					trackedObject = target.GetSubRect(new Rectangle(Location.X, Location.Y, trackedObject.Width, trackedObject.Height)).Convert<Gray, Byte>();
				}*/
			}
		}
		

		private bool DetectObject(Image<Gray, Byte> Area_Image, Image<Gray, Byte> image_object,int variant)
		{
			bool success = false;

			//Work out padding array size
			Point dftSize = new Point(Area_Image.Width + (image_object.Width * 2), Area_Image.Height + (image_object.Height * 2));
			//Pad the Array with zeros
			using (Image<Gray, Byte> pad_array = new Image<Gray, Byte>(dftSize.X, dftSize.Y))
			{
				//copy centre
				pad_array.ROI = new Rectangle(image_object.Width, image_object.Height, Area_Image.Width, Area_Image.Height);
				CvInvoke.cvCopy(Area_Image, pad_array, IntPtr.Zero);

				pad_array.ROI = (new Rectangle(0, 0, dftSize.X, dftSize.Y));

				//Match Template
				using (Image<Gray, float> result_Matrix = pad_array.MatchTemplate(image_object, TM_TYPE.CV_TM_CCOEFF_NORMED))
				{
					/*String win1 = "Debug Window3";
					CvInvoke.cvNamedWindow(win1);

					CvInvoke.cvShowImage(win1, result_Matrix.Ptr);
					*/
					Point[] MAX_Loc, Min_Loc;
					double[] min, max;
					//Limit ROI to look for Match

					if (Area_Image.Width - image_object.Width <= 0 || Area_Image.Height - image_object.Height<=0)
					{
						return false;
					}

					result_Matrix.ROI = new Rectangle(image_object.Width, image_object.Height, Area_Image.Width - image_object.Width, Area_Image.Height - image_object.Height);

					result_Matrix.MinMax(out min, out max, out Min_Loc, out MAX_Loc);

					//if (max[0] > 0.5)
					//{
						success = true;
						if (variant == 0)
						{
							Location = new Point((MAX_Loc[0].X), (MAX_Loc[0].Y));
							Results = result_Matrix.Convert<Gray, Double>();
							result = max[0];
						}
						if (variant == 1)
						{
							smallLocation = new Point((MAX_Loc[0].X), (MAX_Loc[0].Y));
							smallResults = result_Matrix.Convert<Gray, Double>();
							smallResult = max[0];
						}
						if (variant == 2)
						{
							bigLocation = new Point((MAX_Loc[0].X), (MAX_Loc[0].Y));
							bigResults = result_Matrix.Convert<Gray, Double>();
							bigResult = max[0];
						}

					//}
				}
			}
			return success;
		}

		
	}
}

﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeadTracking
{
	class TMMaskedTracker
	{
		Point Location;
		Point smallLocation;
//		Point bigLocation;
		public Image<Gray, double> Results { get; set; }
		public Image<Gray, double> smallResults { get; set; }
//		public Image<Gray, double> bigResults { get; set; }

		Image<Bgr, Byte> smallTrackedObject;
		Image<Bgr, Byte> trackedObject;
//		Image<Bgr, Byte> bigTrackedObject;
		public bool Initialized;
		//int marg = 50;
		bool busy = false;
		double result;
		double smallResult;
//		double bigResult;

		Point smallMove;
		Point move;
//		Point bigMove;

		bool lost = false;
		bool smallLost = false;

		Image<Bgr, Byte> targetImage;

		internal TMImprovedDepthTracker IdTracker
		{
			get;
			set;
		}

		public TMMaskedTracker(HeadTracker ht)
		{
			Initialized = false;

		}

		

		public void PassInitialFrame(Image<Bgr, Byte> modelImage, List<HeadModel> models)
		{
			Initialized = true;
			smallMove = new Point(0,0);
			move = new Point(0,0);
//			bigMove = new Point(0,0);
		//	modelImage = modelImage.Dilate(3);

			modelImage=modelImage.And(IdTracker.GetDepthMask().Convert<Bgr,Byte>());

			foreach (HeadModel model in models)
			{
				if (model.Probability == 1.0)
				{
					
					Rectangle roi = model.ColorBoundingRect;
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					trackedObject = modelImage.GetSubRect(roi);
					Location = roi.Location;

					roi = model.ColorBoundingRect;
					roi = new Rectangle(roi.X + roi.Width / 4, roi.Y + roi.Height / 4, roi.Width / 2, roi.Height / 2);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					smallTrackedObject = modelImage.GetSubRect(roi);
					smallLocation = roi.Location;

/*					roi = model.ColorBoundingRect;
					//roi = new Rectangle(roi.X - roi.Width / 4, roi.Y - roi.Height / 4,(int)( roi.Width*1.5),(int)( roi.Height*1.5));
					roi = new Rectangle(roi.X + (int)(roi.Width * (3.0 / 8.0)), roi.Y + (int)(roi.Height * (3.0 / 8.0)), roi.Width / 4, roi.Height / 4);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					bigTrackedObject = modelImage.GetSubRect(roi);
					bigLocation = roi.Location;
*/
					break;
				}
			}
		}

		public void Update(Image<Bgr, Byte> modelImage,Image<Bgr, Byte> depthMask,List<HeadModel> models)
		{
		//	modelImage = modelImage.Dilate(3);
			//return;

			modelImage = modelImage.And(depthMask);
			
			bool sure = false;
			foreach (HeadModel model in models)
			{
				if (model.Probability == 1.0)
				{
					Rectangle roi = model.ColorBoundingRect;
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					Image<Bgr, Byte> tImg = modelImage.GetSubRect(roi);
					if(!isDark(tImg)){
						trackedObject = tImg;
						Location = roi.Location;
					}

					roi = model.ColorBoundingRect;
					roi = new Rectangle(roi.X + roi.Width / 4, roi.Y + roi.Height / 4, roi.Width / 2, roi.Height / 2);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					tImg = modelImage.GetSubRect(roi);

					if(!isDark(tImg)){
						smallTrackedObject = tImg;
						smallLocation = roi.Location;
					}

/*					roi = model.ColorBoundingRect;
					//roi = new Rectangle(roi.X - roi.Width / 4, roi.Y - roi.Height / 4, (int)(roi.Width * 1.5), (int)(roi.Height * 1.5));
					roi = new Rectangle(roi.X + (int)(roi.Width * (3.0 / 8.0)), roi.Y + (int)(roi.Height * (3.0 / 8.0)), roi.Width / 4, roi.Height / 4);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}

					tImg = modelImage.GetSubRect(roi);
					if(!isDark(tImg)){
						bigTrackedObject = tImg;
						bigLocation = roi.Location;
					}
*/					sure = true;
					break;
				}
			}

			//Compare if tracked head matches one of detected heads
			if (!sure)
			{
				//Find the best model to track
				HeadModel model = null;
				double bestProb=0;
				foreach (HeadModel hm in models)
				{
					Point colorCenter = new Point(hm.ColorBoundingRect.X + hm.ColorBoundingRect.Width / 2, hm.ColorBoundingRect.Y + hm.ColorBoundingRect.Height / 2);
					if (colorCenter.X > smallLocation.X && colorCenter.X < smallLocation.X + smallTrackedObject.Width && colorCenter.Y > smallLocation.Y && colorCenter.Y < smallLocation.Y + smallTrackedObject.Height)
					{
						if (hm.Probability > bestProb)
						{
							model = hm;
							bestProb = hm.Probability;
						}
					}
					if (colorCenter.X > Location.X && colorCenter.X < Location.X + trackedObject.Width && colorCenter.Y > Location.Y && colorCenter.Y < Location.Y+trackedObject.Height)
					{
						model = hm;
						bestProb = hm.Probability;
					}
				
				}
				if (model != null)
				{
					Rectangle roi = model.ColorBoundingRect;
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					Image<Bgr,Byte> tImg = modelImage.GetSubRect(roi);
					if (!isDark(tImg))
					{
						trackedObject = tImg;
						Location = roi.Location;
					}

					roi = model.ColorBoundingRect;
					roi = new Rectangle(roi.X + roi.Width / 4, roi.Y + roi.Height / 4, roi.Width / 2, roi.Height / 2);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					tImg = modelImage.GetSubRect(roi);
					if (!isDark(tImg))
					{
						smallTrackedObject = tImg;
						smallLocation = roi.Location;
					}

				}
			}
		}

		public bool isDark(Image<Bgr, Byte> img)
		{
			return (double)(img.CountNonzero()[0]) / (img.Width * img.Height) < 0.5;
		}

		public bool isHope(Image<Bgr, Byte> img)
		{
			//return !isDark(img);
			return (double)(img.CountNonzero()[0]) / (img.Width * img.Height) >0.0;
		}

		public int countDark(Image<Bgr, Byte> img)
		{
			return img.Width * img.Height - img.CountNonzero()[0];
		}

		public Point GetOutOfThatBlackArea(Image<Bgr, Byte> img)
		{
			Point correct = new Point(0, 0);
			Image<Bgr, Byte> lt = img.GetSubRect(new Rectangle(0, 0, img.Width / 2, img.Height / 2));
			Image<Bgr, Byte> rt = img.GetSubRect(new Rectangle(img.Width / 2, 0, img.Width / 2, img.Height / 2));
			Image<Bgr, Byte> lb = img.GetSubRect(new Rectangle(0, img.Height / 2, img.Width / 2, img.Height / 2));
			Image<Bgr, Byte> rb = img.GetSubRect(new Rectangle(img.Width / 2, img.Height / 2, img.Width / 2, img.Height / 2));

			int additionX = img.Width / 2;
			int additionY = img.Height / 2;
			

			bool rtd = isDark(rt);
			bool ltd = isDark(lt);
			bool rbd = isDark(rb);
			bool lbd = isDark(lb);

			bool rth = isHope(rt);
			bool lth = isHope(lt);
			bool rbh = isHope(rb);
			bool lbh = isHope(lb);

			//Now there is no hope...
			if (!(rth || lth || rbh || lbh))
			{
				return new Point(-640, -480);
			}
			
			//Top is dark
			if (ltd && rtd&&(lbh||rbh))
				correct.Y += additionY;
			//Left is dark
			if (ltd&&lbd&&(rth||rbh))
				correct.X += additionX;
			//Bot is dark
			if (lbd&&rbd&&(rth||rth))
				correct.Y -= additionY;
			//Right is dark
			if (rtd&&rbd&&(lth||lbh))
				correct.X -= additionX;


			return correct;

		}

		public void TrackNormal()
		{
			int margL = 50;
			int margT = 50;
			int margB = 50;
			int margR = 50;


			Point oldLocation = Location;
			margL = Math.Min(margL, Location.X);
			margT = Math.Min(margT, Location.Y);
			margR = Math.Min(margR, 639 - (Location.X + trackedObject.Width));
			margB = Math.Min(margB, 479 - (Location.Y + trackedObject.Height));

			int minX = Location.X - margL;
			int maxX = Location.X + trackedObject.Width + margR;
			int minY = Location.Y - margT;
			int maxY = Location.Y + trackedObject.Height + margB;

			lost = false;


			Image<Bgr, Byte> area = targetImage.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));

			if (DetectObject(area, trackedObject, 0))
			{

				Location.X += oldLocation.X - margL;
				Location.Y += oldLocation.Y - margT;

				move.X = Location.X - oldLocation.X;
				move.Y = Location.Y - oldLocation.Y;
				Point[] pts = { new Point(Location.X, Location.Y), new Point(Location.X + trackedObject.Width, Location.Y), new Point(Location.X + trackedObject.Width, Location.Y + trackedObject.Height), new Point(Location.X, Location.Y + trackedObject.Height) };
				//targetImageCopy.DrawPolyline(pts, true, new Bgr(0, (int)(255 * (result - 0.5) * 2), 0), 3);

				minX = Math.Min(Math.Max(Location.X, 1), 638);
				minY = Math.Min(Math.Max(Location.Y, 1), 478);
				maxX = Math.Max(2, Math.Min(Location.X + trackedObject.Width, 639));
				maxY = Math.Max(2, Math.Min(Location.Y + trackedObject.Height, 479));

				trackedObject = targetImage.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));

				Point correct = GetOutOfThatBlackArea(trackedObject);
				if (correct.X != 0 || correct.Y != 0)
				{

					if (correct.X == -640 && correct.Y == -480)
					{
						lost = true;
						Location = oldLocation;
					}
					else
					{

						Location.X += correct.X;
						Location.Y += correct.Y;

						move.X = Location.X - oldLocation.X;
						move.Y = Location.Y - oldLocation.Y;
						Point[] pts2 = { new Point(Location.X, Location.Y), new Point(Location.X + trackedObject.Width, Location.Y), new Point(Location.X + trackedObject.Width, Location.Y + trackedObject.Height), new Point(Location.X, Location.Y + trackedObject.Height) };
						//targetImageCopy.DrawPolyline(pts2, true, new Bgr(0, (int)(255 * (result - 0.5) * 2), 0), 3);

						minX = Math.Min(Math.Max(Location.X, 1), 638);
						minY = Math.Min(Math.Max(Location.Y, 1), 478);
						maxX = Math.Max(2, Math.Min(Location.X + trackedObject.Width, 639));
						maxY = Math.Max(2, Math.Min(Location.Y + trackedObject.Height, 479));

						trackedObject = targetImage.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));
					}
				}

			}
		}

		public void TrackSmall()
		{
			int margL = 50;
			int margT = 50;
			int margB = 50;
			int margR = 50;

			smallLost = false;

			Point oldLocation = smallLocation;
			margL = Math.Min(margL, smallLocation.X);
			margT = Math.Min(margT, smallLocation.Y);
			margR = Math.Min(margR, 639 - (smallLocation.X + smallTrackedObject.Width));
			margB = Math.Min(margB, 479 - (smallLocation.Y + smallTrackedObject.Height));

			int minX = smallLocation.X - margL;
			int maxX = smallLocation.X + smallTrackedObject.Width + margR;
			int minY = smallLocation.Y - margT;
			int maxY = smallLocation.Y + smallTrackedObject.Height + margB;

			Image<Bgr,Byte> area = targetImage.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));
			if (DetectObject(area, smallTrackedObject, 1))
			{

				smallLocation.X += oldLocation.X - margL;
				smallLocation.Y += oldLocation.Y - margT;

				smallMove.X = smallLocation.X - oldLocation.X;
				smallMove.Y = smallLocation.Y - oldLocation.Y;
				Point[] pts = { new Point(smallLocation.X, smallLocation.Y), new Point(smallLocation.X + smallTrackedObject.Width, smallLocation.Y), new Point(smallLocation.X + smallTrackedObject.Width, smallLocation.Y + smallTrackedObject.Height), new Point(smallLocation.X, smallLocation.Y + smallTrackedObject.Height) };
				//targetImageCopy.DrawPolyline(pts, true, new Bgr(0, 0, (int)(255 * (smallResult - 0.5) * 2)), 3);

				minX = Math.Min(Math.Max(smallLocation.X, 1), 638);
				minY = Math.Min(Math.Max(smallLocation.Y, 1), 478);
				maxX = Math.Max(2, Math.Min(smallLocation.X + smallTrackedObject.Width, 639));
				maxY = Math.Max(2, Math.Min(smallLocation.Y + smallTrackedObject.Height, 479));


				smallTrackedObject = targetImage.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));
				Point correct = GetOutOfThatBlackArea(smallTrackedObject);
				if (correct.X != 0 || correct.Y != 0)
				{
					if (correct.X == -640 && correct.Y == -480)
					{
						smallLost = true;
						smallLocation = oldLocation;
					}
					else
					{
						smallLocation.X += correct.X;
						smallLocation.Y += correct.Y;

						smallMove.X = smallLocation.X - oldLocation.X;
						smallMove.Y = smallLocation.Y - oldLocation.Y;
						Point[] pts2 = { new Point(smallLocation.X, smallLocation.Y), new Point(smallLocation.X + smallTrackedObject.Width, smallLocation.Y), new Point(smallLocation.X + smallTrackedObject.Width, smallLocation.Y + smallTrackedObject.Height), new Point(smallLocation.X, smallLocation.Y + smallTrackedObject.Height) };
						//targetImageCopy.DrawPolyline(pts2, true, new Bgr(0, 0, (int)(255 * (smallResult - 0.5) * 2)), 3);

						minX = Math.Min(Math.Max(smallLocation.X, 1), 638);
						minY = Math.Min(Math.Max(smallLocation.Y, 1), 478);
						maxX = Math.Max(2, Math.Min(smallLocation.X + smallTrackedObject.Width, 639));
						maxY = Math.Max(2, Math.Min(smallLocation.Y + smallTrackedObject.Height, 479));


						smallTrackedObject = targetImage.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));
					}
				}
			}
		}


		public void SendTargetImage(Image<Bgr, Byte> targetImage)
		{
			if (!busy)
			{
				this.targetImage = targetImage;
			}
		}

		public List<HeadModel> GetHeads()
		{
			List<HeadModel> heads = new List<HeadModel>();
			HeadModel h1 = new HeadModel(Location.X + trackedObject.Width / 2, Location.Y + trackedObject.Height / 2, 0, trackedObject.Width / 2);
			h1.Probability = result;
			HeadModel h2 = new HeadModel(smallLocation.X + smallTrackedObject.Width / 2, smallLocation.Y + smallTrackedObject.Height / 2, 0, smallTrackedObject.Width / 2);
			h2.Probability = smallResult;
			heads.Add(h1);
			heads.Add(h2);
			return heads;
		}

		public void Track()
		{
			if (!busy)
			{
				busy = true;
				targetImage  = targetImage.And(IdTracker.GetDepthMask().Convert<Bgr, Byte>());

				Thread normal = new Thread(new ThreadStart(TrackNormal));
				Thread small = new Thread(new ThreadStart(TrackSmall));

				normal.Start();
				small.Start();
				normal.Join();
				small.Join();

				if (lost && !smallLost)
				{
					Location = new Point(smallLocation.X + smallTrackedObject.Width / 2 - trackedObject.Width / 2, smallLocation.Y + smallTrackedObject.Height / 2 - trackedObject.Height / 2);
					if (Location.X > 0 && Location.Y > 0 && trackedObject.Width > 0 && trackedObject.Height > 0)
						trackedObject = targetImage.GetSubRect(new Rectangle(Location.X, Location.Y, trackedObject.Width, trackedObject.Height));
				}
				if (smallLost && !lost)
				{
					smallLocation = new Point(Location.X + trackedObject.Width / 2 - smallTrackedObject.Width / 2, Location.Y + trackedObject.Height / 2 - smallTrackedObject.Height / 2);
					if(smallLocation.X>0&&smallLocation.Y>0&&smallTrackedObject.Width>0&&smallTrackedObject.Height>0)
						smallTrackedObject = targetImage.GetSubRect(new Rectangle(smallLocation.X, smallLocation.Y, smallTrackedObject.Width, smallTrackedObject.Height));
				}

				busy = false;
			}
		}
		

		private bool DetectObject(Image<Bgr, Byte> Area_Image, Image<Bgr, Byte> image_object,int variant)
		{
			bool success = false;

			//Work out padding array size
			Point dftSize = new Point(Area_Image.Width + (image_object.Width * 2), Area_Image.Height + (image_object.Height * 2));
			//Pad the Array with zeros
			using (Image<Bgr, Byte> pad_array = new Image<Bgr, Byte>(dftSize.X, dftSize.Y))
			{
				//copy centre
				pad_array.ROI = new Rectangle(image_object.Width, image_object.Height, Area_Image.Width, Area_Image.Height);
				CvInvoke.cvCopy(Area_Image, pad_array, IntPtr.Zero);

				pad_array.ROI = (new Rectangle(0, 0, dftSize.X, dftSize.Y));

				//Match Template
				using (Image<Gray, float> result_Matrix = pad_array.MatchTemplate(image_object, TM_TYPE.CV_TM_CCOEFF_NORMED))
				{
					
					Point[] MAX_Loc, Min_Loc;
					double[] min, max;
					//Limit ROI to look for Match

					if (Area_Image.Width - image_object.Width <= 0 || Area_Image.Height - image_object.Height<=0)
					{
						return false;
					}

					result_Matrix.ROI = new Rectangle(image_object.Width, image_object.Height, Area_Image.Width - image_object.Width, Area_Image.Height - image_object.Height);

					result_Matrix.MinMax(out min, out max, out Min_Loc, out MAX_Loc);

					//if (max[0] > 0.5)
					//{
						success = true;
						if (variant == 0)
						{
							Location = new Point((MAX_Loc[0].X), (MAX_Loc[0].Y));
							Results = result_Matrix.Convert<Gray, Double>();
							result = max[0];
						}
						if (variant == 1)
						{
							smallLocation = new Point((MAX_Loc[0].X), (MAX_Loc[0].Y));
							smallResults = result_Matrix.Convert<Gray, Double>();
							smallResult = max[0];
						}
				/*		if (variant == 2)
						{
							bigLocation = new Point((MAX_Loc[0].X), (MAX_Loc[0].Y));
							bigResults = result_Matrix.Convert<Gray, Double>();
							bigResult = max[0];
						}*/

					//}
				}
			}
			return success;
		}

		
	}
}

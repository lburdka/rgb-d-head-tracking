﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeadTracking
{
	class TMImprovedDepthTracker
	{
		public Point Location;
//		public Point bigLocation;
		public Image<Gray, double> Results { get; set; }

//		public Image<Gray, double> bigResults { get; set; }
		public double oldMed = 0;

//		public double oldBigMed = 0;


		Image<Gray, Byte> trackedObject;
//		Image<Gray, Byte> bigTrackedObject;
		public bool Initialized;
		
		bool busy = false;
		double result;

//		double bigResult;

//		private Image<Gray, Byte> test2;
		private Image<Gray, Byte> test;
//		private Image<Gray, Byte> test3;

		public Image<Gray, Byte> GetDepthMask()
		{
			if (test == null)
				return new Image<Gray, Byte>(640, 480, new Gray(0));
			return  test.Not().Mul(10);
		}

		public TMImprovedDepthTracker(HeadTracker ht)
		{
			Initialized = false;
		}

		public void PassInitialFrame(Image<Gray, Byte> modelImage, List<HeadModel> models)
		{

			modelImage = modelImage.Erode(6);
			Initialized = true;
			foreach (HeadModel model in models)
			{
				if (model.Probability == 1.0)
				{

					Rectangle roi = model.DepthBoundingRect;
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					trackedObject = modelImage.GetSubRect(roi);
					Location = roi.Location;

					double[] min;
					double[] max;
					Point[] p1;
					Point[] p2;
					trackedObject.MinMax(out min, out max, out p1, out p2);
					double col = min[0];
					trackedObject = trackedObject.Not();
					trackedObject = trackedObject.ThresholdToZero(new Gray(255 - (col + 20)));
					trackedObject = trackedObject.ThresholdToZeroInv(new Gray(255 - (col - 20)));
					trackedObject = trackedObject.Not();
					oldMed = col;

					roi = model.DepthBoundingRect;
					roi = new Rectangle(roi.X + roi.Width / 4, roi.Y + roi.Height / 4, roi.Width / 2, roi.Height / 2);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}


	/*				roi = model.DepthBoundingRect;
					roi = new Rectangle(roi.X - roi.Width / 4, roi.Y - roi.Height / 4, (int)(roi.Width * 1.5), (int)(roi.Height * 1.5));
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}

					//Error here
					bigTrackedObject = modelImage.GetSubRect(roi);
					bigLocation = roi.Location;

					bigTrackedObject.MinMax(out min, out max, out p1, out p2);
					col = min[0];
					bigTrackedObject = bigTrackedObject.Not();
					bigTrackedObject = bigTrackedObject.ThresholdToZero(new Gray(255 - (col + 20)));
					bigTrackedObject = bigTrackedObject.ThresholdToZeroInv(new Gray(255 - (col - 20)));
					bigTrackedObject = bigTrackedObject.Not();
					oldBigMed = col;
					*/
					break;
				}
			}
		}

		public void Update(Image<Gray, Byte> modelImage, List<HeadModel> models)
		{
		//	return;
			modelImage = modelImage.Erode(6);
			bool sure = false;
			foreach (HeadModel model in models)
			{
				if (model.Probability == 1.0)
				{
					Rectangle roi = model.DepthBoundingRect;
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					trackedObject = modelImage.GetSubRect(roi);
					Location = roi.Location;

					double[] min;
					double[] max;
					Point[] p1;
					Point[] p2;
					trackedObject.MinMax(out min, out max, out p1, out p2);
					double col = min[0];
					trackedObject = trackedObject.Not();
					trackedObject = trackedObject.ThresholdToZero(new Gray(255 - (col + 20)));
					trackedObject = trackedObject.ThresholdToZeroInv(new Gray(255 - (col - 20)));
					trackedObject = trackedObject.Not();
					oldMed = col;

					roi = model.DepthBoundingRect;
					roi = new Rectangle(roi.X + roi.Width / 4, roi.Y + roi.Height / 4, roi.Width / 2, roi.Height / 2);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
		
					/*
					roi = model.DepthBoundingRect;
					roi = new Rectangle(roi.X - roi.Width / 4, roi.Y - roi.Height / 4, (int)(roi.Width * 1.5), (int)(roi.Height * 1.5));
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}

					//error here
					bigTrackedObject = modelImage.GetSubRect(roi);
					bigLocation = roi.Location;

					bigTrackedObject.MinMax(out min, out max, out p1, out p2);
					col = min[0];
					bigTrackedObject = bigTrackedObject.Not();
					bigTrackedObject = bigTrackedObject.ThresholdToZero(new Gray(255 - (col + 20)));
					bigTrackedObject = bigTrackedObject.ThresholdToZeroInv(new Gray(255 - (col - 20)));
					bigTrackedObject = bigTrackedObject.Not();
					oldBigMed = col;
					*/
					sure = true;
					break;
				}
			}

			//Compare if tracked head matches one of detected heads
			if (!sure)
			{
				//Find the best model to track
				HeadModel model = null;
				double bestProb = 0;
				foreach (HeadModel hm in models)
				{
					Point colorCenter = new Point(hm.DepthBoundingRect.X + hm.DepthBoundingRect.Width / 2, hm.DepthBoundingRect.Y + hm.DepthBoundingRect.Height / 2);
			
					if (colorCenter.X > Location.X && colorCenter.X < Location.X + trackedObject.Width && colorCenter.Y > Location.Y && colorCenter.Y < Location.Y + trackedObject.Height)
					{
						model = hm;
						bestProb = hm.Probability;
					}
					
				}
				if (model != null)
				{
					Rectangle roi = model.DepthBoundingRect;
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					trackedObject = modelImage.GetSubRect(roi);
					Location = roi.Location;

					double[] min;
					double[] max;
					Point[] p1;
					Point[] p2;
					trackedObject.MinMax(out min, out max, out p1, out p2);
					double col = min[0];
					trackedObject = trackedObject.Not();
					trackedObject = trackedObject.ThresholdToZero(new Gray(255 - (col + 20)));
					trackedObject = trackedObject.ThresholdToZeroInv(new Gray(255 - (col - 20)));
					trackedObject = trackedObject.Not();
					oldMed = col;

					roi = model.DepthBoundingRect;
					roi = new Rectangle(roi.X + roi.Width / 4, roi.Y + roi.Height / 4, roi.Width / 2, roi.Height / 2);
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
				
					/*
					roi = model.DepthBoundingRect;
					roi = new Rectangle(roi.X - roi.Width / 4, roi.Y - roi.Height / 4, (int)(roi.Width * 1.5), (int)(roi.Height * 1.5));
					if (roi.X <= 0)
					{
						roi.X = 1;
					}
					if (roi.Y <= 0)
					{
						roi.Y = 1;
					}
					bigTrackedObject = modelImage.GetSubRect(roi);
					bigLocation = roi.Location;

					bigTrackedObject.MinMax(out min, out max, out p1, out p2);
					col = min[0];
					bigTrackedObject = bigTrackedObject.Not();
					bigTrackedObject = bigTrackedObject.ThresholdToZero(new Gray(255 - (col + 20)));
					bigTrackedObject = bigTrackedObject.ThresholdToZeroInv(new Gray(255 - (col - 20)));
					bigTrackedObject = bigTrackedObject.Not();
					oldBigMed = col;
					*/
				}
			}
		}

		public void Track(Image<Gray, Byte> targetImage)
		{
			if (!busy)
			{
				busy = true;
			double veryImportantConstant1 = 20;
			double veryImportantConstant2 = 20;
		//	double anotherVeryImportantConstant1 = 10;
		//	double anotherVeryImportantConstant2 = 10;
			double oneMoreVeryImportantConstant1 = 20;
			double oneMoreVeryImportantConstant2 = 20;
			//String win = "Debug WindowDD";
			//CvInvoke.cvNamedWindow(win);
			targetImage = targetImage.Erode(6);
			test = targetImage;
		//	test2 = targetImage;
		//	test3 = targetImage;
			List<byte> pixels = new List<byte>();
			double[] min;
			double[] max;
			Point[] p1;
			Point[] p2;
			trackedObject.MinMax(out min, out max, out p1, out p2);
			double col = min[0];
			if (oldMed <= col - veryImportantConstant1)
			{
				veryImportantConstant1 = (col - oldMed) * 1.1;
				oldMed = (col + oldMed) / 2;
			}
			else if (oldMed >= col + veryImportantConstant2)
			{
				veryImportantConstant2 = (oldMed - col) * 1.1;
				oldMed = (col + oldMed) / 2;
			}
			else
			{
				oldMed = col;
			}
			test = test.Not();
			test = test.ThresholdToZero(new Gray(255 - (col + veryImportantConstant2)));
			test = test.ThresholdToZeroInv(new Gray(255 - (col - veryImportantConstant1)));
			test = test.Not();
			
			
			
			int marg = 100;
			int margL = marg;
			int margT = marg;
			int margB = marg;
			int margR = marg;
			//marg = 50;
			
		//		Image<Gray, Byte> targetImageCopy = targetImage.Copy();
		//		Image<Gray, Byte> targetImageCopy = test3.Copy();
				Point oldLocation = Location;
				margL = Math.Min(margL, Location.X);
				margT = Math.Min(margT, Location.Y);
				margR = Math.Min(margR, 639 - (Location.X + trackedObject.Width));
				margB = Math.Min(margB, 479 - (Location.Y + trackedObject.Height));

				int minX = Location.X - margL;
				int maxX = Location.X + trackedObject.Width + margR;
				int minY = Location.Y - margT;
				int maxY = Location.Y + trackedObject.Height + margB;

				Image<Gray, Byte> area = test.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));
				//		CvInvoke.cvShowImage(win, trackedObject.Ptr);
				if (DetectObject(area, trackedObject, 0))
				{
					Location.X += oldLocation.X - margL;
					Location.Y += oldLocation.Y - margT;
					Point[] pts = { new Point(Location.X, Location.Y), new Point(Location.X + trackedObject.Width, Location.Y), new Point(Location.X + trackedObject.Width, Location.Y + trackedObject.Height), new Point(Location.X, Location.Y + trackedObject.Height) };
					//targetImageCopy.DrawPolyline(pts, true, new Gray(255 * (result - 0.5) * 2), 3);

					minX = Math.Min(Math.Max(Location.X, 1), 638);
					minY = Math.Min(Math.Max(Location.Y, 1), 478);
					maxX = Math.Max(2, Math.Min(Location.X + trackedObject.Width, 639));
					maxY = Math.Max(2, Math.Min(Location.Y + trackedObject.Height, 479));

					trackedObject = test.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));
					//		test.DrawPolyline(pts, true, new Gray(255 * (result - 0.5) * 2), 3);
					//	CvInvoke.cvShowImage(win, trackedObject.Ptr);
					//	trackedObject = targetImage.GetSubRect(new Rectangle(Location.X, Location.Y, trackedObject.Width, trackedObject.Height)).Convert<Gray,Byte>();
				}

				/*

				oldLocation = bigLocation;
				margL = Math.Min(margL, bigLocation.X);
				margT = Math.Min(margT, bigLocation.Y);
				margR = Math.Min(margR, 639 - (bigLocation.X + bigTrackedObject.Width));
				margB = Math.Min(margB, 479 - (bigLocation.Y + bigTrackedObject.Height));

				minX = bigLocation.X - margL;
				maxX = bigLocation.X + bigTrackedObject.Width + margR;
				minY = bigLocation.Y - margT;
				maxY = bigLocation.Y + bigTrackedObject.Height + margB;

				area = test3.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));
				if (DetectObject(area, bigTrackedObject, 2))
				{
					bigLocation.X += oldLocation.X - margL;
					bigLocation.Y += oldLocation.Y - margT;
					Point[] pts = { new Point(bigLocation.X, bigLocation.Y), new Point(bigLocation.X + bigTrackedObject.Width, bigLocation.Y), new Point(bigLocation.X + bigTrackedObject.Width, bigLocation.Y + bigTrackedObject.Height), new Point(bigLocation.X, bigLocation.Y + bigTrackedObject.Height) };
					targetImageCopy.DrawPolyline(pts, true, new Gray(255 * (bigResult - 0.5) * 2), 3);

					minX = Math.Min(Math.Max(bigLocation.X, 1), 638);
					minY = Math.Min(Math.Max(bigLocation.Y, 1), 478);
					maxX = Math.Max(2, Math.Min(bigLocation.X + bigTrackedObject.Width, 639));
					maxY = Math.Max(2, Math.Min(bigLocation.Y + bigTrackedObject.Height, 479));

					bigTrackedObject = test3.GetSubRect(new Rectangle(minX, minY, maxX - minX, maxY - minY));


					//bigTrackedObject = targetImage.GetSubRect(new Rectangle(bigLocation.X, bigLocation.Y, bigTrackedObject.Width, bigTrackedObject.Height)).Convert<Gray, Byte>();
				}
				*/

				//		CenterByBest(targetImage);


			
				busy = false;
			}
		}

	


		private bool DetectObject(Image<Gray, Byte> Area_Image, Image<Gray, Byte> image_object, int variant)
		{
			bool success = false;

			//Work out padding array size
			Point dftSize = new Point(Area_Image.Width + (image_object.Width * 2), Area_Image.Height + (image_object.Height * 2));
			//Pad the Array with zeros
			using (Image<Gray, Byte> pad_array = new Image<Gray, Byte>(dftSize.X, dftSize.Y))
			{
				//copy centre
				pad_array.ROI = new Rectangle(image_object.Width, image_object.Height, Area_Image.Width, Area_Image.Height);
				CvInvoke.cvCopy(Area_Image, pad_array, IntPtr.Zero);

				pad_array.ROI = (new Rectangle(0, 0, dftSize.X, dftSize.Y));

				//Match Template
				using (Image<Gray, float> result_Matrix = pad_array.MatchTemplate(image_object, TM_TYPE.CV_TM_CCOEFF_NORMED))
				{
					
					Point[] MAX_Loc, Min_Loc;
					double[] min, max;
					//Limit ROI to look for Match

					if (Area_Image.Width - image_object.Width <= 0 || Area_Image.Height - image_object.Height <= 0)
					{
						return false;
					}

					result_Matrix.ROI = new Rectangle(image_object.Width, image_object.Height, Area_Image.Width - image_object.Width, Area_Image.Height - image_object.Height);

					result_Matrix.MinMax(out min, out max, out Min_Loc, out MAX_Loc);

					//if (max[0] > 0.5)
					//{
					success = true;
					if (variant == 0)
					{
						Location = new Point((MAX_Loc[0].X), (MAX_Loc[0].Y));
						Results = result_Matrix.Convert<Gray, Double>();
						result = max[0];
					}
					/*
					if (variant == 2)
					{
						bigLocation = new Point((MAX_Loc[0].X), (MAX_Loc[0].Y));
						bigResults = result_Matrix.Convert<Gray, Double>();
						bigResult = max[0];
					}*/

					//}
				}
			}
			return success;
		}


	}
}

﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeadTracking
{
	class DepthSilhouetteDetector: HeadDetector
	{

		private HeadTask task;
		private DetectedHead[] heads;
		private int AcceptableSpacing = 500;
		private HeadIntegrator integrator;
		private List<Rectangle> lastDetected;

		public DepthSilhouetteDetector(HeadIntegrator hi)
		{
			integrator = hi;
		}

		public void SendTask(HeadTask task)
		{
			this.task = task;
		}

		public void Reset()
		{
			
		}

		public void Process()
		{
			heads = new DetectedHead[HeadIntegrator.MAX_HEADS];
			Emgu.CV.Image<Gray, Byte> img = ImageConverter.ShortsToGrayImage(task.DepthFrame, 640, 480, 1);
			Emgu.CV.Image<Gray, Byte> origin = img.Copy();
			//Noise reduction from depth image
			img = img.Erode(7);
			//Detect edges
			img = img.Canny(30, 60);

			//Rectangles detected as potential silhouetes
			List<Rectangle> silRects = new List<Rectangle>();

			using (MemStorage storage = new MemStorage())
			{
				for (
					//Link edge points into contours and iterate through them to select those that match criterions
					  Contour<Point> contours = img.FindContours(
						 Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE,
						 Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL,
						 storage);
					  contours != null;
					  contours = contours.HNext)
				{
					Contour<Point> currentContour = contours;

					if (contours.BoundingRectangle.Height > 320 && contours.BoundingRectangle.Height >= contours.BoundingRectangle.Width * 2)
					{
						//img.Draw(contours.BoundingRectangle, new Gray(80), 2);
						silRects.Add(contours.BoundingRectangle);
					}
				}
			}

			PreprocessRectangles(silRects,origin);

			int indx = 0;

			//Filter and join output rectangles
			for (int i = 0; i < silRects.Count; i++)
			{
				System.Drawing.Rectangle rect = silRects[i];

				//Check if not invalidated
				if (rect.X != -999)
				{
					

					//Here features should be extracted and a classifier applied

					// !!!!!!!!!!!!!!!!  CLASSIFIER HERE !!!!!!!!!!!!!!

					//Simple classification: If a head was detected before near this region it's not a mistake
					if(detectedNearby(rect)){

						//For now we assume that the test was passed
						int x = rect.X + rect.Width / 2;
						int y = rect.Y + rect.Height / 2;


						//This should be uncomented but right now we're just printing this on image
						//Microsoft.Xna.Framework.Vector3 spacePos=DepthColorMapper.DepthToSpaceCoords(x,y,origin);

						//Point colPos = DepthColorMapper.SimpleDepthToColorCoords(x, y);
						Point colPos = new Point(x, y);

						Microsoft.Xna.Framework.Vector3 spacePos = new Microsoft.Xna.Framework.Vector3(colPos.X, colPos.Y, 0);
						
						//Comment the line below
				//		Microsoft.Xna.Framework.Vector3 spacePos = new Microsoft.Xna.Framework.Vector3(x, y, 0);

						HeadModel head = new HeadModel(spacePos.X, spacePos.Y, spacePos.Z, rect.Height / 2);
						head.ColorBoundingRect = new Rectangle(colPos.X-rect.Width/2,colPos.Y-rect.Height/2,rect.Width,rect.Height);
						DetectedHead detHead = new DetectedHead(head, 1);
						if (indx < HeadIntegrator.MAX_HEADS)
						{
							heads[indx] = detHead;
							indx++;
						}
					}
				}
				
			}
			while (indx < HeadIntegrator.MAX_HEADS)
			{
				heads[indx] = new DetectedHead(new HeadModel(0, 0, 0, 10), 0);
				indx++;
			}
			lastDetected = new List<Rectangle>();
			lastDetected.AddRange(silRects);
		}

		void PreprocessRectangles(List<Rectangle> silRects,Emgu.CV.Image<Gray, Byte> img)
		{
			for (int i = 0; i < silRects.Count; i++)
			{
				System.Drawing.Rectangle rect = silRects[i];

				//Check if not invalidated
				if (rect.X != -999)
				{
					//Narrow the rectangle to the area of interest
					rect.Height /= 5;

					//The silhouette was divided
					if (rect.Y <5)
					{

						bool matchFound = false;

						//Check if there is another part of the silhouette detected
						for (int j = 0; j < silRects.Count; j++)
						{
							if (j != i)
							{
								Rectangle rect2 = silRects[j];
								//Also touches the top of screen
								if (rect2.Y <5)
								{

									bool left = rect2.X < rect.X;

									//Check if it's close enough
									if (left && rect.X - rect2.X + rect2.Width < AcceptableSpacing)
									{
										rect.Width = rect.X + rect.Width - rect2.X;
										rect.X = rect2.X;

										//Invalidate
										rect2.X = -999;
										silRects[j] = rect2;
										matchFound = true;
										break;
									}
									else
									{
										if (!left && rect2.X - rect.X + rect.Width < AcceptableSpacing)
										{

											rect.Width = rect2.X + rect2.Width - rect.X;
											//Invalidate
											rect2.X = -999;
											silRects[j] = rect2;
											matchFound = true;
											break;
										}
									}
								}
							}
						}

						//The other part of silhouette wasn't detected as a rectangle.
						if (!matchFound)
						{
							//We detect the average depth on both sides of rectangle to check if head's on left or right
							int leftDepth = 0;
							int rightDepth = 0;
							for (int j = 0; j < rect.Height; j++)
							{
								leftDepth += (int)(img.Data[rect.Y + j, rect.X, 0]);
								rightDepth += (int)(img.Data[rect.Y + j, rect.X + rect.Width, 0]);
							}
							leftDepth /= rect.Height;
							rightDepth /= rect.Height;

		//					MessageBox.Show(leftDepth.ToString() + " " + rightDepth.ToString());
							bool headOnTheLeft=false;
							if (leftDepth < rightDepth)
								headOnTheLeft = true;

							//Expand the area of rectangle to the direction of head
							//THERE SEEMS TO BE A LOGICAL ERROR SOMEWHERE HERE
							if (headOnTheLeft)
							{
								rect.X -= rect.Width;
								rect.Width *= 2;
							}
							else
							{
								rect.Width *= 2;
							}
						}
					}

					
				}
				silRects[i] = rect;
			}
			
		}

		private bool detectedNearby(Rectangle rect)
		{
			if (lastDetected != null)
			{
				for (int i = 0; i < lastDetected.Count; i++)
				{
					double dist = Math.Sqrt((rect.X - lastDetected[i].X) * (rect.X - lastDetected[i].X) + (rect.Y - lastDetected[i].Y) * (rect.Y - lastDetected[i].Y));
					if (dist < HeadIntegrator.NEARBY_RANGE)
					{
						return true;
					}
				}
			}
			return false;
		}

		public DetectedHead[] GetResponse()
		{
			return heads;
		}

		public double Weight()
		{
			return 0.6;
		}
	}
}

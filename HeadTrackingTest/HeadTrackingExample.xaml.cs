﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Forms;
using KinectIO;
using HeadTracking;

namespace HeadTrackingTest
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class HeadTrackingExample : Window
	{
		byte[] colorPixelData;
		short[] depthPixelData;
//		Skeleton[] skeletons;
		KinectEmulator emulator;
	//	static string DESTINATION_DIRECTORY = @"G:\Backups\KinectData\testdata";
		static string DESTINATION_DIRECTORY = @"../../../testdata";
		static string COLOR_FILE_NAME = "color.avi";
		static string DEPTH_AVI_FILE_NAME = "depth.avi";
		static string DEPTH_DPT_FILE_NAME = "depth.dpt";
		static string SKELETON_FILE_NAME = "skeleton.skl";
		static string TECHNICAL_DATA_FILE_NAME = "technicalData.txt";
		static string DESCRIPTION_FILE_NAME = "description.txt";
		int situationNumber = 0;
		SituationDescription situationDescription;
		DescriptionSerializer globalDescSerializer;
		TechnicalInfo technicalInfo;
		TechnicalSerializer globalTechSerializer;
		HeadTracker tracker;
		#region properties
		public string ColorFilePath
		{
			get { return DESTINATION_DIRECTORY + @"\" + situationNumber + @"\" + COLOR_FILE_NAME; }
		}
		public string DepthAviFilePath
		{
			get { return DESTINATION_DIRECTORY + @"\" + situationNumber + @"\" + DEPTH_AVI_FILE_NAME; }
		}
		public string DepthDptFilePath
		{
			get { return DESTINATION_DIRECTORY + @"\" + situationNumber + @"\" + DEPTH_DPT_FILE_NAME; }
		}
		public string SkeletonFilePath
		{
			get { return DESTINATION_DIRECTORY + @"\" + situationNumber + @"\" + SKELETON_FILE_NAME; }
		}
		public string TechnicalDataFilePath
		{
			get { return DESTINATION_DIRECTORY + @"\" + situationNumber + @"\" + TECHNICAL_DATA_FILE_NAME; }
		}
		public string DescriptionFilePath
		{
			get { return DESTINATION_DIRECTORY + @"\" + situationNumber + @"\" + DESCRIPTION_FILE_NAME; }
		}
		public string GlobalTechnicalDataFilePath
		{
			get { return DESTINATION_DIRECTORY + @"\" + TECHNICAL_DATA_FILE_NAME; }
		}
		public string GlobalDescriptionFilePath
		{
			get { return DESTINATION_DIRECTORY + @"\" + DESCRIPTION_FILE_NAME; }
		}
		public string SituationDirectory
		{
			get { return DESTINATION_DIRECTORY + @"\" + situationNumber; }
		}
		#endregion

		/*private const int SkeletonCount = 6;
		private readonly List<KinectSkeleton> skeletonCanvases = new List<KinectSkeleton>(SkeletonCount);
		private readonly List<Dictionary<JointType, JointMapping>> jointMappings = new List<Dictionary<JointType, JointMapping>>();
		*/
		public HeadTrackingExample()
		{
			emulator = new KinectEmulator();
			emulator.PerformanceBoost = true;
			InitializeComponent();
			this.Loaded += new RoutedEventHandler(TestWindow_Loaded);
			this.Unloaded += new RoutedEventHandler(TestWindow_Unloaded);
			InitializeVariables();
			emulator.ColorFilePath = ColorFilePath;
			emulator.DepthAviFilePath = DepthAviFilePath;
			emulator.DepthDptFilePath = DepthDptFilePath;
			emulator.SkeletonFilePath = SkeletonFilePath;
			emulator.EnableColorStream();
			emulator.EnableDepthStream();
			emulator.EnableSkeletonStream();
		}

		private void InitializeVariables()
		{
			//Variables
			globalDescSerializer = new DescriptionSerializer();
			globalDescSerializer.FileName = GlobalDescriptionFilePath;
			globalTechSerializer = new TechnicalSerializer();
			globalTechSerializer.FileName = GlobalTechnicalDataFilePath;
			if (globalDescSerializer.FileExists())
			{
				situationNumber = globalDescSerializer.GetLastNumber();
			}
			else
			{
				globalDescSerializer.CreateHeader();
				globalTechSerializer.CreateHeader();
				situationNumber = -1;
			}
			if (!globalTechSerializer.FileExists())
			{
				globalTechSerializer.CreateHeader();
			}
			SetRecordedMode();
			LoadSituationFromFile(situationNumber);
			//Fill interface
			tbxFolder.Text = DESTINATION_DIRECTORY;
			cbxSNumber.Text = situationNumber.ToString();
		//	tbxSNumber.Text = situationNumber.ToString();
		}

		private void SetRecordedMode()
		{

			List<int> numbers = globalDescSerializer.GetAllNumbers();
			if (numbers.Count > 0)
			{
				foreach (int number in numbers)
				{
					cbxSNumber.Items.Add(number);
				}
			}
		}

		private void LoadSituationFromFile(int sNumber)
		{
			situationNumber = sNumber;
			situationDescription = globalDescSerializer.Deserialize(situationNumber);
		//	technicalInfo = globalTechSerializer.Deserialize(situationNumber);
			if (situationDescription != null)
			{
				//TODO Fill interface with situation details
			}
			if (technicalInfo != null)
			{
				//TODO Fill interface with technicalInfo
			}
		}
		
		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			// Build a set of Skeletons, and bind each of their control properties to those
			// exposed on this class so that changes are propagated.
	/*		for (int i = 0; i < SkeletonCount; i++)
			{
				var skeletonCanvas = new KinectSkeleton();
				skeletonCanvas.ClipToBounds = true;

				var showBonesBinding = new System.Windows.Data.Binding("ShowBones");
				showBonesBinding.Source = this;
				skeletonCanvas.SetBinding(KinectSkeleton.ShowBonesProperty, showBonesBinding);

				var showJointsBinding = new System.Windows.Data.Binding("ShowJoints");
				showJointsBinding.Source = this;
				skeletonCanvas.SetBinding(KinectSkeleton.ShowJointsProperty, showJointsBinding);

				var showCenterBinding = new System.Windows.Data.Binding("ShowCenter");
				showCenterBinding.Source = this;
				skeletonCanvas.SetBinding(KinectSkeleton.ShowCenterProperty, showCenterBinding);

				this.skeletonCanvases.Add(skeletonCanvas);
				this.jointMappings.Add(new Dictionary<JointType, JointMapping>());
				this.SkeletonCanvasPanel.Children.Add(skeletonCanvas);
			}*/
		}

		void TestWindow_Unloaded(object sender, RoutedEventArgs e)
		{
			emulator.Stop();
		}

		void TestWindow_Loaded(object sender, RoutedEventArgs e)
		{
		//	emulator.ColorFrameReady += runtime_ColorFrameReady;
		//	emulator.DepthFrameReady += runtime_DepthFrameReady;
		//	emulator.SkeletonFrameReady += runtime_CustomSkeletonFrameReady;
			emulator.AllFramesMode = true;
			emulator.AllFramesReady += runtime_AllFramesReady;
			tracker = new HeadTracker();
			tracker.HeadFrameReady += runtime_HeadFrameReady;
			emulator.Start();
		}

		void runtime_AllFramesReady(object sender, CustomAllFramesReadyEventArgs e)
		{
			bool receivedData = false;
			if (e.ColorArrayLength > 0)
			{
				colorPixelData = new byte[e.ColorArrayLength];
				e.colorFrameReady.CopyColorPixelDataTo(colorPixelData);
				receivedData = true;
			}

			if (receivedData)
			{
				BitmapSource source = BitmapSource.Create(640, 480, 96, 96, PixelFormats.Bgr32, null, colorPixelData, 640 * 4);
				if (depthPixelData != null)
				{
					//			BitmapSource source2 = BitmapSource.Create(640, 480, 96, 96, PixelFormats.Gray8, null, HeadTracker.TestDepthConversion(depthPixelData), 640);
					//			BitmapSource source2 = BitmapSource.Create(640, 480, 96, 96, PixelFormats.Gray16, null, depthPixelData, 640*2);
					//			BitmapSource source2 = BitmapSource.Create(640, 480, 96, 96, PixelFormats.Gray8, null, MotionDetector.GetSample(colorPixelData), 640);
					videoImage.Source = source;
					//			videoImageHead.Source = source2;

					videoImageHead.Source = source;
					videoImageSkel.Source = source;

	//				tracker.AddColorFrame(colorPixelData);
				}
			}
			//////////////////////////////////////////
			receivedData = false;
			if (e.DepthArrayLength > 0)
			{
				depthPixelData = new short[e.DepthArrayLength];
				e.depthFrameReady.CopyDepthPixelDataTo(depthPixelData);

				receivedData = true;
			}

			if (receivedData)
			{
				BitmapSource source = BitmapSource.Create(640, 480, 64, 64, PixelFormats.Gray16, null, depthPixelData, 640 * 2);
				depthImage.Source = source;
		//		tracker.AddDepthFrame(depthPixelData);
			}

			tracker.AddAllFrames(colorPixelData, depthPixelData);



			///////////////////////////////////////////////////////////////////////
	/*		receivedData = false;
			if (e.SkeletonArrayLength > 0)
			{
				skeletons = new Skeleton[e.SkeletonArrayLength];
				e.skeletonFrameReady.CopySkeletonDataTo(skeletons);
				receivedData = true;
			}

			if (receivedData)
			{
				CustomSkeletonFrame CSFrame = e.CSFrame;
				foreach (var skeletonCanvas in this.skeletonCanvases)
				{
					skeletonCanvas.Skeleton = null;
				}

				int colorWidth = 640;
				int colorHeight = 480;

				for (int i = 0; i < this.skeletons.Length && i < this.skeletonCanvases.Count; i++)
				{
					var skeleton = this.skeletons[i];
					var skeletonCanvas = this.skeletonCanvases[i];
					var jointMapping = this.jointMappings[i];

					jointMapping.Clear();

					try
					{
						// Transform the data into the correct space
						// For each joint, we determine the exact X/Y coordinates for the target view
						foreach (Joint joint in skeleton.Joints)
						{
							ColorImagePoint mapped = CSFrame.JointPositions[i][(int)joint.JointType];

							// map back to skeleton.Width & skeleton.Height
							System.Windows.Point mappedPoint = new System.Windows.Point(
								(int)(videoImage.ActualWidth * mapped.X / colorWidth),
								(int)(videoImage.ActualHeight * mapped.Y / colorHeight));

							jointMapping[joint.JointType] = new JointMapping
							{
								Joint = joint,
								MappedPoint = mappedPoint
							};
						}
					}
					catch (UnauthorizedAccessException)
					{
						// Kinect is no longer available.
						return;
					}

					ColorImagePoint centered = CSFrame.SkeletonPositions[i];

					// map back to skeleton.Width & skeleton.Height
					System.Windows.Point centerPoint = new System.Windows.Point(
						(int)(videoImage.ActualWidth * centered.X / colorWidth),
						(int)(videoImage.ActualHeight * centered.Y / colorHeight));

					// Scale the skeleton thickness
					// 1.0 is the desired size at 640 width
					double scale = this.RenderSize.Width / 640;

					skeletonCanvas.Skeleton = skeleton;
					skeletonCanvas.JointMappings = jointMapping;
					skeletonCanvas.Center = centerPoint;
					skeletonCanvas.ScaleFactor = scale;
				}
			}*/
		}

		void runtime_ColorFrameReady(object sender, CustomColorImageFrameReadyEventArgs e)
		{
			bool receivedData = false;
			if (e.ColorArrayLength > 0)
			{
				colorPixelData = new byte[e.ColorArrayLength];
				e.CopyColorPixelDataTo(colorPixelData);
				receivedData = true;
			}

			if (receivedData)
			{
				BitmapSource source = BitmapSource.Create(640, 480, 96, 96, PixelFormats.Bgr32, null, colorPixelData, 640 * 4);
				if (depthPixelData != null)
				{
		//			BitmapSource source2 = BitmapSource.Create(640, 480, 96, 96, PixelFormats.Gray8, null, HeadTracker.TestDepthConversion(depthPixelData), 640);
		//			BitmapSource source2 = BitmapSource.Create(640, 480, 96, 96, PixelFormats.Gray16, null, depthPixelData, 640*2);
		//			BitmapSource source2 = BitmapSource.Create(640, 480, 96, 96, PixelFormats.Gray8, null, MotionDetector.GetSample(colorPixelData), 640);
					videoImage.Source = source;
		//			videoImageHead.Source = source2;
				
		//		videoImageHead.Source = source;
				videoImageSkel.Source = source;
				
				tracker.AddColorFrame(colorPixelData);
				}
			}
		}

		void runtime_DepthFrameReady(object sender, CustomDepthImageFrameReadyEventArgs e)
		{
			bool receivedData = false;
			if (e.DepthArrayLength > 0)
			{
				depthPixelData = new short[e.DepthArrayLength];
				e.CopyDepthPixelDataTo(depthPixelData);

				receivedData = true;
			}

			if (receivedData)
			{
				BitmapSource source = BitmapSource.Create(640, 480, 64, 64, PixelFormats.Gray16, null, depthPixelData, 640 * 2);
				depthImage.Source = source;
				tracker.AddDepthFrame(depthPixelData);
			}
		}

	/*	void runtime_CustomSkeletonFrameReady(object sender, CustomSkeletonFrameReadyEventArgs e)
		{
			bool receivedData = false;
			if (e.SkeletonArrayLength > 0)
			{
				skeletons = new Skeleton[e.SkeletonArrayLength];
				e.CopySkeletonDataTo(skeletons);
				receivedData = true;
			}

			if (receivedData)
			{
				CustomSkeletonFrame CSFrame = e.CSFrame;
				foreach (var skeletonCanvas in this.skeletonCanvases)
				{
					skeletonCanvas.Skeleton = null;
				}

				int colorWidth = 640;
				int colorHeight = 480;

				for (int i = 0; i < this.skeletons.Length && i < this.skeletonCanvases.Count; i++)
				{
					var skeleton = this.skeletons[i];
					var skeletonCanvas = this.skeletonCanvases[i];
					var jointMapping = this.jointMappings[i];

					jointMapping.Clear();

					try
					{
						// Transform the data into the correct space
						// For each joint, we determine the exact X/Y coordinates for the target view
						foreach (Joint joint in skeleton.Joints)
						{
							ColorImagePoint mapped = CSFrame.JointPositions[i][(int)joint.JointType];

							// map back to skeleton.Width & skeleton.Height
							System.Windows.Point mappedPoint = new System.Windows.Point(
								(int)(videoImage.ActualWidth * mapped.X / colorWidth),
								(int)(videoImage.ActualHeight * mapped.Y / colorHeight));

							jointMapping[joint.JointType] = new JointMapping
							{
								Joint = joint,
								MappedPoint = mappedPoint
							};
						}
					}
					catch (UnauthorizedAccessException)
					{
						// Kinect is no longer available.
						return;
					}

					ColorImagePoint centered = CSFrame.SkeletonPositions[i];

					// map back to skeleton.Width & skeleton.Height
					System.Windows.Point centerPoint = new System.Windows.Point(
						(int)(videoImage.ActualWidth * centered.X / colorWidth),
						(int)(videoImage.ActualHeight * centered.Y / colorHeight));

					// Scale the skeleton thickness
					// 1.0 is the desired size at 640 width
					double scale = this.RenderSize.Width / 640;

					skeletonCanvas.Skeleton = skeleton;
					skeletonCanvas.JointMappings = jointMapping;
					skeletonCanvas.Center = centerPoint;
					skeletonCanvas.ScaleFactor = scale;
				}
			}
		}*/

		void runtime_HeadFrameReady(object sender, HeadFrameReadyEventArgs e)
		{
			BitmapSource source=null;
			if (e.rawColorData!=null)
			{
				source = BitmapSource.Create(640, 480, 96, 96, PixelFormats.Bgr32, null, e.rawColorData, 640 * 4);
				videoImageHead.Source = source;
			}
	//		HeadCanvasPanel.Children.Clear();
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
			
			foreach (HeadModel head in e.heads)
			{
				Ellipse myEllipse = new Ellipse();
			//	source
	//			BitmapImage bmImage = new BitmapImage();
				Image myImage = new Image();

				DrawingVisual drawingVisual = new DrawingVisual();
				DrawingContext drawingContext = drawingVisual.RenderOpen();
				SolidColorBrush myBrush = new SolidColorBrush(Colors.Red);
				Pen pen = new Pen(myBrush, 5.0);
				drawingContext.DrawEllipse(null, pen, new Point(head.headX, head.headY), head.rad, head.rad);
				drawingContext.Close();

				RenderTargetBitmap bmp = new RenderTargetBitmap(640, 480, 96, 96, PixelFormats.Pbgra32);
				bmp.Render(drawingVisual);
				videoImageHead2.Source = bmp;

			/*	DrawingVisual drawingVisual = new DrawingVisual();
				DrawingContext drawingContext = drawingVisual.RenderOpen();
				SolidColorBrush myBrush = new SolidColorBrush(Colors.Red);
				Pen pen = new Pen(myBrush, 5.0);
				EllipseGeometry el = new EllipseGeometry(new Point(head.headX, head.headY + 5), head.rad, head.rad);
				drawingContext.DrawGeometry(null, pen, el);
				
				RenderTargetBitmap renderBmap = new RenderTargetBitmap(source.PixelWidth, source.PixelHeight, 96, 96, PixelFormats.Pbgra32);
				renderBmap.Render(drawingVisual);
				videoImageHead2.Source = renderBmap;
				// Describes the brush's color using RGB values.  
				// Each value has a range of 0-255.
				/*    mySolidColorBrush.Color = Color.FromArgb(255, 255, 255, 0);
					myEllipse.Fill = mySolidColorBrush;*/
			/*	myEllipse.StrokeThickness = 2;
				
				myEllipse.Stroke = new SolidColorBrush(Color.FromRgb((byte)(head.Probability * 255), (byte)(head.Probability * 255), (byte)(head.Probability * 255)));

				myEllipse.Width = head.rad;
				myEllipse.Height = head.rad;
				//System.Drawing.Point imgPos = DepthColorMapper.SpaceToColorCoords((float)head.headX, (float)head.headY, (float)head.headZ);
				
				myEllipse.Margin = new Thickness((430/640.0)*head.headX - head.rad / 2, (320.0/480)*head.headY - head.rad / 2, 0, 0);
				//myEllipse.Margin = new Thickness((430 / 640.0) * imgPos.X - head.rad / 2, (320.0 / 480) * imgPos.Y - head.rad / 2, 0, 0);
			//	myEllipse.
	//			HeadCanvasPanel.Children.Add(myEllipse);*/
			}
		//	videoImageHead.Source = source;
		}

		private void btnPlay_Click(object sender, RoutedEventArgs e)
		{
			//play situation
			int number;
			if (Int32.TryParse(this.cbxSNumber.Text, out number))
			{
				situationNumber = number;
			}
			StartRecordedSequence();
		}

		private void StartRecordedSequence()
		{
			emulator.ColorFilePath = ColorFilePath;
			emulator.DepthAviFilePath = DepthAviFilePath;
			emulator.DepthDptFilePath = DepthDptFilePath;
			emulator.SkeletonFilePath = SkeletonFilePath;
			tracker = new HeadTracker();
			//tracker.ResetSession();
			//MotionDetector.ResetS();
			tracker.HeadFrameReady += runtime_HeadFrameReady;
			emulator.Start();
		}


		private void btnChoosePath_Click(object sender, RoutedEventArgs e)
		{
			FolderBrowserDialog folderDialog = new FolderBrowserDialog();
			folderDialog.SelectedPath = "C:\\";

			DialogResult result = folderDialog.ShowDialog();
			if (result.ToString() == "OK")
			{
				tbxFolder.Text = folderDialog.SelectedPath;
				DESTINATION_DIRECTORY = folderDialog.SelectedPath;
			}
		}
	}

}

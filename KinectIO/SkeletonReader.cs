﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace KinectIO
{
	public class SkeletonReader
	{
		private Stream stream;
		private BinaryReader reader;

		public SkeletonReader(string filePath)
		{
			stream = new FileStream(filePath, FileMode.Open);
			reader = new BinaryReader(stream);
		}

		public List<CustomSkeletonFrame> Read()
		{
			List<CustomSkeletonFrame> frames = new List<CustomSkeletonFrame>();
			while (reader.BaseStream.Position != reader.BaseStream.Length)
			{
				CustomSkeletonFrame csf = new CustomSkeletonFrame();
				csf.Timestamp = reader.ReadInt64();
				csf.FloorClipPlane = new Tuple<float, float, float, float>(reader.ReadSingle(), reader.ReadSingle(),reader.ReadSingle(), reader.ReadSingle());
				csf.FrameNumber = reader.ReadInt32();
				BinaryFormatter formatter = new BinaryFormatter();
				csf.SkeletonData=(Skeleton[])formatter.Deserialize(reader.BaseStream);
				csf.SkeletonPositionsTuple = (Tuple<double,double>[])formatter.Deserialize(reader.BaseStream);
				csf.JointPositionsTuple = (Tuple<double, double>[][])formatter.Deserialize(reader.BaseStream);

				csf.SatisfyTiczka();

				frames.Add(csf);
			}

			return frames;
		}

		public void Close()
		{
			stream.Close();
		}
	}
}

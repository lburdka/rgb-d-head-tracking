﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KinectIO
{
	public class CustomColorImageFrameReadyEventArgs:EventArgs
	{
		private byte[] colorImageBytes;
		public int ColorArrayLength;
		//private ColorImageFrame CFrame;

		public CustomColorImageFrameReadyEventArgs(byte[] imageBytes)
		{
			colorImageBytes = imageBytes;
			ColorArrayLength = colorImageBytes.Length;
		}

	/*	public CustomColorImageFrameReadyEventArgs(ColorImageFrame CFrame)
		{
			this.CFrame = CFrame;
			colorImageBytes = new byte[CFrame.PixelDataLength];
			ColorArrayLength = colorImageBytes.Length;
			CFrame.CopyPixelDataTo(colorImageBytes);
		}*/

		public void CopyColorPixelDataTo(byte[] pixelData)
		{
	/*		if (CFrame != null)
			{
				CFrame.CopyPixelDataTo(pixelData);
			}
			else
			{*/
				Array.Copy(colorImageBytes, pixelData, colorImageBytes.Length);
			//}
		}

		public void CopyColorPixelDataTo(IntPtr pixelData,int pixelDataLength)
		{
		/*	if (CFrame != null)
			{
				CFrame.CopyPixelDataTo(pixelData, pixelDataLength);
			}
			else
			{*/
				Marshal.Copy(colorImageBytes, 0, pixelData, colorImageBytes.Length);
			//}
		}
	}
}

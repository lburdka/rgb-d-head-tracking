﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;

namespace KinectIO
{
	public class CustomSkeleton
	{
		private Dictionary<JointType, Joint> joints;
		SkeletonPoint position;
		SkeletonTrackingState trackingState;
		int trackingId;

		#region Properties
		public Dictionary<JointType, Joint> Joints
		{
			get { return joints; }
			set { joints = value; }
		}

		public SkeletonPoint Position
		{
			get { return position; }
			set { position = value; }
		}

		public SkeletonTrackingState TrackingState
		{
			get { return trackingState; }
			set { trackingState = value; }
		}

		public int TrackingId
		{
			get { return trackingId; }
			set { trackingId = value; }
		}
		#endregion

		public CustomSkeleton()
		{
			joints = new Dictionary<JointType, Joint>();
		}

		public CustomSkeleton(Skeleton skel)
		{
			joints = new Dictionary<JointType, Joint>();
			foreach (JointType type in Enum.GetValues(typeof(JointType)))
			{
				Joint j = new Joint();
				j = skel.Joints[type];
				joints.Add(type, j);
			}
			position = skel.Position;
			trackingState = skel.TrackingState;
			trackingId = skel.TrackingId;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace KinectIO
{
	class DepthReader
	{
		private Stream stream;
		private BinaryReader reader;

		public DepthReader(string filePath)
		{
			stream = new FileStream(filePath, FileMode.Open);
			reader = new BinaryReader(stream);
		}

		public List<CustomDepthFrame> Read()
		{
			List<CustomDepthFrame> frames = new List<CustomDepthFrame>();
			while (reader.BaseStream.Position != reader.BaseStream.Length)
			{

				CustomDepthFrame cdf = new CustomDepthFrame();
				BinaryFormatter formatter = new BinaryFormatter();
				/*cdf.Format = (string)*/formatter.Deserialize(reader.BaseStream);
				cdf.MaxDepth = reader.ReadInt32();
				cdf.MinDepth = reader.ReadInt32();
				cdf.PixelDataLength = reader.ReadInt32();
			/*	cdf.Range = (string)*/formatter.Deserialize(reader.BaseStream);

				cdf.DepthData=(short[])formatter.Deserialize(reader.BaseStream);

				frames.Add(cdf);
			}

			return frames;
		}

		public void Close()
		{
			stream.Close();
		}
	}
}

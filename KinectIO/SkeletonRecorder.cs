﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace KinectIO
{
	public class SkeletonRecorder
	{
		private Stream stream;
		private BinaryWriter writer;

		public SkeletonRecorder(string filePath)
		{
			stream = new FileStream(filePath, FileMode.Create);
			writer = new BinaryWriter(stream);

		}

		public void AddFrame(SkeletonFrame sf)
		{
			AddFrame(new CustomSkeletonFrame(sf,null));
			
		}

		public void AddFrame(CustomSkeletonFrame csf)
		{

			writer.Write(csf.Timestamp);
			writer.Write(csf.FloorClipPlane.Item1);
			writer.Write(csf.FloorClipPlane.Item2);
			writer.Write(csf.FloorClipPlane.Item3);
			writer.Write(csf.FloorClipPlane.Item4);

			writer.Write(csf.FrameNumber);

			// Skeletons
			Skeleton[] skeletons = csf.SkeletonData;

			BinaryFormatter formatter = new BinaryFormatter();
			formatter.Serialize(writer.BaseStream, skeletons);

			formatter.Serialize(writer.BaseStream, csf.SkeletonPositionsTuple);
			formatter.Serialize(writer.BaseStream, csf.JointPositionsTuple);
		}


		public void Close()
		{
			stream.Close();
		}

		


	}


}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AviFile;

namespace KinectIO
{
	public class KinectEmulator
	{
		public event EventHandler<CustomColorImageFrameReadyEventArgs> ColorFrameReady;
		public event EventHandler<CustomDepthImageFrameReadyEventArgs> DepthFrameReady;
	//	public event EventHandler<CustomSkeletonFrameReadyEventArgs> SkeletonFrameReady;
		public event EventHandler<CustomAllFramesReadyEventArgs> AllFramesReady;

		AviManager aviManager;
		AviManager depthAviManager;
		VideoStream colorStream;
		VideoStream depthStream;
	//	SkeletonReader skeletonReader;
		DepthReader depthReader;
	//	List<CustomSkeletonFrame> skeletonFrames;
		List<CustomDepthFrame> depthFrames;
		Timer timer;
		int colorFrameNumber = 0;
		int depthFrameNumber = 0;
		int skeletonFrameNumber = 0;
		bool kinectMode;
		bool colorStreamEnabled;
		bool depthStreamEnabled;
		bool skeletonStreamEnabled;
		bool colorStreamFinished;
		bool depthStreamFinished;
		bool skeletonStreamFinished;
		bool readDepthFromReader = true;
		bool allFramesMode = false;
		bool performanceBoost = false;
		List<byte[]> imageList;


		private string colorFilePath;
		private string depthAviFilePath;
		private string depthDptFilePath;
		private string skeletonFilePath;

		public int numCFrames = 0;
		public int numDframes = 0;
		public int numSFrames = 0;
		public int numCalls = 0;

		#region properties

		public bool PerformanceBoost
		{
			get { return performanceBoost; }
			set { performanceBoost = value; }
		}
		public bool AllFramesMode
		{
			get { return allFramesMode; }
			set { allFramesMode = value; }
		}
		public bool KinectMode
		{
			get { return kinectMode; }
			set { kinectMode = value; }
		}
		public string ColorFilePath
		{
			get { return colorFilePath; }
			set { colorFilePath = value; }
		}

		public string DepthAviFilePath
		{
			get { return depthAviFilePath; }
			set { depthAviFilePath = value; }
		}

		public string DepthDptFilePath
		{
			get { return depthDptFilePath; }
			set { depthDptFilePath = value; }
		}

		public string SkeletonFilePath
		{
			get { return skeletonFilePath; }
			set { skeletonFilePath = value; }
		}

		#endregion

		public KinectEmulator()
		{
			kinectMode = false;

		}

		public void EnableColorStream()
		{
			colorStreamEnabled = true;
		}

		public void EnableDepthStream()
		{
			depthStreamEnabled = true;
		}

		public void EnableSkeletonStream()
		{
			skeletonStreamEnabled = true;
		}

		public void Stop()
		{
			
				if (timer != null)
				{
					timer.Stop();
				}
			
		}

		public void Resume()
		{
			if (!kinectMode)
			{
				if (timer != null)
				{
					timer.Start();
				}
			}
		}

		public void Start()
		{
			
				if (colorStreamEnabled || depthStreamEnabled || skeletonStreamEnabled)
				{
					EmulateColorFrames();
				}

			
		}

		public void EmulateColorFrames()
		{
			if (colorStreamEnabled)
			{
				aviManager = new AviManager(colorFilePath, true);
				colorStream = aviManager.GetVideoStream();
				colorStream.GetFrameOpen();
				colorFrameNumber = 0;
				if (performanceBoost)
				{
					imageList = new List<byte[]>();
					int numFr = colorStream.CountFrames;
					byte[] bytes;
					Bitmap bmp;
					for (int i = 0; i < numFr; i++)
					{
						bmp = colorStream.GetBitmap(i);
						bytes = ImageToByte2(bmp);
						imageList.Add(bytes);
					}
					colorStream.GetFrameClose();
				}
			}

			if (depthStreamEnabled)
			{
				if (readDepthFromReader)
				{
					depthReader = new DepthReader(depthDptFilePath);
					depthFrames = depthReader.Read();
					depthReader.Close();
					depthFrameNumber = 0;
				}
				else
				{
					depthAviManager = new AviManager(depthAviFilePath, true);
					depthStream = depthAviManager.GetVideoStream();
					depthStream.GetFrameOpen();
					depthFrameNumber = 0;
				}
			}

			
			if (timer != null)
			{
				timer.Stop();
			}
			timer = new Timer();
		//	if (colorStreamEnabled || depthStreamEnabled || skeletonStreamEnabled)
	//		{
			if (allFramesMode)
			{
				timer.Tick += timerTick;
			}
			else
			{
				//		}
				if (colorStreamEnabled)
				{
					timer.Tick += colorTimer_Tick;
				}
				if (depthStreamEnabled)
				{
					timer.Tick += depthTimer_Tick;
				}

			}
			timer.Interval = (66) * (1);
			timer.Enabled = true;
			timer.Start();
		}

		void timerTick(object sender, EventArgs e)
		{
			bool end = false;
			CustomDepthFrame cdf = null;
			byte[] imageBytes=null;
			if (colorStreamEnabled)
			{
				if (performanceBoost)
				{
					if (imageList.Count > 0)
					{
						imageBytes = imageList[0];
						imageList.RemoveAt(0);
						FireColorEvent(imageBytes);
						colorFrameNumber++;
					}
					else
					{
						timer.Tick -= timerTick;
						end = true;
					}
				}
				else
				{
					if (colorFrameNumber < colorStream.CountFrames)
					{
						Bitmap bmp = colorStream.GetBitmap(colorFrameNumber);
						imageBytes = ImageToByte2(bmp);
						FireColorEvent(imageBytes);
						colorFrameNumber++;
					}
					else
					{
						timer.Tick -= timerTick;
						colorStream.GetFrameClose();
						end = true;
					}
				}
			}
		//
			short[] sdata=null;
			if (depthStreamEnabled)
			{
				if (readDepthFromReader)
				{
					if (depthFrameNumber < depthFrames.Count)
					{
						cdf = depthFrames[depthFrameNumber];
						FireDepthEvent(cdf);
						depthFrameNumber++;
					}
					else
					{
						timer.Tick -= timerTick;
						end = true;
					}
				}
				else
				{
					if (depthFrameNumber < depthStream.CountFrames)
					{
						Bitmap bmp = depthStream.GetBitmap(depthFrameNumber);

						byte[] depthImageBytes = ImageToByte3(bmp);

						sdata = new short[depthImageBytes.Length / 4];

						byte[] imageBytes1 = new byte[depthImageBytes.Length / 2];
						for (int i = 0; i < depthImageBytes.Length / 4; i++)
						{
							imageBytes1[i * 2] = depthImageBytes[i * 4];
							imageBytes1[i * 2 + 1] = depthImageBytes[i * 4 + 1];
						}

						Buffer.BlockCopy(imageBytes1, 0, sdata, 0, imageBytes1.Length);
						FireDepthEvent(sdata);
						depthFrameNumber++;
					}
					else
					{
						timer.Tick -= timerTick;
						depthStream.GetFrameClose();
					}
				}
			}
			if (!end)
			{
				FireAllFramesEvent(imageBytes, cdf);
			}
		}

		void colorTimer_Tick(object sender, EventArgs e)
		{
			if (colorFrameNumber < colorStream.CountFrames)
			{
				Bitmap bmp = colorStream.GetBitmap(colorFrameNumber);
				byte[] imageBytes = ImageToByte2(bmp);
				FireColorEvent(imageBytes);
				colorFrameNumber++;
			}
			else
			{
				timer.Tick -= colorTimer_Tick;
				colorStream.GetFrameClose();
			}
		}

		void depthTimer_Tick(object sender, EventArgs e)
		{
			if (readDepthFromReader)
			{
				if (depthFrameNumber < depthFrames.Count)
				{
					FireDepthEvent(depthFrames[depthFrameNumber]);
					depthFrameNumber++;
				}
				else
				{
					timer.Tick -= depthTimer_Tick;
				}
			}
			else
			{
				if (depthFrameNumber < depthStream.CountFrames)
				{
					Bitmap bmp = depthStream.GetBitmap(depthFrameNumber);

					byte[] depthImageBytes = ImageToByte3(bmp);

					short[] sdata = new short[depthImageBytes.Length / 4];

					byte[] imageBytes = new byte[depthImageBytes.Length / 2];
					for (int i = 0; i < depthImageBytes.Length / 4; i++)
					{
						imageBytes[i * 2] = depthImageBytes[i * 4];
						imageBytes[i * 2 + 1] = depthImageBytes[i * 4 + 1];
					}

					Buffer.BlockCopy(imageBytes, 0, sdata, 0, imageBytes.Length);
					FireDepthEvent(sdata);
					depthFrameNumber++;
				}
				else
				{
					timer.Tick -= depthTimer_Tick;
					depthStream.GetFrameClose();
				}
			}
		}


		public void FireAllFramesEvent(byte[] imageBytes, CustomDepthFrame CDFrame)
		{
			CustomAllFramesReadyEventArgs args = new CustomAllFramesReadyEventArgs(imageBytes, CDFrame);

			if (AllFramesReady != null)
			{
				AllFramesReady(this, args);
			}
		}


		public void FireColorEvent(byte[] imageBytes)
		{
			CustomColorImageFrameReadyEventArgs color = new CustomColorImageFrameReadyEventArgs(imageBytes);

			if (ColorFrameReady != null)
			{
				ColorFrameReady(this, color);
			}
		}


		public void FireDepthEvent(short[] imageBytes)
		{
			CustomDepthImageFrameReadyEventArgs depth = new CustomDepthImageFrameReadyEventArgs(imageBytes);

			if (DepthFrameReady != null)
			{
				DepthFrameReady(this, depth);
			}
		}


		public void FireDepthEvent(CustomDepthFrame CDFrame)
		{
			CustomDepthImageFrameReadyEventArgs depth = new CustomDepthImageFrameReadyEventArgs(CDFrame);

			if (DepthFrameReady != null)
			{
				DepthFrameReady(this, depth);
			}
		}

	/*	public void FireSkeletonEvent(CustomSkeletonFrame CSFrame)
		{
			CustomSkeletonFrameReadyEventArgs skeleton = new CustomSkeletonFrameReadyEventArgs(CSFrame);

			if (SkeletonFrameReady != null)
			{
				SkeletonFrameReady(this, skeleton);
			}
		}
		*/

		public static byte[] ImageToByte(Image img)
		{
			ImageConverter converter = new ImageConverter();
			return (byte[])converter.ConvertTo(img, typeof(byte[]));
		}

		public static byte[] ImageToByte2(Image img)
		{
			byte[] byteArray = new byte[1228800];
			byte[] newArray = new byte[1228800];

			using (MemoryStream stream = new MemoryStream())
			{
				img.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
				stream.Close();

				Array.Copy(stream.ToArray(),54,byteArray,0,1228800);
				for (int i = 0; i < 480; i++)
				{
					Array.Copy(byteArray, i * 2560, newArray, 1228800-(i+1)*2560, 2560);
				}
			}
			return newArray;
		}

		public static byte[] ImageToByte3(Image img)
		{
			byte[] byteArray = new byte[307200];
			byte[] newArray = new byte[307200];

			using (MemoryStream stream = new MemoryStream())
			{
				img.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
				stream.Close();

				Array.Copy(stream.ToArray(), 54, byteArray, 0, 307200);
				for (int i = 0; i < 240; i++)
				{
					Array.Copy(byteArray, i * 1280, newArray, 307200 - (i + 1) * 1280, 1280);
				}
			}
			return newArray;
		}
	}
}

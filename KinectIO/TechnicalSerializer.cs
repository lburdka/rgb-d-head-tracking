﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace KinectIO
{
	public class TechnicalSerializer
	{
		public string FileName { get; set; }

		public TechnicalSerializer()
		{
			
		}

		public bool FileExists()
		{
			return File.Exists(FileName);
		}

		private string getHeader()
		{
			return "number,fps,colorFormat,depthFormat,depthRange,skeletonMode,elevationAngle,uniqueKinectId,maxDepth,minDepth";
		}

		public void CreateHeader()
		{
			using (StreamWriter outfile = new StreamWriter(FileName))
			{
				outfile.WriteLine(getHeader());
			}
		}

		public void SerializeSingle(TechnicalInfo info)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(getHeader());
			fillSingleDescription(info, sb);

			using (StreamWriter outfile = new StreamWriter(FileName))
			{
				outfile.Write(sb.ToString());
			}
		}

		private void fillSingleDescription(TechnicalInfo desc, StringBuilder sb)
		{
			sb.Append(desc.SituationNumber);
			sb.Append(",");
			sb.Append(desc.FPS);
			sb.Append(",");
			sb.Append(desc.ColorFormat);
			sb.Append(",");
			sb.Append(desc.DepthFormat);
			sb.Append(",");
			sb.Append(desc.DepthRange);
			sb.Append(",");
			sb.Append(desc.SkeletonMode);
			sb.Append(",");
			sb.Append(desc.ElevationAngle);
			sb.Append(",");
			sb.Append(desc.UniqueKinectID);
			sb.Append(",");
			sb.Append(desc.MaxDepth);
			sb.Append(",");
			sb.Append(desc.MinDepth);
			sb.AppendLine();
		}

		public int GetLastNumber()
		{
			int last = 0;

			using (StreamReader sr = new StreamReader(FileName))
			{
				sr.ReadLine();
				while (!sr.EndOfStream)
				{
					last = Int32.Parse(sr.ReadLine().Split(',')[0]);
				}
			}
			return last;
		}

		public void Append(TechnicalInfo info)
		{
			StringBuilder sb = new StringBuilder();

			int last = GetLastNumber();

			if (info.SituationNumber > last)
			{
				fillSingleDescription(info, sb);
				using (StreamWriter outfile = File.AppendText(FileName))
				{
					outfile.Write(sb.ToString());
				}
			}
			else
			{
				using (StreamReader sr = new StreamReader(FileName))
				{
					for (int i = 0; i < info.SituationNumber + 1; i++)
					{
						sb.AppendLine(sr.ReadLine());
					}
					fillSingleDescription(info, sb);
					sr.ReadLine();
					for (int i = info.SituationNumber; i < last; i++)
					{
						sb.AppendLine(sr.ReadLine());
					}
				}
				using (StreamWriter outfile = new StreamWriter(FileName))
				{
					outfile.Write(sb.ToString());
				}
			}
		}

		public List<TechnicalInfo> DeserializeAll()
		{
			return null;
		}

		public TechnicalInfo Deserialize(int number)
		{
			return null;
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace KinectIO
{
	public class CoordinateCalculations
	{
		private Tuple<float, float, float, float> FloorClipPlane;
		Matrix transform;

		public CoordinateCalculations(float kinectAngle, float zAngle, float kinectHeight)
		{

			Matrix rot = Matrix.CreateFromYawPitchRoll(0, MathHelper.ToRadians(-kinectAngle), MathHelper.ToRadians(zAngle));
			Matrix trans = Matrix.CreateTranslation(0, kinectHeight/100, 0);
			transform = Matrix.Multiply(rot, trans);
		}

		public CoordinateCalculations(Tuple<float, float, float, float> FloorClipPlane)
		{
			this.FloorClipPlane = FloorClipPlane;
			Vector3 norm = new Vector3(FloorClipPlane.Item1, FloorClipPlane.Item2, FloorClipPlane.Item3);
			Vector3 Yaxis = new Vector3(0, 1, 0);
			Vector3 rotAxis = Vector3.Cross(norm, Yaxis);
			float cos = Vector3.Dot(norm, Yaxis);
			double angle = Math.Acos(cos);
			Matrix rot = Matrix.CreateFromAxisAngle(rotAxis, (float)angle);
			Matrix trans = Matrix.CreateTranslation(0, FloorClipPlane.Item4, 0);
			transform = Matrix.Multiply(rot, trans);
		}

		public Vector3 ToFloor(Vector3 point)
		{
			return Vector3.Transform(point, transform);
		}
	}
}

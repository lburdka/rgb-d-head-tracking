﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KinectIO
{
	public class CustomDepthFrame
	{
	//	public string Format { get; set; }
		public int MaxDepth { get; set; }
		public int MinDepth { get; set; }
		public int PixelDataLength { get; set; }
	//	public string Range { get; set; }
		public short[] DepthData{get; set;}

		public CustomDepthFrame()
		{
			
		}

	/*	public CustomDepthFrame(DepthImageFrame df)
		{
			if (df != null)
			{
				DepthData = new short[df.PixelDataLength];
				df.CopyPixelDataTo(DepthData);
				Format = df.Format;
				MaxDepth = df.MaxDepth;
				MinDepth = df.MinDepth;
				PixelDataLength = df.PixelDataLength;
				Range = df.Range;
			}
		}*/
	}
}

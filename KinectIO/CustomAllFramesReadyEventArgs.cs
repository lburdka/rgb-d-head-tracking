﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectIO
{
	public class CustomAllFramesReadyEventArgs
	{
		public CustomColorImageFrameReadyEventArgs colorFrameReady;
		public CustomDepthImageFrameReadyEventArgs depthFrameReady;
	//	public CustomSkeletonFrameReadyEventArgs skeletonFrameReady;

		public int ColorArrayLength
		{
			get
			{
				if (colorFrameReady != null)
				{
					return colorFrameReady.ColorArrayLength;
				}
				else return 0;
			}
		}

		public int DepthArrayLength
		{
			get
			{
				if (depthFrameReady != null)
				{
					return depthFrameReady.DepthArrayLength;
				}
				else return 0;
			}
		}


	/*	public CustomSkeletonFrame CSFrame
		{
			get
			{
				if (skeletonFrameReady != null)
				{
					return skeletonFrameReady.CSFrame;
				}
				else return null;
			}
		}*/

		public CustomAllFramesReadyEventArgs(byte[] imageBytes, CustomDepthFrame CDFrame)
		{
			if (imageBytes != null)
			{
				colorFrameReady = new CustomColorImageFrameReadyEventArgs(imageBytes);
			}
			if (CDFrame != null)
			{
				depthFrameReady = new CustomDepthImageFrameReadyEventArgs(CDFrame);
			}
		}
	}
}

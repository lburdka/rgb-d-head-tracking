﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectIO
{
	public class SituationDetails
	{
		public SituationDescription description;
		public TechnicalInfo techInfo;

		public SituationDetails(SituationDescription desc, TechnicalInfo info)
		{
			description = desc;
			techInfo = info;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace KinectIO
{
	public class DepthRecorder
	{
		private Stream stream;
		private BinaryWriter writer;

		public DepthRecorder(string filePath)
		{
			stream = new FileStream(filePath, FileMode.Create);
			writer = new BinaryWriter(stream);

		}

	/*	public void AddFrame(DepthImageFrame df)
		{
			AddFrame(new CustomDepthFrame(df));
			
		}

		public void AddFrame(CustomDepthFrame cdf)
		{
			BinaryFormatter formatter = new BinaryFormatter();
			formatter.Serialize(writer.BaseStream, cdf.Format);
			writer.Write(cdf.MaxDepth);
			writer.Write(cdf.MinDepth);
			writer.Write(cdf.PixelDataLength);
			formatter.Serialize(writer.BaseStream, cdf.Range);

			formatter.Serialize(writer.BaseStream, cdf.DepthData);
		}*/

		public void Close()
		{
			stream.Close();
		}


	}
}

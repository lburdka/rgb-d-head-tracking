﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectIO
{
	public class AttributeFilterSerializer : CSVSerializer
	{
		protected override string getHeader()
		{
			return "attribute";
		}

		private void addSingleNumber(string str, StringBuilder sb)
		{
			sb.Append(str);
			sb.AppendLine();
		}

		public void Append(string str)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(str);
			sb.AppendLine();
			using (StreamWriter outfile = File.AppendText(FileName))
			{
				outfile.Write(sb.ToString());
			}
		}

		public void Serialize(List<string> strings)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(getHeader());
			foreach (string str in strings)
			{
				sb.AppendLine(str);
			}

			using (StreamWriter outfile = new StreamWriter(FileName))
			{
				outfile.Write(sb.ToString());
			}

		}

		public List<string> GetAllAttributes()
		{
			List<string> strings = new List<string>();
			using (StreamReader sr = new StreamReader(FileName))
			{
				sr.ReadLine();
				while (!sr.EndOfStream)
				{
					strings.Add(sr.ReadLine().Split(',')[0]);
				}
			}
			return strings;
		}
	}
}

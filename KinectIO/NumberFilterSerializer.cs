﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectIO
{
	public class NumberFilterSerializer: CSVSerializer
	{
		protected override string getHeader()
		{
			return "number";
		}

		private void addSingleNumber(int number, StringBuilder sb)
		{
			sb.Append(number);
			sb.AppendLine();
		}

		public void Append(int number)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(number);
			sb.AppendLine();
			using (StreamWriter outfile = File.AppendText(FileName))
			{
				outfile.Write(sb.ToString());
			}
		}

		public void Serialize(List<int> numbers)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(getHeader());
			foreach (int n in numbers)
			{
				sb.AppendLine(n.ToString());
			}

			using (StreamWriter outfile = new StreamWriter(FileName))
			{
				outfile.Write(sb.ToString());
			}

		}

		public List<int> GetAllNumbers()
		{
			List<int> ints = new List<int>();
			using (StreamReader sr = new StreamReader(FileName))
			{
				sr.ReadLine();
				while (!sr.EndOfStream)
				{
					ints.Add(Int32.Parse(sr.ReadLine().Split(',')[0]));
				}
			}
			return ints;
		}
	}
}

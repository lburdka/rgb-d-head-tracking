﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectIO
{
	public class SituationDescription
	{
		public int situationNumber;
		public string situation;
		public int person;
		public string subSituation;
		public int horizontalAngle;
		public int elevationAngle;
		public int kinectAltitude;
		public int personDistance;
		public string lightning;
		public string skeletonMode;
		public string depthRange;
		public string elapsedTime;
		public int framesCount;
	}
}

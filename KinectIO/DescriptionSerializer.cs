﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace KinectIO
{
	public class DescriptionSerializer : CSVSerializer
	{
		public DescriptionSerializer()
		{
		}

		protected override string getHeader()
		{
			return "number,situation,person,subSituation,horizontalAngle,elevationAngle,kinectAltitude,personDistance,lighting,skeletonMode,depthRange,elapsedTime,framesCount";
		}

		private void fillSingleDescription(SituationDescription desc, StringBuilder sb)
		{
			sb.Append(desc.situationNumber);
			sb.Append(",");
			sb.Append(desc.situation);
			sb.Append(",");
			sb.Append(desc.person);
			sb.Append(",");
			sb.Append(desc.subSituation);
			sb.Append(",");
			sb.Append(desc.horizontalAngle);
			sb.Append(",");
			sb.Append(desc.elevationAngle);
			sb.Append(",");
			sb.Append(desc.kinectAltitude);
			sb.Append(",");
			sb.Append(desc.personDistance);
			sb.Append(",");
			sb.Append(desc.lightning);
			sb.Append(",");
			sb.Append(desc.skeletonMode);
			sb.Append(",");
			sb.Append(desc.depthRange);
			sb.Append(",");
			sb.Append(desc.elapsedTime);
			sb.Append(",");
			sb.Append(desc.framesCount);
			sb.AppendLine();
		}

		private SituationDescription createSituationDescription(string s)
		{
			SituationDescription sdc = new SituationDescription();
			string[] vars=s.Split(',');
			sdc.situationNumber = Int32.Parse(vars[0]);
			sdc.situation = vars[1];
			sdc.person = Int32.Parse(vars[2]);
			sdc.subSituation = vars[3];
			sdc.horizontalAngle = Int32.Parse(vars[4]);
			sdc.elevationAngle = Int32.Parse(vars[5]);
			sdc.kinectAltitude = Int32.Parse(vars[6]);
			sdc.personDistance = Int32.Parse(vars[7]);
			sdc.lightning = vars[8];
			sdc.skeletonMode = vars[9];
			sdc.depthRange = vars[10];
			sdc.elapsedTime = vars[11];
			sdc.framesCount = Int32.Parse(vars[12]);
			return sdc;
		}

		public void SerializeSingle(SituationDescription desc)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(getHeader());
			fillSingleDescription(desc, sb);

			using (StreamWriter outfile = new StreamWriter(FileName))
			{
				outfile.Write(sb.ToString());
			}

		}

		public void Append(SituationDescription desc)
		{
			StringBuilder sb = new StringBuilder();

			int last = GetLastNumber();

			if (desc.situationNumber > last)
			{
				fillSingleDescription(desc, sb);
				using (StreamWriter outfile = File.AppendText(FileName))
				{
					outfile.Write(sb.ToString());
				}
			}
			else
			{
				using (StreamReader sr = new StreamReader(FileName))
				{
					for (int i = 0; i < desc.situationNumber+1; i++)
					{
						sb.AppendLine(sr.ReadLine());
					}
					fillSingleDescription(desc, sb);
					sr.ReadLine();
					for(int i=desc.situationNumber;i<last;i++){
						sb.AppendLine(sr.ReadLine());
					}
				}
				using (StreamWriter outfile = new StreamWriter(FileName))
				{
					outfile.Write(sb.ToString());
				}
			}
		}

		public List<SituationDescription> DeserializeAll()
		{
			List<SituationDescription> sits = new List<SituationDescription>();
			using (StreamReader sr = new StreamReader(FileName))
			{
				sr.ReadLine();
				while (!sr.EndOfStream)
				{
					sits.Add(createSituationDescription(sr.ReadLine()));
				}
			}

			return sits;
		}

		public SituationDescription Deserialize(int number)
		{
			int pos = 0;
			using (StreamReader sr = new StreamReader(FileName))
			{
				string s;
				sr.ReadLine();
				while (!sr.EndOfStream)
				{
					s = sr.ReadLine();

					if (s.Substring(0, s.IndexOf(",")).Equals(number.ToString()))
						return createSituationDescription(s);
					else
					{
						
					}
					pos++;
				}
			}
			return null;
		}

		public int GetLastNumber()
		{
			int last = 0;

			using (StreamReader sr = new StreamReader(FileName))
			{
				sr.ReadLine();
				while (!sr.EndOfStream)
				{
					last = Int32.Parse(sr.ReadLine().Split(',')[0]);
				}
			}
			return last;
		}

		public List<int> GetAllNumbers()
		{
			List<int> ints = new List<int>();
			using (StreamReader sr = new StreamReader(FileName))
			{
				sr.ReadLine();
				while (!sr.EndOfStream)
				{
					ints.Add(Int32.Parse(sr.ReadLine().Split(',')[0]));
				}
			}
			return ints;
		}
	}
}

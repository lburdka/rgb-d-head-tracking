﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;

namespace KinectIO
{
	public class CustomSkeletonFrameReadyEventArgs
	{
		public CustomSkeletonFrame CSFrame;
		private SkeletonFrame SFrame;
		public int SkeletonArrayLength;

		public CustomSkeletonFrameReadyEventArgs(SkeletonFrame SFrame, KinectSensor sensor)
		{
			this.SFrame = SFrame;
			CSFrame = new CustomSkeletonFrame(SFrame, sensor);
			SkeletonArrayLength = SFrame.SkeletonArrayLength;
		}

		public CustomSkeletonFrameReadyEventArgs(CustomSkeletonFrame skeletonFrame)
		{
			this.CSFrame = skeletonFrame;
			SkeletonArrayLength = skeletonFrame.SkeletonData.Length;
		}

		public void CopySkeletonDataTo(Skeleton[] skeletonData)
		{
			if (SFrame == null)
			{
				Array.Copy(CSFrame.SkeletonData, skeletonData, CSFrame.SkeletonData.Length);
			}
			else
			{
				SFrame.CopySkeletonDataTo(skeletonData);
			}
		}
	}
}

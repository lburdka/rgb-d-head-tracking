﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KinectIO
{
	public class CustomDepthImageFrameReadyEventArgs:EventArgs
	{
		public CustomDepthFrame CDFrame;
	//	public DepthImageFrame DFrame;
		private short[] depthImageShort;
		public int DepthArrayLength;

		public CustomDepthImageFrameReadyEventArgs(short[] imageBytes)
		{
			depthImageShort = imageBytes;
			DepthArrayLength = depthImageShort.Length;
			
		}

	/*	public CustomDepthImageFrameReadyEventArgs(DepthImageFrame DFrame)
		{
			this.DFrame = DFrame;
			depthImageShort = new short[DFrame.PixelDataLength];
			DepthArrayLength = depthImageShort.Length;
			DFrame.CopyPixelDataTo(depthImageShort);
			CDFrame = new CustomDepthFrame(DFrame);
		}*/

		public CustomDepthImageFrameReadyEventArgs(CustomDepthFrame CDFrame)
		{
			this.CDFrame = CDFrame;
			depthImageShort = new short[CDFrame.PixelDataLength];
			DepthArrayLength = depthImageShort.Length;
			Array.Copy(CDFrame.DepthData, depthImageShort, CDFrame.PixelDataLength);
		}

		public void CopyDepthPixelDataTo(short[] pixelData)
		{
	/*		if (DFrame != null)
			{
				DFrame.CopyPixelDataTo(pixelData);
			}
			else
			{*/
				Array.Copy(depthImageShort, pixelData, depthImageShort.Length);
		//	}
		}


		public void CopyDepthPixelDataTo(byte[] pixelData)
		{
			byte[] depthImageBytes = new byte[depthImageShort.Length * 3];
			byte[] imageBytes = new byte[depthImageShort.Length*2];
			Buffer.BlockCopy(depthImageShort, 0, imageBytes, 0, depthImageShort.Length * 2);
			for (int i = 0; i < depthImageShort.Length; i++)
			{
				depthImageBytes[i * 3] = imageBytes[i * 2];
				depthImageBytes[i * 3 + 1] = imageBytes[i * 2 + 1];
				depthImageBytes[i * 3 + 2] = 0;
			}
			Buffer.BlockCopy(depthImageBytes, 0, pixelData, 0, depthImageBytes.Length);
		}

		public void CopyDepthPixelDataTo(IntPtr pixelData, int pixelDataLength)
		{
		/*	if (DFrame != null)
			{
				DFrame.CopyPixelDataTo(pixelData, pixelDataLength);
			}
			else
			{*/
				Marshal.Copy(depthImageShort, 0, pixelData, depthImageShort.Length);
		//	}
		}
	}
}

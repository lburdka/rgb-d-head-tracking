﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KinectIO
{
	public class CustomSkeletonFrame
	{
		public Tuple<float, float, float, float> FloorClipPlane { get; set; }
		public int FrameNumber { get; set; }
		public long Timestamp { get; set; }
		public Skeleton[] SkeletonData { get; set; }
		public ColorImagePoint[][] JointPositions { get; set; }
		public ColorImagePoint[] SkeletonPositions { get; set; }
		public Tuple<double, double>[][] JointPositionsTuple { get; set; }
		public Tuple<double, double>[] SkeletonPositionsTuple { get; set; }


		public CustomSkeletonFrame()
		{
			
		}

		public CustomSkeletonFrame(SkeletonFrame sf,KinectSensor ks){
			if (sf != null)
			{
				SkeletonData = new Skeleton[sf.SkeletonArrayLength];
				sf.CopySkeletonDataTo(SkeletonData);
				FrameNumber = sf.FrameNumber;
				FloorClipPlane = sf.FloorClipPlane;
				Timestamp = sf.Timestamp;
				this.JointPositions = colorPoints(SkeletonData, ks);
				this.SkeletonPositions = new ColorImagePoint[SkeletonData.Length];
				SkeletonPositionsTuple = new Tuple<double, double>[SkeletonData.Length];

				for (int i = 0; i < SkeletonData.Length; i++)
				{
					SkeletonPositions[i] = ks.CoordinateMapper.MapSkeletonPointToColorPoint(SkeletonData[i].Position, ColorImageFormat.RgbResolution640x480Fps30);
					Tuple<double, double> jpt = new Tuple<double, double>(SkeletonPositions[i].X, SkeletonPositions[i].Y);
					SkeletonPositionsTuple[i] = jpt;
				}

			}
		}

		private ColorImagePoint[][] colorPoints(Skeleton[] skels, KinectSensor ks)
		{
			ColorImagePoint[][] cip = new ColorImagePoint[skels.Length][];
			JointPositionsTuple = new Tuple<double, double>[skels.Length][];
			for (int i = 0; i < skels.Length; i++)
			{
				cip[i] = new ColorImagePoint[20];
				JointPositionsTuple[i] = new Tuple<double, double>[20];
				for (int j = 0; j < 20; j++)
				{
					cip[i][j] = ks.CoordinateMapper.MapSkeletonPointToColorPoint(skels[i].Joints[(JointType)j].Position, ColorImageFormat.RgbResolution640x480Fps30);
					JointPositionsTuple[i][j] = new Tuple<double, double>(cip[i][j].X, cip[i][j].Y);
				}
			}

			return cip;
		}

		public void SatisfyTiczka()
		{
			JointPositions = new ColorImagePoint[JointPositionsTuple.Length][];
			SkeletonPositions = new ColorImagePoint[SkeletonPositionsTuple.Length];
			for (int i = 0; i < SkeletonPositionsTuple.Length; i++)
			{
				JointPositions[i] = new ColorImagePoint[JointPositionsTuple[i].Length];
				ColorImagePoint cip = new ColorImagePoint();
				cip.X = (int)SkeletonPositionsTuple[i].Item1;
				cip.Y = (int)SkeletonPositionsTuple[i].Item2;
				SkeletonPositions[i] = cip;
			}

			for (int i = 0; i < SkeletonPositionsTuple.Length; i++)
			{
				for (int j = 0; j < JointPositionsTuple[i].Length; j++)
				{
					ColorImagePoint cip = new ColorImagePoint();
					cip.X = (int)JointPositionsTuple[i][j].Item1;
					cip.Y = (int)JointPositionsTuple[i][j].Item2;
					JointPositions[i][j] = cip;
				}
			}
		}
	}
}

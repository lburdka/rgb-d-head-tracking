﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectIO
{
	public abstract class CSVSerializer
	{
		public string FileName { get; set; }
		protected abstract string getHeader();

		public void CreateHeader()
		{
			using (StreamWriter outfile = new StreamWriter(FileName))
			{
				outfile.WriteLine(getHeader());
			}
		}

		public bool FileExists()
		{
			return File.Exists(FileName);
		}
	}
}

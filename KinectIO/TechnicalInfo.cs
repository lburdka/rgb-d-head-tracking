﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectIO
{
	public class TechnicalInfo
	{
		public int SituationNumber;
		public int FPS;
		public string ColorFormat;
		public string DepthFormat;
		public string DepthRange;
		public string SkeletonMode;
		public int ElevationAngle;
		public string UniqueKinectID;
		public int MaxDepth;
		public int MinDepth;

		public TechnicalInfo()
		{
			
		}
	}
}
